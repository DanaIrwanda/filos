<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shop extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('headtukunen');
		$this->load->view('shop');
		$this->load->view('foot-tukunen');
	}

	public function preorder()
	{
		$this->load->view('headtukunen');
		$this->load->view('preorder');
		$this->load->view('foot-tukunen');
	}
}
