<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index($url)
	{
		$this->load->view('headtukunen');
		$this->load->view('kategori', ["url" => $url]);
		$this->load->view('foot-tukunen');
	}
}
