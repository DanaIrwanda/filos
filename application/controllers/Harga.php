<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Harga extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('headtukunen');
		$this->load->view('harga');
		$this->load->view('foot-tukunen');
	}
}
