<!-- Swiper CSS -->
<link href="<?= base_url(); ?>assets/css/swiper.min.css" rel="stylesheet">

<!-- Chosen multiselect CSS -->
<link href="<?= base_url(); ?>assets/css/chosen.min.css" rel="stylesheet">

<!-- nouislider CSS -->
<link href="<?= base_url(); ?>assets/css/nouislider.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
</head>

<body>
    <?php include 'loader.php'; ?>
    <div class="sidebar">
        <?php include 'menu.php'; ?>
    </div>
    <div class="wrapper">
        <?php include 'header.php'; ?>

        <div class="container">
            <div class="bg-white full py-3 pl-3 pr-3 mb-2">
                <div class="title">
                    <h2><?= $page['title']; ?></h2>
                </div>
                <hr>
                <?= $page['content']; ?>
            </div>
        </div>
        <?php include 'fix_footer.php'; ?>
    </div>
    <!-- jquery, popper and bootstrap js -->
    <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- cookie js -->
    <script src="<?= base_url(); ?>assets/js/jquery.cookie.js"></script>

    <!-- swiper js -->
    <script src="<?= base_url(); ?>assets/js/swiper.min.js"></script>

    <!-- nouislider js -->
    <script src="<?= base_url(); ?>assets/js/nouislider.min.js"></script>

    <!-- chosen multiselect js -->
    <script src="<?= base_url(); ?>assets/js/chosen.jquery.min.js"></script>

    <!-- template custom js -->
    <script src="<?= base_url(); ?>assets/js/main.js"></script>

    <!-- page level script -->
    <script>
        $(window).on('load', function() {

        });
    </script>