<div class="container-fluid bg-success text-white my-3">
    <div class="row">
        <div class="container">
            <div class="row  py-4 ">
                <div class="col-12 col-md-4 mb-3">
                    <h5 class="text-uppercase mb-2">Hub Kami</h5>
                    <?php $set = $this->func->getSetting("semua"); ?>
                    <p>
                        <?= $set->jamkerja ?>
                    </p>

                    <p><i class="material-icons">perm_phone_msg</i> <?= $set->wasap ?> </p>
                    <p><i class="material-icons">email</i> <?= $set->email ?> </p>
                    <p><i class="material-icons">map</i> <?= $set->alamat ?> </p>
                </div>
                <div class="col-6 col-md-2 mb-3">
                    <h5 class="text-uppercase mb-2">Bantuan</h5>

                    <?php
                    $kategori = $this->db->get("page");
                    foreach ($kategori->result() as $r) {
                    ?>
                        <p class="mb-0"><a href="<?= site_url("page/" . $r->slug) ?>">
                                <?= ucwords(strtolower($r->nama)) ?>
                            </a></p>
                    <?php
                    }
                    ?>
                </div>
                <div class="col-6 col-md-3 mb-3">
                    <h5 class="text-uppercase mb-2">Kategori</h5>
                    <p class="mb-0"><a href="<?= site_url("shop/preorder") ?>">preorder
                        </a></p>

                    <?php
                    $this->db->where("parent", 0);
                    $this->db->limit(8);
                    $kategori = $this->db->get("kategori");
                    foreach ($kategori->result() as $r) {
                    ?>
                        <p class="mb-0"><a href="<?= site_url("kategori/" . $r->url) ?>">
                                <?= ucwords(strtolower($r->nama)) ?>
                            </a></p>
                    <?php
                    }
                    ?>
                </div>
                <div class="col-12 col-md-3">
                    <h5 class="text-uppercase mb-2">Ekspedisi</h5>
                    <?php
                    $kurir = explode("|", $set->kurir);
                    for ($i = 0; $i < count($kurir); $i++) {
                        $kur = $this->func->getKurir($kurir[$i], "halaman");
                        echo '<img style="width:28%;margin:2%;" src="' . base_url("admin/assets/img/kurir/" . $kur . ".png") . '" />';
                    }
                    ?>

                </div>
            </div>
            <div class="text-center">
                <span> Copyright © <?= date('Y') ?> All rights reserved | <?= strtoupper(strtolower($set->nama)) ?></span>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <a href="<?= $set->facebook ?>" title="Facebook" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-facebook-f"></i></a>
    <a href="<?= $set->instagram ?>" title="Instagram" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-instagram"></i></a>
    <a href="<?= $set->color1rgba ?>" title="Twitter" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-twitter"></i></a>
    <a href="<?= $set->color2 ?>" title="Linkedin" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-linkedin-in"></i></a>
    <a href="<?= $set->color2rgba ?>" title="Youtube" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-youtube"></i></a>
</div>