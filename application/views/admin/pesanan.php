<!-- Container -->

<body>
	<?php include 'loader.php'; ?>
	<div class="wrapper">
		<?php include 'menuheadtukunen.php'; ?>
		<div class="container">
			<div class="card mb-4 shadow-sm text-center">
				<h4 class="mt-2">Status Pesanan</h4>
			</div>
			<div class="tab tab-riwayat">
				<div class="row">
					<div class="swiper-container menu-profil">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<nav class="nav" role="tablist">
									<a class="nav-link klik belumbayar btn btn-md btn-success shadow btn-rounded-15 ml-2" href="#belumbayar" data-tablink="belumbayar" role="tab" data-toggle="tab"><i class="far fa-money-bill-alt"></i></a>
									<a class="nav-link btn dikemas btn-md btn-success shadow btn-rounded-15 ml-2" href="#dikemas" role="tab" data-tablink="dikemas" data-toggle="tab"><i class="fas fa-spinner fa-spin "></i></a>
									<a class="nav-link btn dikirim btn-md btn-success shadow btn-rounded-15 ml-2" href="#dikirim" role="tab" data-tablink="dikirim" data-toggle="tab"><i class="fas fa-shipping-fast fa-blink"></i></a>
									<a class="nav-link btn selesai btn-md btn-success shadow btn-rounded-15 ml-2" href="#selesai" role="tab" data-tablink="selesai" data-toggle="tab"><i class="fas fa-check-circle"></i></a>
									<a class="nav-link btn batal btn-md btn-success shadow btn-rounded-15 ml-2" href="#batal" role="tab" data-tablink="batal" data-toggle="tab"><i class="fas fa-exclamation-circle"></i></a>
									<a class="nav-link btn preorder btn-md btn-success shadow btn-rounded-15 ml-2" href="#preorder" data-tablink="preorder" role="tab" data-toggle="tab"><i class="fas fa-dolly"></i></a>
								</nav>
							</div>
						</div>
					</div>
				</div>

				<li class="d-none nav-item">
					<a class="nav-link klik belumbayar" href="#belumbayar" role="tab" data-tablink="belumbayar" data-toggle="tab">Belum Bayar</a>
				</li>
				<!-- Konten -->
				<div class="tab-content">
					<!-- BELUM BAYAR -->
					<div class="tab-pane fade in" role="tabpanel" id="belumbayar"></div>
					<!-- DIKEMAS -->
					<div class="tab-pane fade in" role="tabpanel" id="dikemas"></div>
					<!-- DIKIRIM -->
					<div class="tab-pane fade in" role="tabpanel" id="dikirim"></div>
					<!-- SELESAI -->
					<div class="tab-pane fade in" role="tabpanel" id="selesai"></div>
					<!-- BATAL -->
					<div class="tab-pane fade in" role="tabpanel" id="batal"></div>
					<!-- PRE ORDER -->
					<div class="tab-pane fade in" role="tabpanel" id="preorder"></div>

				</div>
			</div>
		</div>
	</div>
	<?php include 'footermobiletukunen.php'; ?>

</body>

</html>
<!-- jquery, popper and bootstrap js -->
<!-- jquery, popper and bootstrap js -->
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
	$(".js-select2").each(function() {
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});
		/* chosen select*/
	});
	$(".chosen").chosen();
	$(function() {
		<?php
		if (isset($_GET["konfirmasi"])) {
			$datar = [
				"ipaymu" => "",
				"ipaymu_link" => "",
				"ipaymu_trx" => "",
				"ipaymu_tipe" => "",
				"ipaymu_channel" => "",
				"ipaymu_nama" => "",
				"ipaymu_kode" => "",
				"midtrans_id" => ""
			];
			$this->db->where("id", $_GET["konfirmasi"]);
			$this->db->update("pembayaran", $datar);
			$this->func->notiftransfer($_GET["konfirmasi"]);
			echo "konfirmasi(" . $_GET["konfirmasi"] . ")";
		}
		?>

		$("#belumbayar").load("<?php echo site_url("assync/pesanan?status=belumbayar"); ?>", function() {
			$(".nav-item .klik").trigger("click");
		});

		$(".nav-link").each(function() {
			var tab = $(this).data("tablink");
			$(this).click(function() {
				//if($("#"+tab).html() == ""){
				$("#" + tab).html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
				//$(".nav-link .active").removeClass("active");
				//	$(this).addClass("active");
				//var id = $("a",this).attr("id");
				$("#" + tab).load("<?php echo site_url("assync/pesanan?status="); ?>" + tab);
				//}
			});
		});

		$("#upload").on("submit", function(e) {
			$("#upload button").hide();
			$("#upload").append("<h5 class='cl1'><i class='fa fa-circle-o-notch'></i> Mengunggah...</h5>");
		});
	});

	function cekMidtrans(bayar) {
		$('.status-modal').modal('show');
		$("#status").load("<?= site_url("assync/cekmidtrans") ?>?bayar=" + bayar);
	}

	function bayarUlang(trx, invoice) {
		swal({
				title: "Anda yakin?",
				text: "metode pembayaran sebelumnya akan dibatalkan.",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					<?php
					$revoke = site_url("home/invoice?revoke=true&inv=");
					$klik = site_url("home/invoice?inv=");
					$set = $this->func->getSetting("semua");
					if (strpos($set->midtrans_snap, "sandbox") == false) {
					?>
						/*$.post("<?php echo site_url("assync/bayarulangpesanan"); ?>",{"bayar":trx},function(msg){
							var data = eval("("+msg+")");
							if(data.success == true){
								window.location.href = "<?= $klik ?>";
							}else{
								swal("Gagal!","Gagal membatalkan pembayaran sebelumnya, coba ulangi beberapa saat lagi","error");
							}
						});*/
						$.ajax({
							type: "POST",
							url: "<?= site_url("assync/bayarulangpesanan") ?>",
							data: {
								"bayar": trx
							},
							statusCode: {
								200: function(responseObject, textStatus, jqXHR) {
									var data = eval("(" + msg + ")");
									window.location.href = "<?= $revoke ?>";
								},
								404: function(responseObject, textStatus, jqXHR) {
									window.location.href = "<?= $revoke ?>" + invoice;
								},
								500: function(responseObject, textStatus, jqXHR) {
									window.location.href = "<?= $revoke ?>" + invoice;
								}
							}
						});
					<?php } else { ?>
						//swal("Gagal!","Admin menggunakan server sandbox dari midtrans, jadi tidak dapat mengubah status transaksi di midtrans, tapi anda dapat mengganti metode pembayaran lain","error").then((res) =>{
						window.location.href = "<?= $revoke ?>" + invoice;
						//});
					<?php } ?>
				}
			});
	}

	function konfirmasi(bayar) {
		$('.konfirmasi-modal').modal('show');
		$("#bayar").val(bayar);
	}

	function terimaPesanan(trx) {
		swal({
				title: "Anda yakin?",
				text: "pesanan akan di selesaikan dan dana akan diteruskan kepada penjual.",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/terimaPesanan"); ?>", {
						"pesanan": trx
					}, function(msg) {
						var data = eval("(" + msg + ")");
						if (data.success == true) {
							refreshDikirim(1);
							$(".selesai").trigger("click");
						} else {
							swal("Gagal!", "Gagal menyelesaikan pesanan, coba ulangi beberapa saat lagi", "error");
						}
					});
				}
			});
	}

	function ajukanbatal(trx) {
		swal({
				title: "Anda yakin?",
				text: "pesanan akan dibatalkan dan apabila penjual telah menyetujui maka pembayaran akan dikembalikan ke saldo Anda.",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/requestbatalkanPesanan"); ?>", {
						"pesanan": trx
					}, function(msg) {
						var data = eval("(" + msg + ")");
						if (data.success == true) {
							refreshBatal(1);
							$(".batal").trigger("click");
						} else {
							swal("Gagal!", "Gagal mengajukan pembatalan pesanan, coba ulangi beberapa saat lagi", "error");
						}
					});
				}
			});
	}

	function batal(bayar) {
		swal({
				title: "Anda yakin?",
				text: "pesanan akan dibatalkan.",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/batalkanPesanan"); ?>", {
						"pesanan": bayar
					}, function(msg) {
						var data = eval("(" + msg + ")");
						if (data.success == true) {
							refreshBatal(1);
							$(".batal").trigger("click");
						} else {
							swal("Gagal!", "Gagal membatalkan pesanan, doba ulangi beberapa saat lagi", "error");
						}
					});
				}
			});
	}

	function perpanjang(bayar) {
		swal({
				title: "Anda yakin?",
				text: "Batas waktu pengemasan penjual akan diperpanjang 2 hari.",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/perpanjangPesanan"); ?>", {
						"pesanan": bayar
					}, function(msg) {
						var data = eval("(" + msg + ")");
						if (data.success == true) {
							refreshBatal(1);
							$(".dikemas").trigger("click");
						} else {
							swal("Gagal!", "Gagal membatalkan pesanan, doba ulangi beberapa saat lagi", "error");
						}
					});
				}
			});
	}

	function refreshBelumbayar(page) {
		$("#belumbayar").html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
		$("#belumbayar").load("<?php echo site_url("assync/pesanan?status=belumbayar&page="); ?>" + page);
	}

	function refreshBatal(page) {
		$("#batal").html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
		$("#batal").load("<?php echo site_url("assync/pesanan?status=batal&page="); ?>" + page);
	}

	function refreshDikemas(page) {
		$("#dikemas").html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
		$("#dikemas").load("<?php echo site_url("assync/pesanan?status=dikemas&page="); ?>" + page);
	}

	function refreshDikirim(page) {
		$("#dikirim").html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
		$("#dikirim").load("<?php echo site_url("assync/pesanan?status=dikirim&page="); ?>" + page);
	}

	function refreshSelesai(page) {
		$("#selesai").html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
		$("#selesai").load("<?php echo site_url("assync/pesanan?status=selesai&page="); ?>" + page);
	}

	function refreshPO(page) {
		$("#preorder").html("<div class='m-lr-auto txt-center p-tb-20'><h5>loading...</h5></div>");
		$("#preorder").load("<?php echo site_url("assync/pesanan?status=po&page="); ?>" + page);
	}
</script>


<!-- Modal1 -->
<div class="modal fade  status-modal ">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b>Status Pembayaran</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="col-12 mt-3 mb-3">
				<div id="status" class="col-12">

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal1 -->
<div class="modal fade konfirmasi-modal">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content text-center">
			<div class="modal-header">
				<h4 class="modal-title"><b>Konfirmasi</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<p>Upload Bukti Transfer <span style="font-size: 15px">(.jpg, .png, .pdf)</span></p>
			<form id="upload" method="POST" enctype="multipart/form-data" action="<?php echo site_url("manage/konfirmasi"); ?>">
				<input name="idbayar" type="hidden" id="bayar" value="0" />
				<div class="col-md-12">
					<div class="form-control">
						<div class="upload-options">
							<label>
								<input type="file" name="bukti" class="pointer image-upload" accept="image/*" />
							</label>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-default btn-rounded mt-3 mb-3">
					Upload
				</button>
			</form>
		</div>
	</div>
</div>

<div class="modal fade wrap-modal2 js-modal2 modalva p-t-60 p-b-20">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b>Rekening</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="col-12 text-center mt-3">
				Silahkan melakukan pembayaran ke <br />
				<h4>Nomor Virtual Account</h4>
				<div class="loadva"></div>
				<b>Catatan:</b><br />
				Apabila melakukan pembayaran melalui Channel Alfamart / Indomaret, sampaikan kepada petugas kasirnya
				bahwa akan melakukan pembayaran <b>IPAYMU</b>
				<a href="" id="linkVA" class="btn btn-warning btn-rounded mt-3 mb-3">UBAH METODE PEMBAYARAN</a>
			</div>
		</div>
	</div>
</div>