<div class="text-center mt-2 mb-2">
	<button class="btn btn-sm btn-danger shadow btn-rounded">
		Pre Order
	</button>
</div>

<div class="col-12 px-0">
	<?php
	$page = (isset($_GET["page"]) and $_GET["page"] != "" and intval($_GET["page"]) > 0) ? intval($_GET["page"]) : 1;
	$perpage = 10;

	$this->db->where("usrid", $_SESSION["usrid"]);
	$rows = $this->db->get("preorder");
	$rows = $rows->num_rows();

	$this->db->where("usrid", $_SESSION["usrid"]);
	$this->db->order_by("status", "ASC");
	$this->db->limit($perpage, ($page - 1) * $perpage);
	$db = $this->db->get("preorder");
	if ($db->num_rows() > 0) {
		foreach ($db->result() as $rx) {
	?>

			<div class="col-12 px-0">
				<ul class="list-items mb-1">
					<li>
						<div class="col-12">
							<div class="row">
								<div class="card shadow-sm border-0">
									<div class="card-body">
										<figure class="product-image"><img src="<?php echo $this->func->getFoto($rx->idproduk, "utama"); ?>" alt="produk" class=""></figure>
									</div>
								</div>
								<div class="col">
									<p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
																				echo $produk->nama;
																			} else {
																				echo "Produk telah dihapus";
																			} ?></p>
									<?= $variasi ?>
									<p class="text-secondary small mb-2"> Payment ID <?php echo $rx->invoice; ?>
									</p>
									<h5 class="text-success font-weight-normal mb-0">Rp <?php echo $this->func->formUang($rx->harga); ?> <span style="font-size:11px">x<?php echo $rx->jumlah; ?></span></h5>
								</div>
							</div>
						</div>
					</li>
				</ul>
				<div class="row">
					<div class="col-md-2">
						<div class="how-itemcart1">
							<?php
							$produk = $this->func->getProduk($rx->idproduk, "semua");
							if (isset($produk->tglpo)) {
								$tglpo = $produk->tglpo;
							} else {
								$tglpo =  "0000-00-00 00:00:00";
							}
							?>
						</div>
					</div>
				</div>
				<div class="col-12 px-0">
					<div class="row">
						<div class="col-md-8">
							<?php if ($rx->status == 0) { ?>
								<div class="col-md-6"><span class="text-danger">Belum Bayar</span></div>
								<div class="col-md-6">
									<a class="btn btn-sm btn-success btn-rounded" href="<?= site_url("home/invoicepreorder/?inv=" . $this->func->arrEnc(array("idbayar" => $rx->id), "encode")) ?>">Cara Pembayaran</a>
								</div>
							<?php } elseif ($rx->status == 1) { ?>
								<?php if ($this->func->ubahTgl("Ymd", $tglpo) > date("Ymd")) { ?>
									<div class="col-12"><b class="text-secondary small text-primary">Sedang Dalam Proses Produksi</b></div>
									<?php } else {
									$this->db->where("idpo", $rx->id);
									$this->db->where("idtransaksi >", 0);
									$dbx = $this->db->get("transaksiproduk");
									if ($dbx->num_rows() > 0) {
									?>
										<b class="text-secondary small text-success">Pesanan sudah diproses</b>
									<?php } else { ?>
										<div class="row">
											<div class="col-6">
												<b class="text-secondary small text-success">Stok Ready Silahkan Melakukan Checkout/Pelunasan</b>
											</div>
											<div class="col-6 text-center mt-3">
												<a class="btn btn-sm btn-success btn-rounded" href="<?= site_url("home/bayarpreorder/?predi=" . $this->func->arrEnc(array("idbayar" => $rx->id), "encode")) ?>">Checkout</a>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</div>
						<div class="col-md-4 mt-2">
							<div class="row">
								<div class="col-6 text-right">
									<h5 class="text-black">Total DP</h5>
								</div>
								<div class="col-6">
									<h5 class="text-black">: Rp <?php echo $this->func->formUang($rx->total); ?></h5>
								</div>
							</div>
							<div class="row">
								<div class="col-6 text-right">
									<h5 class="text-danger">Total Pelunasan</h5>
								</div>
								<div class="col-6">
									<h5 class="text-danger">: Rp <?php echo $this->func->formUang(($rx->jumlah * $rx->harga) - $rx->total); ?></h5>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
			</div>
		<?php
		}
		echo $this->func->createPagination($rows, $page, $perpage, "refreshDikirim");
		?>
</div>
<?php
	} else {
?>
	<div class="col-md-12 text-center">
		<img src="<?php echo base_url("assets/images/komponen/open-box.png"); ?>" style="width:240px;" />
		<h5 class="mtext-103 color2">TIDAK ADA PESANAN</h5>
	</div>
<?php
	}
?>

<script type="text/javascript">
	$(document).ready(function() {
		$(".show-product").hide();
		$(".view-product").click(function() {
			$(this).parent().parent().find(".show-product").slideToggle();
			$(this).parent().parent().find(".view-product").toggle();
		});
	});
</script>