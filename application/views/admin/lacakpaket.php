<?php include 'loader.php'; ?>
<div class="wrapper">
  <?php include 'menuheadtukunen.php'; ?>
  <div class="container">
    <div class="row card">
      <div class="col-12">
        <h4 class="text-center mt-3">
          Order ID <span class="cl2"><?php echo $orderid; ?></span>
        </h4>
        <hr>
        <div class="row">
          <div class="col-md-6 p-b-10 p-t-10">
            <p class="m-b-20">
              Kurir Pengiriman:<br />
              <b class='badge badge-secondary' style="font-size:100%;"><?php echo strtoupper(strtolower($this->func->getKurir($transaksi->kurir, "nama", "rajaongkir") . " - " . $transaksi->paket)); ?></b>
            </p>
            <p class="m-b-10">
              No Resi Pengiriman:<br />
              <b class="cl1" style="font-size:120%;"><?php echo strtoupper(strtolower($transaksi->resi)); ?></b>
            </p>
          </div>
          <div class="col-12 mb-3">
            <p class="m-b-10">
              Waktu Pengiriman:<br />
              <i class="cl1"><?php echo $this->func->ubahTgl("d M Y H:i", $transaksi->kirim); ?> WIB</i>
            </p>
          </div>
        </div>
      </div>
    </div>


    <div class="row card mt-2">
      <div class="col-12">
        <h4 class="text-center mt-3">
          Status Pengiriman
        </h4>
        <hr>
        <div class="of-hidden mb-3" id="load">
          <i class="fa fa-spin fa-globe"></i> &nbsp;menghubungi ekspedisi, mohon tunggu sebentar...
        </div>
      </div>
    </div>
  </div>
</div>
<?php include 'footermobiletukunen.php'; ?>

<style>
  .tracking {
    margin-left: 13px;
  }

  .task-list {
    list-style: none;
    position: relative;
    padding: 5px 0 0;
  }

  .task-list:after {
    content: "";
    position: absolute;
    background: #ecedef;
    height: 130%;
    width: 2px;
    top: 0;
    left: 30px;
    z-index: 1;
  }

  .task-list li {
    margin-bottom: 10px;
    padding-left: 55px;
    position: relative;
  }

  .task-list li:last-child {
    margin-bottom: 0;
  }

  .bg-c-green {
    background: #1de9b6;
  }

  .task-list li .task-icon {
    position: absolute;
    left: 22px;
    top: 13px;
    border-radius: 50%;
    padding: 2px;
    width: 17px;
    height: 17px;
    z-index: 2;
    -webkit-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.2);
  }
</style>


<script type="text/javascript">
  $(function() {
    $("#load").load("<?php echo site_url("assync/lacakiriman?orderid=" . $orderid); ?>");
  });
</script>

<!-- jquery, popper and bootstrap js -->
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
  $(".js-select2").each(function() {
    $(this).select2({
      minimumResultsForSearch: 20,
      dropdownParent: $(this).next('.dropDownSelect2')
    });
    /* chosen select*/
  });
  $(".chosen").chosen();
</script>