<?php include 'loader.php'; ?>
<div class="wrapper">
  <?php include 'menuheadtukunen.php'; ?>

  <!-- Shoping Cart -->
  <div class="container">
    <div class="row card">
      <h4 class="mt-3 text-center">
        Ulasan Produk Pesanan
      </h4>
      <hr>
      <?php
      $order = $this->func->getTransaksi($orderid, "semua", "orderid");
      $this->db->where("idtransaksi", $order->id);
      $db = $this->db->get("review");

      if ($db->num_rows() > 0) {
      ?>
        <?php
        foreach ($db->result() as $r) {
          $produk = $this->func->getProduk($r->idproduk, "semua");
        ?>
          <div class="col-12">
            <div class="show-product">
              <ul class="list-items mb-1">
                <li>
                  <div class="col-12">
                    <div class="row">
                      <div class="card shadow-sm border-0">
                        <div class="card-body">
                          <figure class="product-image"><img src="<?php echo $this->func->getFoto($produk->id, "utama"); ?>" alt="produk" class=""></figure>
                        </div>
                      </div>
                      <div class="col">
                        <a href="<?php echo site_url('produk/' . $produk->url); ?>">
                          <p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
                                                                  echo $produk->nama;
                                                                } else {
                                                                  echo "Produk telah dihapus";
                                                                } ?></p>
                          <?= $variasi ?>
                          </p>
                        </a>
                        <span style="color:gold;">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star <?php if ($r->nilai < 2) {
                                                  echo "text-secondary";
                                                } ?>"></i>
                          <i class="fa fa-star <?php if ($r->nilai < 3) {
                                                  echo "text-secondary";
                                                } ?>"></i>
                          <i class="fa fa-star <?php if ($r->nilai < 4) {
                                                  echo "text-secondary";
                                                } ?>"></i>
                          <i class="fa fa-star <?php if ($r->nilai < 5) {
                                                  echo "text-secondary";
                                                } ?>"></i>
                        </span>
                        <small style="display:block;">
                          <?php
                          switch ($r->nilai) {
                            case 1:
                              echo "(produk sangat buruk)";
                              break;
                            case 2:
                              echo "(produk kurang baik)";
                              break;
                            case 3:
                              echo "(produk standar)";
                              break;
                            case 4:
                              echo "(produk baik)";
                              break;
                            case 5:
                              echo "(produk sangat baik)";
                              break;
                          }
                          echo "<br/>";
                          echo $this->func->ubahTgl("d M Y H:i", $r->tgl);
                          ?>
                        </small>
                        <?php if ($r->keterangan != "") { ?>
                          <div class="row">
                            <div class="col-md-6">
                              <textarea class="form-control" readonly><?php echo $r->keterangan; ?></textarea>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>

          </div>
        <?php
        }
        ?>
        <?php
      } else {
        if ($_SESSION["usrid"] != $order->usrid) {
        ?>
          <div class="m-b-60 p-lr-0-sm w-full">
            <h5 class="p-lr-14 cl1">Pembeli belum memberikan ulasan untuk pesanan ini</h5>
          </div>
        <?php
        } else {
        ?>
          <div class="col-12">
            <form class="row m-lr-0" id="addreview">
              <?php
              $this->db->where("idtransaksi", $order->id);
              $db = $this->db->get("transaksiproduk");
              $no = 1;
              foreach ($db->result() as $t) {
                $produk = $this->func->getProduk($t->idproduk, "semua");
              ?>
                <div class="col-12">
                  <div class="bor10 p-lr-40 p-t-30 m-lr-0-xl p-lr-15-sm">
                    <input type="hidden" name="orderid[]" value="<?php echo $order->orderid; ?>" />
                    <input type="hidden" name="produk[]" value="<?php echo $t->idproduk; ?>" />
                    <div class="px-0 col-12">

                      <div class="show-product">
                        <ul class="list-items mb-1">
                          <li>
                            <div class="col-12">
                              <div class="row">
                                <div class="card shadow-sm border-0">
                                  <div class="card-body">
                                    <figure class="product-image"><img src="<?php echo $this->func->getFoto($produk->id, "utama"); ?>" alt="produk" class=""></figure>
                                  </div>
                                </div>
                                <div class="col">
                                  <p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
                                                                          echo $produk->nama;
                                                                        } else {
                                                                          echo "Produk telah dihapus";
                                                                        } ?></p>
                                  <?= $variasi ?>
                                  <div id="ulasan_<?= $t->id ?>">
                                    <span class="wrap-rating rating-star pointer">
                                      <i class="item-rating pointer fa fa-star text-secondary nilai_1" onclick="nilai(<?= $t->id ?>,1);$('#nilai_<?php echo $no; ?>').val(1);"></i>
                                      <i class="item-rating pointer fa fa-star text-secondary nilai_2" onclick="nilai(<?= $t->id ?>,2);$('#nilai_<?php echo $no; ?>').val(2);"></i>
                                      <i class="item-rating pointer fa fa-star text-secondary nilai_3" onclick="nilai(<?= $t->id ?>,3);$('#nilai_<?php echo $no; ?>').val(3);"></i>
                                      <i class="item-rating pointer fa fa-star text-secondary nilai_4" onclick="nilai(<?= $t->id ?>,4);$('#nilai_<?php echo $no; ?>').val(4);"></i>
                                      <i class="item-rating pointer fa fa-star text-secondary nilai_5" onclick="nilai(<?= $t->id ?>,5);$('#nilai_<?php echo $no; ?>').val(5);"></i>
                                      <input type="hidden" class="nilai" id="nilai_<?php echo $no; ?>" name="nilai[]" value="0">
                                    </span>
                                  </div>
                                  <style>
                                    .rating-star {
                                      color: gold;
                                    }
                                  </style>
                                  <div class="row mt-3">
                                    <div class="col-md-6">
                                      <textarea class="form-control" id="review" name="keterangan[]" placeholder="Beritahu yang lain tentang produk ini"></textarea>
                                    </div>
                                  </div>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
                $no++;
              }
              ?>
              <div class="col-12 p-lr-20 m-b-30">
                <button type="submit" class="submit btn btn-success btn-rounded shadow mb-2">KIRIM ULASAN</button>
                <a href="javascript:history.back()" class="submit btn btn-danger btn-rounded shadow mb-2">LAIN KALI</a>
                <h5 class="col1 loaders" style="display:none;"><i class="fa fa-spin fa-circle-o-notch"></i> mengirim ulasan</h5>
              </div>
            </form>
          </div>
      <?php
        }
      }
      ?>

    </div>
  </div>
</div>

<?php include 'footermobiletukunen.php'; ?>
</div>
<!-- jquery, popper and bootstrap js -->
<!-- jquery, popper and bootstrap js -->
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
  $(".js-select2").each(function() {
    $(this).select2({
      minimumResultsForSearch: 20,
      dropdownParent: $(this).next('.dropDownSelect2')
    });
    /* chosen select*/
  });
  $(".chosen").chosen();
</script>


<script type="text/javascript">
  $(function() {
    $("#addreview").on("submit", function(e) {
      e.preventDefault();
      $(".submit").hide();
      $(".loaders").show();

      var nonempty = $('.nilai').filter(function() {
        return this.value == '0';
      });

      if (nonempty.length == 0) {
        $.post("<?php echo site_url("assync/tambahulasan"); ?>", $(this).serialize(), function(msg) {
          var data = eval("(" + msg + ")");
          if (data.success == true) {
            swal("Berhasil!", "berhasil mengirim ulasan", "success").then((value) => {
              location.reload();
            });
          } else {
            swal("Gagal!", "terjadi kendala pada saat mengirim ulasan, cobalah beberapa saat lagi", "error");
            $(".submit").show();
            $(".loaders").hide();
          }
        });
      } else {
        swal("Gagal!", "Semua produk harus diberi nilai", "error").then((value) => {
          $(".submit").show();
          $(".loaders").hide();
        })
      }
    });
  });

  function nilai(trx, num) {
    for (i = 1; i <= num; i++) {
      $("#ulasan_" + trx + " .nilai_" + i).removeClass("text-secondary");
    }
    var nam = num + 1;
    for (i = nam; i <= 5; i++) {
      $("#ulasan_" + trx + " .nilai_" + i).addClass("text-secondary");
    }
  }
</script>