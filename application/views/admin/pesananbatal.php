<div class="text-center mt-2 mb-2">
	<button class="btn btn-sm btn-danger shadow btn-rounded">
		Pesanan Batal
	</button>
</div>

<div class="col-12 px-0">
	<?php
	$page = (isset($_GET["page"]) and $_GET["page"] != "" and intval($_GET["page"]) > 0) ? intval($_GET["page"]) : 1;
	$perpage = 10;

	$this->db->where("status", 4);
	$this->db->where("usrid", $_SESSION["usrid"]);
	$rows = $this->db->get("transaksi");
	$rows = $rows->num_rows();

	$this->db->where("status", 4);
	$this->db->where("usrid", $_SESSION["usrid"]);
	$this->db->order_by("id", "DESC");
	$this->db->limit($perpage, ($page - 1) * $perpage);
	$db = $this->db->get("transaksi");
	if ($db->num_rows() > 0) {
		foreach ($db->result() as $rx) {
	?>

			<div class="col-12 px-0">
				<div class="col-md-12">
					<div class="row">
						<?php
						$this->db->where("idtransaksi", $rx->id);
						$trp = $this->db->get("transaksiproduk");
						$totalproduk = 0;
						$no = 1;
						foreach ($trp->result() as $key) {
							$totalproduk += $key->harga * $key->jumlah;
							$produk = $this->func->getProduk($key->idproduk, "semua");
							$variasee = $this->func->getVariasi($key->variasi, "semua");
							$variasi = ($key->variasi != 0 and $variasee != null) ? $this->func->getWarna($variasee->warna, "nama") . " " . $produk->subvariasi . " " . $this->func->getSize($variasee->size, "nama") : "";
							$variasi = ($key->variasi != 0 and $variasee != null) ? "<br/><small class='text-primary'>" . $produk->variasi . ": " . $variasi . "</small>" : "";
							if ($no == 2) {
						?>
								<div class="col-12">
									<div class="row show-product">
									<?php
								}
									?>
									<ul class="list-items mb-1">
										<li>
											<div class="col-12">
												<div class="row">
													<div class="card shadow-sm border-0">
														<div class="card-body">
															<figure class="product-image"><img src="<?php echo $this->func->getFoto($key->idproduk, "utama"); ?>" alt="produk" class=""></figure>
														</div>
													</div>
													<div class="col">
														<p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
																									echo $produk->nama;
																								} else {
																									echo "Produk telah dihapus";
																								} ?></p>
														<?= $variasi ?>
														<p class="text-secondary small mb-2"> Payment ID <?php echo $rx->orderid; ?>
														</p>
														<h5 class="text-success font-weight-normal mb-0">Rp <?php echo $this->func->formUang($key->harga); ?> <span style="font-size:11px">x<?php echo $key->jumlah; ?></span></h5>
														<a href="<?php echo site_url("manage/detailpesanan/?orderid=") . $rx->orderid; ?>" class="badge badge-danger text-white">Lihat Rincian Pesanan</a>

													</div>
												</div>
											</div>
										</li>
									</ul>
								<?php
								$no++;
							}
							if ($no > 2) {
								?>
									</div>
									<div class="row p-b-30 p-r-10">
										<a href="javascript:void(0)" class="view-product badge badge-warning"><i class="fa fa-chevron-circle-down"></i> Lihat produk lainnya</a>
										<a href="javascript:void(0)" class="view-product badge badge-warning" style="display:none;"><i class='fa fa-chevron-circle-up'></i> Sembunyikan produk</a>
									</div>
								<?php
							}
								?>
								</div>
								<div class="col-12 px-0 mb-3">
									<div class="row">
										<div class="col-md-5">
											<b>Keterangan:</b><br />
											<span><?php echo $rx->keterangan; ?></span>
										</div>
										<div class="col-md-3">
											<b>Waktu Pembatalan:</b><br />
											<i class="badge"><?php echo $this->func->ubahTgl("d M Y H:i", $rx->selesai); ?> WIB</i>
										</div>
										<div class="col-md-4">
											<div class="row">
												<div class="col-6 txt-right">
													<h5 class="mtext-102 text-black">Total Order</h5>
												</div>
												<div class="col-6">
													<h5 class="mtext-102 text-black text-right">Rp <?php echo $this->func->formUang($rx->ongkir + $totalproduk); ?></h5>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>

					</div>
				</div>
			<?php
		}
		echo $this->func->createPagination($rows, $page, $perpage, "refreshDikirim");
			?>
			</div>
		<?php
	} else {
		?>
			<div class="col-md-12 text-center cl1 p-tb-20">
				<img src="<?php echo base_url("assets/images/komponen/open-box.png"); ?>" style="width:240px;" />
				<h5 class="mtext-103 color2">TIDAK ADA PESANAN</h5>
			</div>
		<?php
	}
		?>

		<script type="text/javascript">
			$(document).ready(function() {
				$(".show-product").hide();
				$(".view-product").click(function() {
					$(this).parent().parent().find(".show-product").slideToggle();
					$(this).parent().parent().find(".view-product").toggle();
				});
			});
		</script>