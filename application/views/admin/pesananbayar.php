<div class="text-center mt-2 mb-2">
	<button class="btn btn-sm btn-danger shadow btn-rounded">
		Belum bayar
	</button>
</div>

<div class="col-12 px-0">
	<?php
	$page = (isset($_GET["page"]) and $_GET["page"] != "" and intval($_GET["page"]) > 0) ? intval($_GET["page"]) : 1;
	$perpage = 10;

	$this->db->where("status", 0);
	$this->db->where("usrid", $_SESSION["usrid"]);
	$rows = $this->db->get("pembayaran");
	$rows = $rows->num_rows();

	$this->db->where("status", 0);
	$this->db->where("usrid", $_SESSION["usrid"]);
	$this->db->order_by("status ASC, id DESC");
	$this->db->limit($perpage, ($page - 1) * $perpage);
	$db = $this->db->get("pembayaran");
	if ($db->num_rows() > 0) {
		foreach ($db->result() as $res) {
			$idbyr = $this->func->arrEnc(array("idbayar" => $res->id), "encode");
			$this->db->where("idbayar", $res->id);
			$konf = $this->db->get("konfirmasi");
			$link = $this->func->arrEnc(array("idbayar" => $res->id), "encode");
			$klik = ($res->ipaymu_tipe == "va" || $res->ipaymu_tipe == "cstore") ? "bayarVA(" . $res->id . ",'" . site_url("home/invoice?inv=" . $link) . "')" : "openLink('" . site_url("home/invoice?inv=" . $link) . "')";
			//
	?>

			<div class="col-md-12">
				<div class="row">
					<?php
					$this->db->where("idbayar", $res->id);
					$trx = $this->db->get("transaksi");
					$no = 1;
					foreach ($trx->result() as $rx) {
						$this->db->where("idtransaksi", $rx->id);
						$trp = $this->db->get("transaksiproduk");
						foreach ($trp->result() as $key) {
							$produk = $this->func->getProduk($key->idproduk, "semua");
							$variasee = ($key->variasi != 0) ? $this->func->getVariasi($key->variasi, "semua") : null;
							$variasi = ($key->variasi != 0 and $variasee != null) ? $this->func->getWarna($variasee->warna, "nama") . " " . $produk->subvariasi . " " . $this->func->getSize($variasee->size, "nama") : "";
							$variasi = ($key->variasi != 0 and $variasee != null) ? "<br/><small class='text-primary'>" . $produk->variasi . ": " . $variasi . "</small>" : "";
							//if($no == 1){
							if ($no == 2) {
					?>
								<div class="col-12">
									<div class="row show-product">
									<?php
								}
									?>
									<ul class="list-items mb-1">
										<li>
											<div class="col-12">
												<div class="row">
													<div class="card shadow-sm border-0">
														<div class="card-body">
															<figure class="product-image"><img src="<?php echo $this->func->getFoto($key->idproduk, "utama"); ?>" alt="produk" class=""></figure>
														</div>
													</div>
													<div class="col">
														<p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
																									echo $produk->nama;
																								} else {
																									echo "Produk telah dihapus";
																								} ?></p>
														<?= $variasi ?>
														<p class="text-secondary small mb-2"> Payment ID <?php echo $res->invoice; ?>
														</p>
														<h5 class="text-success font-weight-normal mb-0">Rp <?php echo $this->func->formUang($key->harga); ?> <span style="font-size:11px">x<?php echo $key->jumlah; ?></span></h5>
													</div>
												</div>
											</div>
										</li>
									</ul>
								<?php
								$no++;
							}
						}
						if ($no > 2) {
								?>
									</div>
								</div>
								<div class="col-12">
									<a href="javascript:void(0)" class="view-product badge badge-warning"><i class="fa fa-chevron-circle-down"></i> Lihat produk lainnya</a>
									<a href="javascript:void(0)" class="view-product badge badge-warning" style="display:none;"><i class='fa fa-chevron-circle-up'></i> Sembunyikan produk</a>

								<?php
							}
								?>
								<a href="javascript:void(0)" onclick="batal(<?php echo $res->id; ?>)" class="badge badge-danger text-white">
									<i class="fa fa-times-circle"></i> batalkan pesanan
								</a>
								</div>

				</div>
				<hr>
				<div class="row">
					<div class="col-6">
						<h5 class="text-black">Total Pembayaran</h5>
					</div>
					<div class="col-6">
						<h5 class="text-black text-right">Rp <?php echo $this->func->formUang($res->saldo + $res->transfer); ?></h5>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p class="text-secondary small mb-2">Segera lakukan pembayaran dalam <b>1x24 jam</b>, atau pesanan Anda akan Otomatis Dibatalkan.</p>
					</div>
					<div class="col-md-6">
						<div class="row">

							<?php
							$this->db->where("idbayar", $res->id);
							$knf = $this->db->get("konfirmasi");

							if ($knf->num_rows() > 0) {
								echo "<div class='col-md-12 cl1 txt-center'><b>status pembayaran:</b> <i>menunggu verifikasi sistem</i>";
								foreach ($knf->result() as $ref) {
									echo "<br/><b>waktu konfirmasi:</b> <i>" . $this->func->ubahTgl("d M Y H:i", $ref->tgl) . " WIB</i>";
								}
								echo "</div>";
							} else {
								if ($res->midtrans_id != "") {
							?>
									<div class="col-md-6 mt-2">
										<a href="javascript:void(0)" onclick="cekMidtrans(<?= $res->id ?>)" class="w-100 btn btn-sm btn-success shadow btn-rounded">
											Cek Status
										</a>
									</div>
									<div class="col-md-6 mt-2">
										<a href="javascript:void(0)" onclick="bayarUlang(<?= "'" . $res->id . "','" . $link . "'" ?>)" class="w-100 btn btn-sm btn-success shadow btn-rounded">
											Ubah Metode Pembayaran
										</a>
									</div>
								<?php
								} else {
								?>
									<div class="col-md-6 text-center mb-2 mt-2">
										<?php if ($res->ipaymu_tipe == "va" || $res->ipaymu_tipe == "cstore") { ?>
											<a href="javascript:void(0)" onclick="<?= $klik ?>" class="w-100 btn btn-sm btn-success shadow btn-rounded">
												Ubah Metode Bayar
											</a>

										<?php } else { ?>
											<a href="javascript:void(0)" onclick="<?= $klik ?>" class="w-100 btn btn-sm btn-success shadow btn-rounded">
												Bayar Pesanan
											</a>
										<?php } ?>
									</div>
									<div class="col-md-6 text-center mb-2 mt-2">
										<a href="javascript:void(0)" onclick="konfirmasi(<?php echo $res->id; ?>)" class="w-100 btn btn-sm btn-success shadow btn-rounded">
											Konfirmasi
										</a>
									</div>
							<?php
								}
							}
							?>
							<hr>
						</div>
					</div>
				</div>
				<hr>
			</div>
			<div id="bayarva_<?= $res->id ?>" class="bayarva" style="display:none;">
				<div class="nomerva m-lr-30 m-t-20 p-lr-20 p-tb-14 bg2 bold">
					<h2 class="text-success"><?= $res->ipaymu_kode ?></h2>
				</div>
				<div class="bank m-lr-30 m-t-10">
					<h4>Channel: <?= strtoupper(strtolower($res->ipaymu_channel)) ?></h4>
				</div>
				<div class="bank m-lr-30 m-t-10">
					<h4>Total Pembayaran:<b class="text-danger"> Rp. <?= $this->func->formUang($res->transfer + $res->kodebayar) ?></b></h4>
				</div>
			</div>
		<?php
		}
		echo $this->func->createPagination($rows, $page, $perpage, "refreshBelumbayar");
		?>
</div>
<?php
	} else {
?>
	<div class="col-md-12 text-center">
		<img src="<?php echo base_url("assets/images/komponen/open-box.png"); ?>" style="width:240px;" />
		<h5 class="mtext-103 color2">TIDAK ADA PESANAN</h5>
	</div>
<?php
	}
?>

<script type="text/javascript">
	$(document).ready(function() {
		$(".show-product").hide();
		$(".view-product").click(function() {
			$(this).parent().parent().find(".show-product").slideToggle();
			$(this).parent().parent().find(".view-product").toggle();
		});
	});

	function bayarVA(id, link) {
		$('.modalva').modal('show');
		$(".loadva").html($("#bayarva_" + id).html());
		$("#linkVA").attr("href", link);
	}

	function tutupVA() {
		$('.modalva').removeClass('show-modal2');
	}

	function openLink(id) {
		window.location.href = id;
	}
</script>