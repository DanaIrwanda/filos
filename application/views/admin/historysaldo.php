<?php
if ($saldo->num_rows() > 0) {
?>
  <div class="wrap-table-alamat">
    <?php
    foreach ($saldo->result() as $res) {
      $old = ($res->darike != 2) ? "[invoice]" : "[rekening]";
      switch ($res->darike) {
        case '1':
          $new = $this->func->getTransaksi($res->sambung, "orderid");
          break;
        case '2':
          $new = $this->func->getSaldotarik($res->sambung, "idrek");
          $new = $this->func->getRekening($new, "semua");
          $bank = $this->func->getBank($new->idbank, "nama");
          $new = $bank . " a/n " . $new->atasnama . " (" . $new->norek . ")";
          break;
        case '3':
          $new = $this->func->getBayar($res->sambung, "invoice");
          break;
        case '4':
          $new = $this->func->getTransaksi($res->sambung, "orderid");
          break;
        default:
          $new = "";
          break;
      }
      $status = ($res->darike == 2) ? $this->func->getSaldotarik($res->sambung, "status") : 1;
      $status = ($status == 1) ? "<span style='color:#27ae60'>Berhasil</span>" : "<span style='color:#c0392b'>Sedang Diproses</span>";
      $jumlah = $this->func->formUang($res->jumlah);
      $jumlah = ($res->darike != 2 and $res->darike != 3) ? "<span style='color:#27ae60'>+ Rp " . $jumlah . "</span>" : "<span style='color:#c0392b'>- Rp " . $jumlah . "</span>";
    ?>

      <ul class="list-items mb-2">
        <li>
          <div class="col-12">
            <div class="row">
              <div class="col-7">
                <span class="text-secondary small mb-2">
                  <?php echo $this->func->ubahTgl("d M Y H:i", $res->tgl); ?> </span>
                <h5 class="text-danger font-weight-normal mb-0"><?php echo $jumlah; ?></h5>
                </span></p>
                <span class="text-success"> <i style="color:#f39c12"> <?php echo $status; ?>
                  </i> </span>
              </div>
              <div class="col-5 text-right">
                <button class="btn btn-md btn-success btn-rounded shadow"> Rp <?php echo $this->func->formUang($res->saldoakhir); ?> </button>
              </div>
            </div>
          </div>
        </li>
      </ul>
    <?php
    }
    ?>
  </div>
<?php
  echo $this->func->createPagination($rows, $page, $perpage, "historySaldo");
} else {
  echo "<div class='w-full txt-center'><h5>BELUM ADA TRANSAKSI</h5></div>";
}
?>