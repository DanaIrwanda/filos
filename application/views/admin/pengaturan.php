<body>
	<?php include 'loader.php'; ?>
	<div class="wrapper">
		<?php include 'menuheadtukunen.php'; ?>
		<div class="container">
			<!--<div class="text-center">
				<div class="figure-profile shadow my-4">
					<figure><img src="http://tukunen2.adflinky.com/assets/images/profile/1601301981552.jpg" alt="bambang"></figure>
				</div>
				<h3 class="mb-1 ">bambang</h3>
				<p class="text-secondary">kendal, kendal</p>
			</div>-->
			<div class="card mb-4 shadow-sm">
				<div class="card-body">
					<div class="row">
						<div class="col-auto">
							<span class="btn btn-default p-3 btn-rounded-15">
								<i class="material-icons">account_balance_wallet</i>
							</span>
						</div>
						<div class="col pl-0">
							<p class="text-secondary mb-1">Saldo</p>
							<h4 class="text-dark my-0">Rp <?php echo $this->func->formUang($this->func->getSaldo($_SESSION["usrid"], "saldo", "usrid")); ?>,-</h4>
						</div>
						<div class="col-auto pl-0 align-self-center">
							<button data-toggle="modal" data-target="#topupsaldo" class="btn btn-default button-rounded-36 shadow"><i class="material-icons">add</i></button>
							<!--<button data-toggle="modal" data-target="#tariksaldo" class="btn btn-success button-rounded-36 shadow"><i class="material-icons">remove</i></button>-->
						</div>

					</div>
				</div>
			</div>

			<div class="tab">
				<div class="row">
					<div class="swiper-container menu-profil">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<nav class="nav swiper-slide" role="tablist">
									<a class="nav-link klik btn btn-md btn-success shadow btn-rounded-15 ml-2" href="#saldo" role="tab" data-toggle="tab"><i class="material-icons md-18 text-mute">account_balance_wallet</i></a>
									<a class="nav-link btn btn-md btn-success shadow btn-rounded-15 ml-2" href="#rekening" role="tab" data-toggle="tab"><i class="material-icons md-18 text-mute">card_giftcard</i></a>
									<a class="nav-link btn btn-md btn-success shadow btn-rounded-15 ml-2" href="#alamat" role="tab" data-toggle="tab"><i class="material-icons md-18 text-mute">home</i></a>
									<a class="nav-link btn btn-md btn-success shadow btn-rounded-15 ml-2" href="#informasi" role="tab" data-toggle="tab"><i class="material-icons md-18 text-mute">settings</i></a>
									<a class="nav-link btn btn-md btn-success shadow btn-rounded-15 ml-2" href="<?= site_url("manage/pesanan") ?>"><i class="material-icons md-18 text-mute">shopping_cart</i></a>
									<a class="nav-link btn btn-md btn-success shadow btn-rounded-15 ml-2" href="<?= site_url("home/signout") ?>"><i class="material-icons md-18 text-mute">exit_to_app</i></a>
								</nav>
							</div>
						</div>
					</div>
				</div>


				<!-- Tab panes -->
				<div class="tab-content">
					<!-- SALDO -->
					<div role="tabpanel" class="tab-pane fade in" id="saldo">
						<div class="container">

							<!-- Riwayat  Topup -->
							<div class="row">
								<div class="col-12 text-center mb-2">
									<button class="btn btn-sm btn-success shadow btn-rounded">
										Top Up Saldo
									</button>
									<!--<div class="col-md-4 text-right">
											<a href="#" class="js-show-modal1"><i class="fa fa-history"></i> Lihat Semua Transaksi</a>
										</div>-->
								</div>

								<div class="col-md-12 ">
									<div class="row">
										<div class="col-12 px-0">
											<div id="loadhistorytopup">
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- Riwayat Tarik -->
							<div class="row">
								<div class="col-12 mt-3 text-center mb-2">
									<button class="btn btn-sm btn-success shadow btn-rounded">
										Transaksi
									</button>
									<!--<div class="col-md-4 text-right">
											<a href="#" class="js-show-modal1"><i class="fa fa-history"></i> Lihat Semua Transaksi</a>
										</div>-->
								</div>


								<div class="col-md-12">
									<div class="row">
										<div class="col-12 px-0">
											<div id="loadhistorysaldo">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Tab Informasi Akun -->
					<div role="tabpanel" class="tab-pane fade in" id="informasi">
						<div class="container">
							<div class="row">
								<div class="col-12 col-md-6">
									<!--
									<img style="position:absolute;top:10%;right:0;z-index:-1;" src="<?php echo base_url("assets/img/komponen/user-detail.png"); ?>" />
									-->
									<div class="btn btn-sm btn-success shadow btn-rounded">
										profil pengguna
									</div>
									<?php
									$profil = $this->func->getProfil($_SESSION["usrid"], "semua", "usrid");
									$user = $this->func->getUser($_SESSION["usrid"], "semua");
									?>
									<form id="profil">
										<div class="row mt-3">
											<div class="col-12">
												<div class="form-group float-label active">
													<input class="form-control" type="text" name="nama" value="<?php echo $profil->nama; ?>">
													<label class="form-control-label" for="nama">Nama Lengkap</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group float-label active">
													<input class="form-control" type="text" name="email" value="<?php echo $user->username; ?>">
													<label class="form-control-label" for="email">Email</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group float-label active">
													<input class="form-control" type="text" name="nohp" value="<?php echo $profil->nohp; ?>">
													<label class="form-control-label" for="nohp">No Handphone</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<label class="form-control-label" for="kelamin">Jenis Kelamin</label>
													<select class="form-control js-select2" name="kelamin">
														<option value="">Kelamin</option>
														<option value="1" <?php if ($profil->kelamin == 1) {
																				echo "selected";
																			} ?>>Laki-laki</option>
														<option value="2" <?php if ($profil->kelamin == 2) {
																				echo "selected";
																			} ?>>Perempuan</option>
													</select>

													<div class="dropDownSelect2"></div>
												</div>
											</div>
										</div>
										<div class="text-center mb-2 form-group">
											<a href="javascript:void(0)" onclick="simpanProfil()" class="btn btn-md btn-default shadow btn-rounded">
												simpan pengaturan
											</a>
											<span id="profilload" class="cl1" style="display:none;"><i class='fa fa-spin fa-spinner'></i> Menyimpan...</span>
										</div>
									</form>
								</div>

								<div class="col-12 col-md-6">
									<!--
									<img style="position:absolute;top:5%;right:0;z-index:-1;" src="<?php echo base_url("assets/img/komponen/password.png"); ?>" />
									-->
									<div class="btn btn-sm btn-success shadow btn-rounded">
										ganti password
									</div>
									<form id="gantipassword">
										<div class="row">
											<div class="col-12">
												<div class="form-group float-label">
													<input class="form-control" type="password" name="password" value="">
													<label class="form-control-label" for="password">Password Baru</label>
												</div>
											</div>
										</div>
										<div class="text-center mb-2 form-group m-t-50">
											<a href="javascript:void(0)" onclick="simpanPassword()" class="btn btn-md btn-default shadow btn-rounded">
												simpan password
											</a>
											<span id="passwload" class="cl1" style="display:none;"><i class='fa fa-spin fa-spinner'></i> Menyimpan...</span>
										</div>
									</form>
								</div>

							</div>
							<hr>
							<div class="text-center mb-2">
								<button class="btn btn-md btn-default shadow btn-rounded">pilih tema</button>
							</div>
							<div class="col-12 text-center mb-3 theme-color">
								<button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
								<button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
								<button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
								<button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
								<button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
								<button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
							</div>
							<div class="col-12 text-center w-100">
								<div class="row justify-content-center">
									<div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
									<div class="col-auto text-center align-self-center px-0">
										<div class="custom-control custom-switch float-right">
											<input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
											<label class="custom-control-label" for="theme-dark"></label>
										</div>
									</div>
									<div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
								</div>
							</div>

						</div>


					</div>

					<!-- REKENING -->
					<div role="tabpanel" class="tab-pane fade" id="rekening">
						<?php
						$this->db->where("usrid", $_SESSION["usrid"]);
						$db = $this->db->get("rekening");

						if ($db->num_rows() <= 10) {
						?>
							<button data-toggle="modal" data-target="#tambahrekening" class="btn btn-sm btn-success shadow btn-rounded"><i class="material-icons">add</i> Tambah Rekening</button>
						<?php
						}
						?>
						<div class="row">
							<div class="col-lg-12 m-lr-auto m-b-50">
								<div class="m-l-25 m-r--38 m-lr-0-xl">
									<div class="wrap-table-alamat">

										<?php
										foreach ($db->result() as $res) {
										?>

											<ul class="list-items mt-2">
												<li>
													<div class="col-12">
														<div class="row">
															<div class="col-8">
																Nama Bank: <?php echo $this->func->getBank($res->idbank, "nama"); ?>
																<p class="text-secondary small mb-2">Atas Nama: <?php echo $res->atasnama; ?></p>
																<h5 class="text-success font-weight-normal mb-0"><?php echo $res->norek; ?></h5>

																<span>Cabang: <?php echo $res->kcp; ?></span>

															</div>
															<div class="col-4 text-right">
																<a href="javascript:editRekening(<?php echo $res->id; ?>)" class="btn btn-sm btn-success mb-1" title="Edit"><i class="fa fa-edit"></i> Edit</a>
																<a href="javascript:hapusRekening(<?php echo $res->id; ?>)" class="btn btn-sm btn-danger mb-1" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>

															</div>
														</div>
													</div>
												</li>
											</ul>

										<?php
										}
										if ($db->num_rows() == 0) {
											echo "
											<div class='text-center'>
													<p><i class='fa fa-exclamation-triangle'></i> Belum ada daftar rekening, silahkan tambah data untuk menarik saldo.</p>
											</div>
											";
										}
										?>

									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- ALAMAT -->
					<div role="tabpanel" class="tab-pane fade" id="alamat">
						<?php
						$this->db->where("usrid", $_SESSION["usrid"]);
						$db = $this->db->get("alamat");

						if ($db->num_rows() <= 10) {
						?>
							<div class="row">
								<div class="col-md-4 mb-2">
									<a href="javascript:tambahAlamat();"><button class="btn btn-sm btn-success shadow btn-rounded"><i class="material-icons">add</i> Tambah Alamat</button></a>
								</div>
							</div>
						<?php
						}
						?>

						<?php
						foreach ($db->result() as $res) {
						?>

							<div class="row">
								<div class="col-lg-12">
									<ul class="list-items mt-2">
										<li>
											<div class="col-12">
												<div class="row">
													<div class="col-8">
														<?php echo $res->judul; ?> <p class="text-secondary small mb-2">Alamat Utama</p>
														<h5 class="text-success font-weight-normal mb-0"><?php echo $res->nohp; ?></h5>

														<span><?php echo $res->alamat . "<br/><small>Kodepos " . $res->kodepos . "</small>"; ?></span>

													</div>
													<div class="col-4 text-right">
														<a href="javascript:editAlamat(<?php echo $res->id; ?>)" class="btn btn-sm btn-success mb-1" title="Edit"><i class="fa fa-edit"></i> Edit</a>
														<a href="javascript:hapusAlamat(<?php echo $res->id; ?>)" class="btn btn-sm btn-danger mb-1" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>

													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						<?php
						}
						if ($db->num_rows() == 0) {
							echo "<tr><td class='p-all-10 txt-center' colspan=5>
													<p class='cl1'><i class='fa fa-exclamation-triangle'></i> Belum ada alamat tersimpan.</p>
													</td></tr>";
						}
						?>

					</div>
				</div>
			</div>


		</div>
	</div>
	<?php include 'footermobiletukunen.php'; ?>
	</div>
	<!-- jquery, popper and bootstrap js -->
	<!-- jquery, popper and bootstrap js -->
	<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- cookie js -->
	<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

	<!-- swiper js -->
	<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

	<!-- template custom js -->
	<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/js/main.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
	<!-- chosen multiselect js -->
	<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


	<!-- page level script -->
	<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

	<script type="text/javascript">
		$(".js-select2").each(function() {
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
			/* chosen select*/
		});
		$(".chosen").chosen();
	</script>

</body>

</html>

<div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content ">
			<div class="modal-header theme-header border-0">
				<h6 class="">Setting Warna</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0">
				<div class="text-center theme-color">
					<button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
					<button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
					<button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
					<button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
					<button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
					<button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-12 text-center w-100">
					<div class="row justify-content-center">
						<div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
						<div class="col-auto text-center align-self-center px-0">
							<div class="custom-control custom-switch float-right">
								<input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
								<label class="custom-control-label" for="theme-dark"></label>
							</div>
						</div>
						<div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- color chooser menu ends -->

</body>

</html>



<!-- Container -->
<script type="text/javascript">
	$(document).ready(function() { //Make script DOM ready
		$('#myselect').change(function() { //jQuery Change Function
			var opval = $(this).val(); //Get value from select element
			if (opval == "0") { //Compare it and if true
				$('#tambahrekening').modal("show"); //Open Modal
			}
		});
	});
	$(document).ready(function() {
		$("#rekeningidbank").select2({
			dropdownParent: $("#tambahrekening")
		});
	});
	$(document).ready(function() {
		$("#alamatprov").select2({
			dropdownParent: $("#tambahalamat")
		});
	});
	$(document).ready(function() {
		$("#alamatkab").select2({
			dropdownParent: $("#tambahalamat")
		});
	});
	$(document).ready(function() {
		$("#alamatkec").select2({
			dropdownParent: $("#tambahalamat")
		});
	});

	$(document).ready(function() {
		$("#myselect").select2({
			dropdownParent: $("#tariksaldo")
		});
	});
	/* swiper slider carousel */
	var swiper = new Swiper('.menu-profil', {
		slidesPerView: 'auto',
		observer: true,
		observeParents: true,
		spaceBetween: 0,
	});



	$(function() {
		$(".klik").trigger("click");

		$("#loadhistorysaldo").load("<?php echo site_url("assync/getHistoryTarik"); ?>");
		$("#loadhistorytopup").load("<?php echo site_url("assync/getHistoryTopup"); ?>");

		$("#rekeningchange").change(function() {
			if ($(this).val() == 0) {
				$('.wrap-modal2').removeClass("show-modal2");
				$('#tambahrekening').addClass("show-modal2");
				$(this).val("").trigger("change");
			}
		});

		$("#tariksaldo form").on("submit", function(e) {
			e.preventDefault();
			$(".submitbutton", this).parent().append("<span class='cl1'>Memproses...</span>");
			$(".submitbutton", this).hide();
			var submitbtn = $(".submitbutton", this);

			$.post("<?php echo site_url("manage/saldo"); ?>", $(this).serialize(), function(msg) {
				var data = eval("(" + msg + ")");
				if (data.success == true) {
					swal("Berhasil!", "Berhasil menarik saldo, tunggu maks. 2 hari kerja sampai uang Anda masuk ke rekening", "success").then((value) => {
						location.reload();
					});
				} else {
					swal("Gagal!", data.msg, "error");
					submitbtn.show();
					submitbtn.parent().find("span").remove();
				}
			});
		});

		$("#topupsaldo form").on("submit", function(e) {
			e.preventDefault();
			$(".submitbutton", this).parent().append("<span class='cl1'>Memproses...</span>");
			$(".submitbutton", this).hide();
			var submitbtn = $(".submitbutton", this);

			$.post("<?php echo site_url("assync/topupsaldo"); ?>", $(this).serialize(), function(msg) {
				var data = eval("(" + msg + ")");
				if (data.success == true) {
					window.location.href = "<?= site_url("home/topupsaldo") ?>?inv=" + data.idbayar;
				} else {
					swal("Gagal!", data.msg, "error");
					submitbtn.show();
					submitbtn.parent().find("span").remove();
				}
			});
		});

		$("#tambahalamat form").on("submit", function(e) {
			e.preventDefault();
			$(".submitbutton", this).parent().append("<span class='cl1'>Memproses...</span>");
			$(".submitbutton", this).hide();
			var submitbtn = $(".submitbutton", this);

			$.post("<?php echo site_url("assync/tambahalamat"); ?>", $(this).serialize(), function(msg) {
				var data = eval("(" + msg + ")");
				if (data.success == true) {
					swal("Berhasil!", "Berhasil menambah alamat", "success").then((value) => {
						location.reload();
					});
				} else {
					swal("Gagal!", "Gagal menambah alamat baru, silahkan ulangi beberapa saat lagi.", "error");
					submitbtn.show();
					submitbtn.parent().find("span").remove();
				}
			});
		});

		$("#tambahrekening form").on("submit", function(e) {
			e.preventDefault();
			$(".submitbutton", this).parent().append("<span class='cl1'>Memproses...</span>");
			$(".submitbutton", this).hide();
			var submitbtn = $(".submitbutton", this);

			$.post("<?php echo site_url("assync/tambahrekening"); ?>", $(this).serialize(), function(msg) {
				var data = eval("(" + msg + ")");
				if (data.success == true) {
					swal("Berhasil!", "Rekening telah disimpan", "success").then((value) => {
						location.reload();
					});
				} else {
					swal("Gagal!", "Gagal menambah rekening baru, silahkan ulangi beberapa saat lagi.", "error");
					submitbtn.show();
					submitbtn.parent().find("span").remove();
				}
			});
		});

		$("#alamatprov").change(function() {
			changeKab($(this).val(), "");
		});

		$("#alamatkab").change(function() {
			changeKec($(this).val(), "");
		});

	});

	function copyLink() {
		$("#copylink").select();
		document.execCommand("copy");
		swal("Berhasil menyalin!", "silahkan paste/tempel dan kirim alamat yg sudah disalin ke teman Anda", "success");
	}

	// FORM CHANGING
	function changeProv(proval, callback) {
		$("#alamatprov").val(proval).trigger("change");
		if (callback) callback();
	}

	function changeKec(kabval, valu, callback) {
		$("#alamatkec").html("<option value=''>Loading...</option>").trigger("change");
		$.post("<?php echo site_url("assync/getkec"); ?>", {
			"id": kabval
		}, function(msg) {
			var data = eval("(" + msg + ")");
			$("#alamatkec").html(data.html).promise().done(function() {
				$("#alamatkec").val(valu);
			});
		});
		if (callback) callback();
	}

	function changeKab(proval, valu, callback) {
		$("#alamatkab").html("<option value=''>Loading...</option>").trigger("change");
		$("#alamatkec").html("<option value=''>Kecamatan</option>").trigger("change");

		$.post("<?php echo site_url("assync/getkab"); ?>", {
			"id": proval
		}, function(msg) {
			var data = eval("(" + msg + ")");
			$("#alamatkab").html(data.html).promise().done(function() {
				$("#alamatkab").val(valu);
			})
		});
		if (callback) callback();
	}

	// REKENING
	function tambahRekening() {
		$('.wrap-modal2').removeClass("show-modal2");
		$('#tambahrekening').addClass("show-modal2");
	}

	function editRekening(rek) {
		$.post("<?php echo site_url("assync/getRekening"); ?>", {
			"rek": rek
		}, function(msg) {
			var data = eval("(" + msg + ")");

			if (data.success == true) {
				$("#rekeningid").val(rek);
				$("#rekeningidbank").val(data.idbank).trigger("change");
				$("#rekeningatasnama").val(data.atasnama);
				$("#rekeningnorek").val(data.norek);
				$("#rekeningkcp").val(data.kcp);
				$('#tambahrekening').modal("show"); //Open Modal

			} else {
				swal("Error!", "terjadi kesalahan silahkan ulangi beberapa saat lagi.", "error");
			}
		});
	}

	function hapusRekening(rek) {
		swal({
				title: "Anda yakin?",
				text: "menghapus rekening ini dari akun Anda?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/hapusRekening"); ?>", {
						"rek": rek
					}, function(msg) {
						var data = eval("(" + msg + ")");

						if (data.success == true) {
							swal("Berhasil!", "Berhasil menghapus rekening", "success").then((value) => {
								location.reload();
							});
						} else {
							swal("Error!", "terjadi kesalahan silahkan ulangi beberapa saat lagi.", "error");
						}
					});
				}
			});
	}

	function batalTopup(rek) {
		swal({
				title: "Anda yakin?",
				text: "membatalkan topup saldo ini?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/bataltopup"); ?>", {
						"id": rek
					}, function(msg) {
						var data = eval("(" + msg + ")");

						if (data.success == true) {
							swal("Berhasil!", "Berhasil membatalkan topup saldo", "success").then((value) => {
								location.reload();
							});
						} else {
							swal("Error!", "terjadi kesalahan silahkan ulangi beberapa saat lagi.", "error");
						}
					});
				}
			});
	}

	// ALAMAT
	function tambahAlamat() {
		$("#alamatid").val(0);
		$("#alamatnama").val("");
		$("#alamatnohp").val("");
		$("#alamatstatus").val(0).trigger("change");
		$("#alamatalamat").val("");
		$("#alamatkodepos").val("");
		$("#alamatjudul").val("");
		$("#alamatprov").val("").trigger("change");
		$('.wrap-modal2').removeClass("show-modal2");
		$('#tambahalamat').modal("show");
	}

	function editAlamat(rek) {
		$.post("<?php echo site_url("assync/getAlamat"); ?>", {
			"rek": rek
		}, function(msg) {
			var data = eval("(" + msg + ")");

			if (data.success == true) {
				changeProv(data.prov),
					changeKab(data.prov, data.kab),
					changeKec(data.kab, data.idkec);
				$("#alamatid").val(rek);
				$("#alamatnama").val(data.nama);
				$("#alamatnohp").val(data.nohp);
				$("#alamatstatus").val(data.status).trigger("change");
				$("#alamatalamat").val(data.alamat);
				$("#alamatkodepos").val(data.kodepos);
				$("#alamatjudul").val(data.judul);
				$('.wrap-modal2').removeClass("show-modal2");
				$('#tambahalamat').modal("show");
			} else {
				swal("Error!", "terjadi kesalahan silahkan ulangi beberapa saat lagi.", "error");
			}
		});
	}

	function hapusAlamat(rek) {
		swal({
				title: "Anda yakin?",
				text: "menghapus alamat ini dari akun Anda?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.post("<?php echo site_url("assync/hapusAlamat"); ?>", {
						"rek": rek
					}, function(msg) {
						var data = eval("(" + msg + ")");

						if (data.success == true) {
							swal("Berhasil!", "Berhasil menghapus alamat", "success").then((value) => {
								location.reload();
							});
						} else {
							swal("Error!", "terjadi kesalahan silahkan ulangi beberapa saat lagi.", "error");
						}
					});
				}
			});
	}

	// SALDO
	function topupSaldo() {
		$('#topupsaldo').addClass('show-modal2');
	}

	function tarikSaldo() {
		$('#tariksaldo').addClass('show-modal2');
	}

	function historySaldo(page) {
		$("#loadhistorysaldo").html("Loading...");
		$("#loadhistorysaldo").load("<?php echo site_url("assync/getHistoryTarik"); ?>?page=" + page);
	}

	function getopupSaldo(page) {
		$("#loadhistorysaldo").html("Loading...");
		$("#loadhistorysaldo").load("<?php echo site_url("assync/getHistoryTopup"); ?>?page=" + page);
	}

	function simpanProfil() {
		$("#profil a").hide();
		$("#profilload").show();
		$.post("<?php echo site_url("assync/updateprofil"); ?>", $("#profil").serialize(), function(msg) {
			var data = eval("(" + msg + ")");
			$("#profil a").show();
			$("#profilload").hide();
			if (data.success == true) {
				swal("Berhasil!", "Berhasil menyimpan informasi pengguna", "success");
			} else {
				swal("Gagal!", "Gagal menyimpan informasi pengguna", "error");
			}
		});
	}

	function simpanPassword() {
		$("#gantipassword a").hide();
		$("#passwload").show();
		$.post("<?php echo site_url("assync/updatepass"); ?>", $("#gantipassword").serialize(), function(msg) {
			var data = eval("(" + msg + ")");
			$("#gantipassword a").show();
			$("#passwload").hide();
			if (data.success == true) {
				$("#gantipassword input").val("");
				swal("Berhasil!", "Berhasil menyimpan password baru", "success");
			} else {
				swal("Gagal!", "Gagal menyimpan informasi password", "error");
			}
		});
	}
</script>


<!-- Modal3-Topup Saldo -->
<div class="modal fade" id="topupsaldo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<form method="POST" action="<?= site_url("manage/topupsaldo") ?>">

		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><b>Top Up Saldo</b></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="col-md-12">
					<div class="form-group row">
						<div class="col-sm-3 mt-2 text-center">
							<label class="btn btn-sm btn-success">Jumlah</label>
						</div>
						<div class="col-md-9 mt-1">
							<input class="form-control" type="text" id="jumlahtopup" name="jumlah" placeholder="Rp" required>
						</div>
						<div class="col-6 col-md-3 text-center">
							<button type="button" class="btn btn-md btn-success shadow btn-rounded mb-3 mt-3" onclick="$('#jumlahtopup').val(50000)">50.000</button>
						</div>
						<div class="col-6 col-md-3 text-center">
							<button type="button" class="btn btn-md btn-success shadow btn-rounded mb-3 mt-3" onclick="$('#jumlahtopup').val(100000)">100.000</button>
						</div>
						<div class="col-6 col-md-3 text-center">
							<button type="button" class="btn btn-md btn-success shadow btn-rounded mb-3 mt-3" onclick="$('#jumlahtopup').val(150000)">150.000</button>
						</div>
						<div class="col-6 col-md-3 text-center">
							<button type="button" class="btn btn-md btn-success shadow btn-rounded mb-3 mt-3" onclick="$('#jumlahtopup').val(200000)">200.000</button>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-md btn-default shadow btn-rounded mb-3 mt-3"><i class="material-icons mb-18 mr-2">add</i>top up </button>
				</div>
			</div>
		</div>
	</form>
</div>

<!-- Modal3-Tarik Saldo -->
<div class="modal fade" id="tariksaldo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<form>
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h4 class="modal-title"><b>Penarikan Saldo</b></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="col-md-12">
					<div class="form-group row mt-3">
						<div class="col-sm-4 mt-1">
							<label class="btn btn-sm btn-success">Jumlah</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="text" name="jumlah" placeholder="Rp" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4 mt-1">
							<label class="btn btn-sm btn-success">Rek Tujuan</label>
						</div>
						<div class="col-md-8">
							<select class="form-control js-select2" id="myselect" name="idrek" required>
								<option value="" id='defaultrek'>Pilih Rekening</option>
								<?php
								$this->db->where("usrid", $_SESSION["usrid"]);
								$db = $this->db->get("rekening");
								foreach ($db->result() as $res) {
									echo "<option value='" . $res->id . "'>" . $res->norek . " - " . $res->atasnama . "</option>";
								}
								?>
								<option value="0">+ Tambah Rekening Baru</option>
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4 mt-1">
							<label class="btn btn-sm btn-success">Catatan</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="text" name="keterangan" placeholder="Catatan">
						</div>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-md btn-default shadow btn-rounded mb-3 mt-3"><i class="material-icons mb-18 mr-2">remove</i>tarik saldo </button>
				</div>
			</div>
		</div>
	</form>
</div>

<!-- Modal3-Tambah Rekening -->
<div class="modal fade" id="tambahrekening" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<form>
		<input type="hidden" name="id" id="rekeningid" value="0" />
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><b>Rekening</b></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="col-md-12">
					<div class="form-group row">
						<div class="col-sm-4 mt-2">
							<label class="btn btn-sm btn-success">Bank</label>
						</div>
						<div class="col-md-8 mt-2">
							<select class="form-control js-select2" id="rekeningidbank" name="idbank" required>
								<option value="">Pilih Bank</option>
								<?php
								$db = $this->db->get("rekeningbank");
								foreach ($db->result() as $res) {
									echo "<option value='" . $res->id . "'>" . $res->nama . "</option>";
								}
								?>
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>

					<div class="form-group row mt-3">
						<div class="col-sm-4 mt-1">
							<label class="btn btn-sm btn-success">No Rek</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" id="rekeningnorek" type="text" name="norek" placeholder="" required>
						</div>
					</div>
					<div class="form-group row mt-3">
						<div class="col-sm-4 mt-1">
							<label class="btn btn-sm btn-success">Atas Nama</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" id="rekeningatasnama" type="text" name="atasnama" placeholder="" required>
						</div>
					</div>
					<div class="form-group row mt-3">
						<div class="col-sm-4 mt-1">
							<label class="btn btn-sm btn-success">Cabang</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" id="rekeningkcp" type="text" name="kcp" placeholder="" required>
						</div>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-md btn-default shadow btn-rounded mb-3 mt-3"><i class="material-icons mb-18 mr-2">add</i>Simpan Rekening </button>
					</div>

				</div>
			</div>
		</div>
	</form>
</div>

<!-- Modal3-Tambah Rekening -->
<div class="modal fade" id="tambahalamat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

	<form>
		<input type="hidden" name="id" id="alamatid" value="0" />
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><b>Alamat</b></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Simpan sebagai? <small>cth: Alamat Rumah, Alamat Kantor, dll</small></label>
							<input class="form-control stext-111 cl2 plh3 size-116" id="alamatjudul" type="text" name="judul" placeholder="" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Nama Penerima</label>
							<input class="form-control stext-111 cl2 plh3 size-116" id="alamatnama" type="text" name="nama" placeholder="" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">No Handphone</label>
							<input class="form-control stext-111 cl2 plh3 size-116" id="alamatnohp" type="text" name="nohp" placeholder="" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Alamat Lengkap</label>
							<input class="form-control stext-111 cl2 plh3 size-116" id="alamatalamat" type="text" name="alamat" placeholder="" required>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Provinsi</label>
							<select class="form-control js-select2" id="alamatprov" required>
								<option value="">Pilih Provinsi</option>
								<?php
								$db = $this->db->get("prov");
								foreach ($db->result() as $res) {
									echo "<option value='" . $res->id . "'>" . $res->nama . "</option>";
								}
								?>
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Kabupaten</label>
							<select class="form-control js-select2" id="alamatkab" required>
								<option value="">Pilih Kabupaten/Kota</option>
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Kecamatan</label>
							<select class="form-control js-select2" id="alamatkec" name="idkec" required>
								<option value="">Pilih Kecamatan</option>
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Kodepos</label>
							<input class="form-control stext-111 cl2 plh3 size-116" id="alamatkodepos" type="text" name="kodepos" placeholder="" required>
						</div>
					</div>
					<div class="row m-t-20">
						<div class="col-md-12">
							<label class="mtext-102 control-label" style="padding-top: 10px;">Simpan Sebagai</label>
							<select class="form-control js-select2" id="alamatstatus" name="status" required>
								<option value="0">Alamat</option>
								<option value="1">Alamat Utama</option>
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-md btn-default shadow btn-rounded mb-3 mt-3"><i class="material-icons mb-18 mr-2">add</i>Simpan </button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>