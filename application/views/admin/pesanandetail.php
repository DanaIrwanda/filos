<body>

	<?php include 'loader.php'; ?>
	<div class="sidebar">
		<?php include 'menutukunen.php'; ?>
	</div>
	<div class="wrapper">
		<?php include 'menuheadtukunen.php'; ?>

		<div class="container">
			<div class="row card">
				<div class="col-12">
					<h4 class="mt-3 text-center">
						Order ID <span class="cl2"><?php echo $transaksi->orderid; ?></span>
					</h4>
					<hr>
					<div class="row">
						<div class="col-6 status-pesanan">
							<b>Waktu Pemesanan:</b><br />
							<p class="badge badge-success"><?php echo $this->func->ubahTgl("d M Y H:i", $transaksi->tgl); ?> WIB</p><br />
							<b>Waktu Pembayaran:</b><br />
							<p class="badge badge-success mb-2"><?php echo $this->func->ubahTgl("d M Y H:i", $bayar->tgl); ?> WIB</p>

						</div>
						<div class="col-6">
							<?php if ($transaksi->status == 0) { ?>
								<!-- Belum Dibayar -->
								<p class="btn btn-md btn-danger">Belum Dibayar</p>
								<p class="mb-3 small">segera lakukan pembayaran maks. 1x24jam untuk menghindari pembatalan otomatis.</p>
							<?php } elseif ($transaksi->status == 1) { ?>
								<!-- Dalam Pengiriman -->
								<p class="btn btn-md btn-warning">Menunggu Konfirmasi</p>
								<p class="mb-3 small">menunggu konfirmasi pesanan dari penjual.</p>
							<?php } elseif ($transaksi->status == 2 and $transaksi->resi != "") { ?>
								<!-- Dalam Pengiriman -->
								<p class="btn btn-md btn-success">Sedang Dikirim</p>
								<p class="mb-3 small">pesanan Anda sudah dalam perjalanan, untuk melihat proses pengiriman silahkan cek info dibawah.</p>
							<?php } elseif ($transaksi->status == 2) { ?>
								<!-- Sedang Dikemas -->
								<p class="btn btn-md btn-warning">Sedang Dikemas</p>
								<p class="mb-3 small">pesanan sedang dikemas oleh penjual dan akan segera dikirim.</p>
							<?php } elseif ($transaksi->status == 3) { ?>
								<!-- Selesai -->
								<p class="btn btn-md btn-success">Telah Diterima</p>
								<p class="mb-3 small">pesanan telah diterima oleh pembeli.</p>
							<?php } elseif ($transaksi->status == 4) { ?>
								<!-- Selesai -->
								<p class="btn btn-md btn-danger">Pesanan Dibatalkan</p>
								<p class="mb-3 small">pesanan dibatalkan karena <?php echo $transaksi->keterangan; ?></p>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row card">
				<div class="col-12">
					<h4 class="text-center mt-2">
						Informasi Pengiriman
						<?php
						$alamat = $this->func->getAlamat($transaksi->alamat, "semua");
						$kec = $this->func->getKec($alamat->idkec, "semua");
						$kab = $this->func->getKab($kec->idkab, "nama");
						?>
					</h4>
					<hr>

					<div class="mb-3">
						<div class="row">
							<div class="col-md-6">
								<h5 class="text-black p-b-10">KURIR & PAKET</h5>
								<p>
									<?php
									echo strtoupper($this->func->getKurir($transaksi->kurir, "nama", "rajaongkir")) . "<br/>" . $this->func->getPaket($transaksi->paket, "nama", "rajaongkir");
									?>
								</p>
							</div>
							<div class="col-md-6">
								<h5 class="text-black p-b-10">RESI PENGIRIMAN</h5>
								<p><b class="color1"><?php echo $transaksi->resi; ?></b></p>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-md-6">
								<h5 class="text-black p-b-10">Nama Penerima</h5>
								<p><?php echo strtoupper(strtolower($alamat->nama)); ?></p>
							</div>
							<div class="col-md-6">
								<h5 class="text-black p-b-10">No Telepon</h5>
								<p><?php echo $alamat->nohp; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h5 class="text-black p-b-10">Alamat Pengiriman</h5>
								<p>
									<?php echo strtoupper(strtolower($alamat->alamat)); ?><br>
									<?php echo $kec->nama . ", " . $kab; ?><br>
									Kodepos <?php echo $alamat->kodepos; ?>
								</p>
							</div>
						</div>
					</div>
					<?php if ($transaksi->resi != "" and $transaksi->kurir != "cod" and $transaksi->kurir != "toko") { ?>
						<hr class="mb-3" />
						<a href="<?php echo site_url("manage/lacakpaket/" . $transaksi->orderid); ?>" class="btn btn-sm btn-success btn-rounded shadow mb-3"><i class="fa fa-truck fa-blink ml-2"></i> &nbsp;<b>CEK STATUS PENGIRIMAN</b></a>
					<?php } ?>
				</div>
			</div>

			<div class="row card">
				<div class="col-12 px-0">
					<h4 class="text-center mt-2">
						Produk Pesanan
					</h4>
					<div class="col-12 px-1">
						<?php
						$this->db->where("idtransaksi", $transaksi->id);
						$db = $this->db->get("transaksiproduk");
						$total = 0;
						foreach ($db->result() as $res) {
							$total += $res->harga * $res->jumlah;
							$produk = $this->func->getProduk($res->idproduk, "semua");
							$variasee = $this->func->getVariasi($res->variasi, "semua");
							$variasi = ($res->variasi != 0 and $variasee->warna) ? $this->func->getWarna($variasee->warna, "nama") . " " . $produk->subvariasi . " " . $this->func->getSize($variasee->size, "nama") : "";
							$variasi = ($res->variasi != 0) ? "<br/><small class='text-primary'>" . $produk->variasi . ": " . $variasi . "</small>" : "";
						?>
							<div class="show-product">
								<ul class="list-items mb-1">
									<li>
										<div class="col-12">
											<div class="row">
												<div class="card shadow-sm border-0">
													<div class="card-body">
														<figure class="product-image"><img src="<?php echo $this->func->getFoto($res->idproduk, "utama"); ?>" alt="produk" class=""></figure>
													</div>
												</div>
												<div class="col">
													<p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
																								echo $produk->nama;
																							} else {
																								echo "Produk telah dihapus";
																							} ?></p>
													<?= $variasi ?>
													</p>
													<h5 class="text-success font-weight-normal mb-0">Rp <?php echo $this->func->formUang($res->harga); ?> <span style="font-size:11px">x<?php echo $res->jumlah; ?></span></h5>
													<p class="text-secondary small mb-2">Total: Rp <?php echo $this->func->formUang($total); ?> + Ongkir Rp <?php echo $this->func->formUang($transaksi->ongkir);
																																							$total += $transaksi->ongkir; ?></p>
													<h5 class="text-secondary">Rp <?php echo $this->func->formUang($total); ?></h5>

												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
					</div>
				<?php
						}
				?>
				</div>
			</div>
		</div>


	</div>
	</div>
	<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- cookie js -->
	<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

	<!-- swiper js -->
	<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

	<!-- template custom js -->
	<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/js/main.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
	<!-- chosen multiselect js -->
	<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


	<!-- page level script -->
	<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

	<script type="text/javascript">
		$(".js-select2").each(function() {
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
			/* chosen select*/
		});
		$(".chosen").chosen();
	</script>

</body>