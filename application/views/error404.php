<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="sidebar">
	<?php include 'menutukunen.php'; ?>
</div>
<div class="wrapper">
	<?php include 'menuheadtukunen.php'; ?>
	<div class="container">
		<p>
			<h2 class="cl2 stext-301" style="font-size:400%;">404</h2>
		</p>
		Maaf, halaman yang anda cari tidak ditemukan atau sedang dalam proses pengembangan. Silahkan laporkan ke admin apabila
		kendala ini terjadi berulang kali.
		<div class="text-center">
			<a href="javascript:history.back()" class="btn btn-md btn-rounded shadow">
				<i class="fa fa-chevron-left"></i> &nbsp;Halaman sebelumnya
			</a>
		</div>
	</div>
</div>
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
	$(".js-select2").each(function() {
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});
		/* chosen select*/
	});
	$(".chosen").chosen();
</script>