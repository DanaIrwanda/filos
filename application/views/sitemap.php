<?php
header('Content-type: application/xml; charset="ISO-8859-1"', true);
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

	<?php
	defined('BASEPATH') or exit('No direct script access allowed');
	$set = $this->func->globalset("semua");
	$nama = (isset($titel)) ? $set->nama . " " . $titel : $set->nama . " " . $set->slogan;
	?>


	<url>
		<loc><?= $nama; ?></loc>
		<lastmod><?= date('d-m-Y H:i:s') ?></lastmod>
		<changefreq>daily</changefreq>
		<priority>0.1</priority>
	</url>


	<?php
	$this->db->where("parent", 0);
	$db = $this->db->get("kategori");
	foreach ($db->result() as $r) {
	?>

		<url>
			<loc><?= site_url("kategori/" . $r->url) ?></loc>
			<changefreq>daily</changefreq>
			<priority>0.1</priority>
		</url>


	<?php
	}
	?>

	<?php
	$this->db->select("SUM(stok),idproduk");
	$this->db->group_by("idproduk");
	$dbvar = $this->db->get("produkvariasi");
	$notin = array();
	foreach ($dbvar->result() as $not) {
		$notin[] = $not->idproduk;
	}

	$where = "(nama LIKE '%$cari%' OR harga LIKE '%$cari%' OR hargareseller LIKE '%$cari%' OR hargaagen LIKE '%$cari%' OR deskripsi LIKE '%$cari%') AND status = 1 AND preorder != 1";
	$this->db->where($where);
	if (count($notin) > 0) {
		$this->db->where_not_in($notin);
	}
	$dbs = $this->db->get("produk");

	$this->db->where($where);
	if (count($notin) > 0) {
		$this->db->where_not_in($notin);
	}
	$this->db->limit($perpage, ($page - 1) * $perpage);
	$this->db->order_by($orderby);
	$db = $this->db->get("produk");
	$totalproduk = 0;

	foreach ($db->result() as $r) {
		$level = isset($_SESSION["lvl"]) ? $_SESSION["lvl"] : 0;
		if ($level == 5) {
			$result = $r->hargadistri;
		} elseif ($level == 4) {
			$result = $r->hargaagensp;
		} elseif ($level == 3) {
			$result = $r->hargaagen;
		} elseif ($level == 2) {
			$result = $r->hargareseller;
		} else {
			$result = $r->harga;
		}
		$ulasan = $this->func->getReviewProduk($r->id);

		$this->db->where("idproduk", $r->id);
		$dbv = $this->db->get("produkvariasi");
		$totalstok = ($dbv->num_rows() > 0) ? 0 : $r->stok;
		$hargs = 0;
		$harga = array();
		foreach ($dbv->result() as $rv) {
			$totalstok += $rv->stok;
			if ($level == 5) {
				$harga[] = $rv->hargadistri;
			} elseif ($level == 4) {
				$harga[] = $rv->hargaagensp;
			} elseif ($level == 3) {
				$harga[] = $rv->hargaagen;
			} elseif ($level == 2) {
				$harga[] = $rv->hargareseller;
			} else {
				$harga[] = $rv->harga;
			}
			$hargs += $rv->harga;
		}

		if ($totalstok > 0) {
			$totalproduk += 1;
	?>

			<url>
				<loc><?= site_url('produk/' . $r->url) ?></loc>
				<lastmod><?= $r->tglupdate ?></lastmod>
				<changefreq>daily</changefreq>
				<priority>0.1</priority>
			</url>

	<?php
		}
	} ?>


	<?php
	$db = $this->db->get("blog");

	if ($db->num_rows() > 0) {
		foreach ($db->result() as $blog) {
	?>
			<url>
				<loc><?= site_url('blog/' . $blog->url) ?></loc>
				<lastmod><?= $blog->tgl ?></lastmod>
				<changefreq>daily</changefreq>
				<priority>0.1</priority>
			</url>
	<?php
		}
	} else {
	}
	?>
</urlset>