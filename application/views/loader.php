<div class="row no-gutters vh-100 loader-screen">
    <div class="col align-self-center text-white text-center">

        <?php
        defined('BASEPATH') or exit('No direct script access allowed');
        $set = $this->func->globalset("semua");
        ?>

        <img src="<?= base_url("admin/assets/img/" . $set->favicon) ?>" alt="logo <?= base_url(); ?>">
        <?php
        ?>
        <h4><span class="font-weight-light"><?= $set->nama ?></h4>
        <div class="laoderhorizontal">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>