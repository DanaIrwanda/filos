<?php if ($this->func->cekLogin() == true) { ?>
  <div class="text-center">
    <?php
    $profil = $this->func->getProfil($_SESSION["usrid"], "semua", "usrid");
    $user = $this->func->getUser($_SESSION["usrid"], "semua");
    ?>

    <h5 class="mb-1 mt-5"><?php echo $profil->nama; ?></h5>
    <p class="text-mute small"><?php echo $profil->nohp; ?></p>
  </div>
  <br>
  <div class="row mx-0">
    <div class="col">
      <div class="card mb-3 border-0 shadow-sm bg-template-light">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <p class="text-secondary small mb-0">Saldo</p>
              <h6 class="text-dark my-0">Rp <?php echo $this->func->formUang($this->func->getSaldo($_SESSION["usrid"], "saldo", "usrid")); ?>,-</h6>
            </div>
            <div class="col-auto">
              <a href="<?= site_url("manage") ?>"><button class="btn btn-default button-rounded-36 shadow"><i class="material-icons">add</i></button></a>
            </div>
          </div>
        </div>
      </div>
      <h5 class="subtitle text-uppercase"><span>Menu</span></h5>
      <div class="list-group main-menu">
        <a href="<?= site_url("manage") ?>" class="list-group-item list-group-item-action active">Dashboard<i class="material-icons float-right">chevron_right</i></a>
        <a href="<?= site_url("manage/pesanan") ?>" class="list-group-item list-group-item-action active">Pesanan<i class="material-icons float-right">chevron_right</i></a>
        <a href="" data-toggle="modal" data-target="#colorscheme" class="list-group-item list-group-item-action active">Setting Warna<i class="material-icons float-right">chevron_right</i></a>
        <a href="<?= site_url("home/signout") ?>" class="list-group-item list-group-item-action mt-4 active">Logout<i class="material-icons float-right">chevron_right</i></a>
      </div>
    </div>
  </div>
<?php } else { ?>

  <div class="text-center">
    <h5 class="mb-1 mt-5">Silahkan login</h5>
  </div>
  <br>
  <div class="row mx-0">
    <div class="col">
      <div class="card mb-3 border-0 shadow-sm bg-template-light">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <p class="text-secondary small mb-0">Saldo</p>
              <h6 class="text-dark my-0">Rp <?php echo $this->func->formUang($this->func->getSaldo($_SESSION["usrid"], "saldo", "usrid")); ?>,-</h6>
            </div>
            <div class="col-auto">
              <a href="<?= site_url("manage") ?>"><button class="btn btn-default button-rounded-36 shadow"><i class="material-icons">add</i></button></a>
            </div>
          </div>
        </div>
      </div>
      <h5 class="subtitle text-uppercase"><span>Menu</span></h5>
      <div class="list-group main-menu">
        <a href="<?= site_url("manage") ?>" class="list-group-item list-group-item-action active">Dashboard<i class="material-icons float-right">chevron_right</i></a>
        <a href="<?= site_url("manage/pesanan") ?>" class="list-group-item list-group-item-action active">Pesanan<i class="material-icons float-right">chevron_right</i></a>
        <p data-toggle="modal" data-target="#colorscheme" class="list-group-item list-group-item-action active">Setting Warna<i class="material-icons float-right">chevron_right</i></p>
        <a href="<?= site_url("home/signin") ?>" class="list-group-item list-group-item-action mt-4 active">Login<i class="material-icons float-right">chevron_right</i></a>
      </div>
    </div>
  </div>
<?php } ?>