<?php
$set = $this->func->globalset("semua");

?>
<div class="container">



	<div class="vh-100 proh bg-template">
		<div class="col align-self-center px-3 text-center" id="load">
			<img src="<?= base_url("admin/assets/img/" . $set->logo) ?>" alt="logo" class="logo-small">
			<h2 class="text-white "><span class="font-weight-light">Daftar</span><?= $set->nama ?></h2>

			<form id="signup" class="form-signin shadow">
				<div class=" form-group float-label">
					<input class="form-control" type="text" id="nama" name="nama" required>
					<label for="nama" class="form-control-label">Nama</label>
				</div>
				<div class="form-group float-label">
					<input onkeypress="return isNumber(event)" class="form-control" type="text" name="nohp" required>
					<label for="nohp" class="form-control-label">Nomor Hp</label>
				</div>
				<div class="form-group float-label">
					<input class="form-control" type="text" id="email" name="email" required>
					<label for="Email" class="form-control-label">Alamat Email</label>
				</div>
				<p id="imelerror" style="color:#cc0000;display:none;"><small>terjadi kesalahan, mohon formulir dilengkapi dulu</small></p>
				<div class="form-group float-label">
					<input class="form-control" type="password" name="pass" required>
					<label for="pass" class="form-control-label">password</label>
				</div>
				<div class="rs1-select2 rs2-select2 mb-2">
					<select class="form-control js-select2" name="kelamin" required>
						<option value="">Jenis Kelamin</option>
						<option value="1">Laki - laki</option>
						<option value="2">Perempuan</option>
					</select>
					<div class="dropDownSelect2"></div>
				</div>
				<div class="rs1-select2 rs2-select2 mb-2">
					<select class="form js-select2" name="tgl" required>
						<option value="">Tanggal lahir</option>
						<?php
						for ($i = 1; $i <= 31; $i++) {
							$a = ($i < 10) ? 0 . $i : $i;
							echo '<option value="' . $a . '">' . $i . '</option>';
						}
						?>
					</select>
					<div class="dropDownSelect2"></div>
				</div>
				<div class="rs1-select2 rs2-select2 mb-2">
					<select class="form js-select2" name="bln" required>
						<option value="00">Bulan</option>
						<option value="01">Januari</option>
						<option value="02">Februari</option>
						<option value="03">Maret</option>
						<option value="04">April</option>
						<option value="05">Mei</option>
						<option value="06">Juni</option>
						<option value="07">Juli</option>
						<option value="08">Agustus</option>
						<option value="09">September</option>
						<option value="10">Oktober</option>
						<option value="11">November</option>
						<option value="12">Desember</option>
					</select>
					<div class="dropDownSelect2"></div>
				</div>
				<div class="rs1-select2 rs2-select2 mb-2">
					<select class="form js-select2" name="thn" required>
						<option value="">Tahun</option>
						<?php
						$awal = date("Y") - 65;
						$akhir = date("Y") - 17;
						for ($i = $akhir; $i >= $awal; $i--) {
							echo '<option value="' . $i . '">' . $i . '</option>';
						}
						?>
					</select>
					<div class="dropDownSelect2"></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="proses" style="display:none;">
							<h5 class="cl1"><i class="fa fa-circle-o-notch fa-spin"></i> Memproses...</h5>
						</div>
						<button id="submit" type="submit" class="btn btn-default shadow btn-rounded">
							DAFTAR
						</button>
						<p class="text-center m-t-15">Sudah punya akun? <a href="<?php echo site_url("home/signin"); ?>">Login</a></p>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</form>
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
	$(".js-select2").each(function() {
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});
		/* chosen select*/
	});
	$(".chosen").chosen();
</script>


<script type="text/javascript">
	function validation() {
		return 0;
	}

	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}

	$(".email").each(function() {
		if ($(this).val() != "") {
			$(this).trigger("change");
		}
	});

	$(function() {
		localStorage["error"] = 0;

		$("#signup").on("submit", function(e) {
			e.preventDefault();

			if (validation() == 0) {
				$("input,select").prop("readonly", true);
				$("#proses").show();
				$("#submit").hide();
				//	$("#submit").html("<i class='fa fa-spin fa-spinner'></i> tunggu sebentar...");
				$.post("<?php echo site_url("home/signup"); ?>", $(this).serialize(), function(msg) {
					fbq('track', 'CompleteRegistration', {
						content_name: $("#nama").val()
					});
					$("#load").html(msg);
					$('html, body').animate({
						scrollTop: $("#load").offset().top - 300
					});
				});
			} else {
				$("#error").show();
			}
		});

		$("#email").change(function() {
			if ($(this).val().indexOf("@") != -1 && $(this).val().indexOf(".") != -1) {
				$.post("<?php echo site_url("home/signup/cekemail"); ?>", {
					"email": $("#email").val()
				}, function(msg) {
					var result = eval('(' + msg + ')');
					if (result.success == true) {
						$("#imelerror").hide();
					} else {
						$("#imelerror").show();
						$("#imelerror small").html(result.message);
					}
				});
			} else {
				$("#imelerror").show();
				$("#imelerror small").html("masukkan format email dengan benar");
			}
		});

	});
</script>