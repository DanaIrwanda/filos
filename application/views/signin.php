<?php
$set = $this->func->globalset("semua");

?>
<!-- breadcrumb -->
<div class="container">
	<div class="row no-gutters vh-100 proh bg-template">
		<div class="col align-self-center px-3 text-center" id="load">
			<img src="<?= base_url("admin/assets/img/" . $set->logo) ?>" alt="logo" class="logo-small">
			<h2 class="text-white "><span class="font-weight-light">Log</span>In</h2>
			<form id="signin" method="post" class="form-signin shadow">
				<div class="form-group float-label">
					<input type="email" name="email" class="form-control" required autofocus>
					<label for="inputEmail" class="form-control-label">Alamat Email</label>
				</div>

				<div class="form-group float-label">
					<input type="password" name="pass" class="form-control" required>
					<label for="inputPassword" class="form-control-label">Password</label>
				</div>

				<div class="form-group my-4 text-left">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" name="remember" class="custom-control-input" id="checkbox6">
						<label class="custom-control-label" for="checkbox6">Ingat Saya</label>
					</div>
				</div>

				<div class="row">
					<div class="col-auto">
						<button type="submit" class="btn btn-sm btn-default btn-rounded shadow"><span>Log in</span><i class="material-icons">arrow_forward</i></button>
					</div>
					<div class="col-auto">
						<a href="javascript:void(0)" id="reset"><button type="submit" class="btn btn-sm btn-default btn-rounded shadow"><span>lupa password</span><i class="material-icons">arrow_forward</i></button></a>
					</div>
				</div>
			</form>
			<p class="text-center text-white">
				Belum punya akun<br>
				<a href="<?php echo site_url("home/signup"); ?>">Daftar</a> disini.
			</p>
		</div>
	</div>
	<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- cookie js -->
	<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

	<!-- swiper js -->
	<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

	<!-- template custom js -->
	<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/js/main.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
	<!-- chosen multiselect js -->
	<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


	<!-- page level script -->
	<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

	<script type="text/javascript">
		$(".js-select2").each(function() {
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
			/* chosen select*/
		});
		$(".chosen").chosen();
	</script>

	<script type="text/javascript">
		$(function() {
			$("#signin").on("submit", function(e) {
				e.preventDefault();

				var submit = $("#submit").html();
				$(".form").prop("readonly", true);
				$("#submit").html("<i class='fa fa-spin fa-spinner'></i> tunggu sebentar...");
				$.post("<?php echo site_url("home/signin"); ?>", $(this).serialize(), function(msg) {
					var data = eval('(' + msg + ')');
					if (data.success == true) {
						window.location.href = data.redirect;
					} else {
						$("#submit").html(submit);
						swal("Warning!", "alamat email atau password salah, silahkan cek kembali", "error");
					}
				});
			});

			$("#reset").click(function() {
				$("#load").html("<i class='fa fa-spin fa-spinner'></i> mohon tunggu sebentar...");
				$("#load").load("<?php echo site_url("home/signin/pwreset"); ?>");
			});
		});
	</script>