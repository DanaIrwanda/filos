<?php
defined('BASEPATH') or exit('No direct script access allowed');
$set = $this->func->globalset("semua");
$nama = (isset($titel)) ? $set->nama . " " . $titel : $set->nama . " " . $set->slogan;
$headerclass = (isset($titel)) ? "header-v4" : "";
$keranjang = (isset($_SESSION["usrid"]) and $_SESSION["usrid"] > 0) ? $this->func->getKeranjang() : 0;
$keyw = $this->db->get("kategori");
$keywords = "";
foreach ($keyw->result() as $key) {
    $keywords .= "," . $key->nama;
}

?>


<!doctype html>
<html lang="en" class="<?= $set->color1 ?>">

<head>
    <title><?= $nama ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover, user-scalable=no">
    <meta name="description" content="Filos ID">
    <link rel="shortcut icon" href="<?= base_url("admin/assets/img/" . $set->favicon) ?>" type="image/x-icon" />    

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="655968634437471">
    <meta property="og:title" content="<?= $nama ?> Premium Quality" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?= site_url('produk/' . $data->url); ?>" />
    <meta property="og:image" content="<?= base_url("admin/uploads/" . $gambar->nama); ?>" />
    <meta property="og:description" content="Aplikasi toko online <?= $nama ?>" />
    <meta property="og:site_name" content="<?= $nama ?> Premium Quality" />

    <!-- Material design icons CSS -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?= base_url("admin/assets/img/" . $set->favicon) ?>" />
    <meta name="google-site-verification" content="G35UyHn6lX6mRzyFws0NJYYxHQp_aejuAFbagRKCL7c" />
    <meta name="description" content="Jual Khimar dan Dress dengan pilihan terlengkap serta menerima reseller" />
    <meta name="yandex-verification" content="alspdfjaltnldfkjalphp;lk" />
    <meta name="author" content="filos.id">

    <!--  Social tags      -->
    <meta name="keywords" content="Aplikasi toko online <?= $nama ?>">
    <meta name="description" content="Aplikasi toko online <?= $nama ?>">
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?= $nama ?> Premium Quality">
    <meta itemprop="description" content="Aplikasi toko online <?= $nama ?>">
    <meta itemprop="image" content="<?= base_url("admin/assets/img/" . $set->favicon) ?>">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@masbil_al">
    <meta name="twitter:title" content="<?= $nama ?> Premium Quality">
    <meta name="twitter:description" content="Aplikasi toko online <?= $nama ?>">
    <meta name="twitter:creator" content="@masbil_al">
    <meta name="twitter:image" content="<?= base_url("admin/assets/img/" . $set->favicon) ?>">

    <link rel="stylesheet" href="<?= base_url('assets/css/material-icons.css') ?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Roboto fonts CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><!-- Swiper CSS -->
    <link href="<?= base_url('assets/css/swiper.min.css') ?>" rel="stylesheet">
    <!-- Chosen multiselect CSS -->
    <link href="<?= base_url('assets/css/chosen.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor/select2/select2.min.css') ?>">

    <!-- Custom styles for this template -->
    <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/fonts/fontawesome.all.min.css') ?>" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="<?= base_url('assets/vendor/jquery/jquery-3.2.1.min.js') ?>"></script>

</head>