<body>
	<?php include 'loader.php'; ?>
	<div class="sidebar">
		<?php include 'menutukunen.php'; ?>
	</div>
	<div class="wrapper">
		<?php include 'menuheadtukunen.php'; ?>

		<div class="container">
			<form action="<?= site_url("shop?cari=" . $cari) ?>" method="get" class="search-header">
				<div class="input-group mb-3">
					<input class="form-control" type="text" value="<?= $cari ?>" name="cari" placeholder="Cari Produk">
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="submit"><i class="material-icons md-18">search</i></button>
					</div>
				</div>
			</form>
			<div class="full swiper-container swiper-banner">
				<div class="swiper-wrapper">
					<?php
					$this->db->where("tgl<=", date("Y-m-d H:i:s"));
					$this->db->where("tgl_selesai>=", date("Y-m-d H:i:s"));
					$this->db->where("jenis", 1);
					$this->db->where("status", 1);
					$this->db->order_by("id", "DESC");
					$sld = $this->db->get("promo");
					if ($sld->num_rows() > 0) {
						foreach ($sld->result() as $s) {
					?>
							<div class="swiper-slide">
								<a href="<?= $s->link ?>">
									<div class="swiper-zoom-container"><img src="<?= base_url('admin/promo/' . $s->gambar) ?>" alt="promo"></div>
								</a>
							</div>

						<?php
						}
					} else {
						?>

					<?php
					}
					?>
				</div>
				<!-- Add Pagination -->
				<div class="swiper-pagination"></div>
				<!-- Add Arrows -->
				<div class="d-none d-sm-block swiper-button-next"></div>
				<div class="d-none d-sm-block swiper-button-prev"></div>

			</div>
			<h6 class="subtitle">Kategori</h6>
			<div class="row">
				<!-- Swiper -->
				<div class="swiper-container small-slide">
					<div class="swiper-wrapper">
						<?php
						$this->db->where("parent", 0);
						$db = $this->db->get("kategori");
						foreach ($db->result() as $r) {
						?>
							<div class="swiper-slide">
								<div class="card shadow-sm border-0">
									<div class="card-body">
										<figure class="category-image">
											<a href="<?= site_url("kategori/" . $r->url) ?>">
												<img src="<?= base_url("admin/kategori/" . $r->icon) ?>" alt="Kategori" class="small-slide-right"></a>
										</figure>
											<a href="<?= site_url("kategori/" . $r->url) ?>" class="text-dark mb-1 mt-2 h6 tulisan-card-kategori"><?= $r->nama ?> </a>
									</div>
								</div>
							</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
			<h6 class="subtitle">Ready <a href="<?= base_url('shop') ?>" class="float-right small">Lihat Semua</a></h6>
			<div class="row">

				<?php
				$this->db->where("preorder !=", 1);
				$this->db->where("status", 1);
				$this->db->limit(18);
				$this->db->order_by("id DESC,tglupdate DESC");
				$db = $this->db->get("produk");
				$totalproduk = 0;
				foreach ($db->result() as $r) {
					$level = isset($_SESSION["lvl"]) ? $_SESSION["lvl"] : 0;
					if ($level == 5) {
						$result = $r->hargadistri;
					} elseif ($level == 4) {
						$result = $r->hargaagensp;
					} elseif ($level == 3) {
						$result = $r->hargaagen;
					} elseif ($level == 2) {
						$result = $r->hargareseller;
					} else {
						$result = $r->harga;
					}
					$ulasan = $this->func->getReviewProduk($r->id);
					$this->db->where("idproduk", $r->id);
					$dbv = $this->db->get("produkvariasi");
					$totalstok = ($dbv->num_rows() > 0) ? 0 : $r->stok;
					$hargs = 0;
					$harga = array();
					foreach ($dbv->result() as $rv) {
						$totalstok += $rv->stok;
						if ($level == 5) {
							$harga[] = $rv->hargadistri;
						} elseif ($level == 4) {
							$harga[] = $rv->hargaagensp;
						} elseif ($level == 3) {
							$harga[] = $rv->hargaagen;
						} elseif ($level == 2) {
							$harga[] = $rv->hargareseller;
						} else {
							$harga[] = $rv->harga;
						}
						$hargs += $rv->harga;
					}


				?>

				<?php 	if ($totalproduk < 20) :  ?>

				<?php $totalproduk += 1 ?>
				
						<div class="col-6 col-md-4 col-lg-3 col-xl-2">
							<div class="card shadow-sm border-0 mb-4">
								<div class="card-body">
									<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" class="add_wishlist btn btn-sm btn-link p-0"><i class="material-icons md-18">favorite_outline</i></button>-->

									<span class="badge badge-warning pl-1 pr-1"><?= $ulasan['nilai'] ?></span>
									
									<i class="stars" data-rating="<?= $ulasan['nilai'] ?>" data-num-stars="5"></i>

									<figure class="product-image">
										<a href="<?php echo site_url('produk/' . $r->url); ?>">
										<img src="<?= $this->func->getFoto($r->id, "utama") ?>" alt="<?= $r->nama ?>" class=""></a>
									</figure>
									
									<a href="<?php echo site_url('produk/' . $r->url); ?>" class="text-dark mb-1 mt-2 h6 tulisan-card"><?= $r->nama ?></a>

									<div class="oldPrice">	
										<?php echo "Rp. " . $this->func->formUang($r->hargaagen); ?>
									</div>	

									<h5 class="text-success font-weight-normal mb-0">
										<div class="newPrice">
											<?php
											if ($hargs > 0) {
												echo "Rp. " . $this->func->formUang(min($harga));
											} else {
												echo "Rp. " . $this->func->formUang($result);
											}
											?>
										</div>
									</h5>
									
										<?php if ($hargs > 0): ?>
										
										<div class="product_marks" style="background: red;">
										<?php 
											$amount 	= $this->func->formUang($r->hargaagen) - $this->func->formUang($r->harga);
											$discount 	= $amount/$this->func->formUang($r->hargaagen)*100;

											echo "<b style='color:white;'>" . " " . intval($discount). "%" . "</b>";       
										?>
									</div>

										
									<?php else: ?>
									
									<div class="product_marks" style="background: blue;">
											<?php echo "<b style='color:white;'>" . "New" . "</b>"; ?>
										</div>

									<?php endif ?>
									<!--<button class="btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>
                                        -->
									<p class="text-secondary small text-mute mb-0"><?= $ulasan['ulasan'] ?> Ulasan</p>

									<?php if ($totalstok > 0) : ?>
										<span class="badge badge-info pl-1 pr-1">Tersedia</span>
									<?php else : ?>
										<span class="badge badge-dark pl-1 pr-1">Habis</span>
									<?php endif ?>

									<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" data-ket="" class="add_cart btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>-->
								</div>
							</div>
						</div>

				<?php endif ?>

				<?php 	}

				if ($totalproduk == 0) {
					echo "<div class='col-12 text-center m-tb-40'><button class='btn btn-lg btn-default shadow btn-rounded'>Produk Kosong</button></div>";
				}
				?>



			</div>
		</div>
		<?php
		$this->db->where("jenis", 2);
		$this->db->where("status", 1);
		$sld = $this->db->get("promo");
		if ($sld->num_rows() > 0) { ?>
			<div class="container-fluid bg-warning text-white my-3">

				<div class="container">
					<div class="row">

						<div class="col-md-2">
							<h1 class="text-uppercase mb-3">PROMO</h1>
							<p>Jangan lewatkan kesempatan ini</p>
						</div>
						<div class="col-md-10 mt-2 mb-2">
							<div class="row">
								<div class="swiper-container swiper-banner">
									<div class="swiper-wrapper">
										<?php
										$this->db->where("tgl<=", date("Y-m-d H:i:s"));
										$this->db->where("tgl_selesai>=", date("Y-m-d H:i:s"));
										$this->db->where("jenis", 2);
										$this->db->where("status", 1);
										$this->db->order_by("tgl_selesai", "DESC");
										$sld = $this->db->get("promo");
										if ($sld->num_rows() > 0) {
											foreach ($sld->result() as $s) {
										?>
												<div class="swiper-slide">
													<a href="<?= $s->link ?>">
														<div class="swiper-zoom-container"><img src="<?= base_url('admin/promo/' . $s->gambar) ?>" alt="promo"></div>
													</a>
												</div>
											<?php
											}
										} else {
											?>

										<?php
										}
										?>
									</div>

									<!-- Add Pagination -->
									<div class="swiper-pagination"></div>
									<!-- Add Arrows -->
									<div class="d-none d-sm-block swiper-button-next"></div>
									<div class="d-none d-sm-block swiper-button-prev"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } else { ?>
		<?php } ?>
		<div class="container">
			<h6 class="subtitle">Preorder <a href="<?= base_url('shop/preorder') ?>" class="float-right small">Lihat Semua</a></h6>
			<div class="row">

				<?php
				$this->db->where("preorder =", 1);
				$this->db->where("status =", 1);
				$this->db->limit(6);
				$this->db->order_by("id DESC,tglupdate DESC");
				$db = $this->db->get("produk");
				$totalproduk = 0;
				foreach ($db->result() as $r) {
					$level = isset($_SESSION["lvl"]) ? $_SESSION["lvl"] : 0;
					if ($level == 5) {
						$result = $r->hargadistri;
					} elseif ($level == 4) {
						$result = $r->hargaagensp;
					} elseif ($level == 3) {
						$result = $r->hargaagen;
					} elseif ($level == 2) {
						$result = $r->hargareseller;
					} else {
						$result = $r->harga;
					}
					$ulasan = $this->func->getReviewProduk($r->id);
					$this->db->where("idproduk", $r->id);
					$dbv = $this->db->get("produkvariasi");
					$totalstok = ($dbv->num_rows() > 0) ? 0 : $r->stok;
					$hargs = 0;
					$harga = array();
					foreach ($dbv->result() as $rv) {
						$totalstok += $rv->stok;
						if ($level == 5) {
							$harga[] = $rv->hargadistri;
						} elseif ($level == 4) {
							$harga[] = $rv->hargaagensp;
						} elseif ($level == 3) {
							$harga[] = $rv->hargaagen;
						} elseif ($level == 2) {
							$harga[] = $rv->hargareseller;
						} else {
							$harga[] = $rv->harga;
						}
						$hargs += $rv->harga;
					}

					if ($totalstok > 0 and $totalproduk < 12) {
						$totalproduk += 1;
				?>

						<div class="col-6 col-md-4 col-lg-3 col-xl-2">
							<div class="card shadow-sm border-0 mb-4">
								<div class="card-body">
									<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" class="add_wishlist btn btn-sm btn-link p-0"><i class="material-icons md-18">favorite_outline</i></button>-->

									<span class="badge badge-warning pl-1 pr-1"><?= $ulasan['nilai'] ?></span>
									<i class="stars" data-rating="<?= $ulasan['nilai'] ?>" data-num-stars="5"></i>

									<figure class="product-image"><img src="<?= $this->func->getFoto($r->id, "utama") ?>" alt="<?= $r->nama ?>" class=""></figure>
									<a href="<?php echo site_url('produk/' . $r->url); ?>" class="text-dark mb-1 mt-2 h6 tulisan-card"><?= $r->nama ?></a>
									<h5 class="text-success font-weight-normal mb-0">

										<!-- old price -->
										<div class="oldPrice">	
											<?php echo "Rp. " . $this->func->formUang($r->hargaagen); ?>
										</div>	

										<div class="newPrice">
											<?php
											if ($hargs > 0) {
												echo "Rp. " . $this->func->formUang(min($harga));
											}
											?>
										</div>
									</h5>
									
									<?php if ($hargs > 0): ?>
										
										<div class="product_marks" style="background: red;">
										<?php 
											$amount 	= $this->func->formUang($r->hargaagen) - $this->func->formUang($r->harga);
											$discount 	= $amount/$this->func->formUang($r->hargaagen)*100;

											echo "<b style='color:white;'>" . " " . intval($discount). "%" . "</b>";       
										?>
									</div>

										
									<?php else: ?>
									
									<div class="product_marks" style="background: blue;">
											<?php echo "<b style='color:white;'>" . "New" . "</b>"; ?>
										</div>

									<?php endif ?>
								
									<!--<button class="btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>
                                        -->
									<p class="text-secondary small text-mute mb-0"><?= $ulasan['ulasan'] ?> Ulasan</p>

									<p class="badge badge-warning pl-1 pr-1">Preorder</p>

									<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" data-ket="" class="add_cart btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>-->
								</div>
							</div>
						</div>

				<?php
					}
				}

				if ($totalproduk == 0) {
					echo "<div class='col-12 text-center m-tb-40'><button class='btn btn-lg btn-default shadow btn-rounded'>Produk pre Order Kosong</button></div>";
				}
				?>



			</div>
		</div>
		<div class="container">
			<h6 class="subtitle">Posting blog Terbaru <a href="<?= base_url("blog") ?>" class="float-right small">Lihat Semua</a></h6>

			<div class="row">
				<!-- Swiper -->
				<div class="swiper-container news-slide">
					<div class="swiper-wrapper">

						<?php
						$this->db->select("id");
						$dbs = $this->db->get("blog");

						$this->db->limit(12, 0);
						$this->db->order_by("tgl DESC");
						$db = $this->db->get("blog");

						if ($db->num_rows() > 0) {
							foreach ($db->result() as $res) {
						?>


								<div class="swiper-slide">
									<div class="card shadow-sm border-0 bg-dark text-white">
										<figure class="background">
											<img src="<?= base_url("admin/uploads/" . $res->img) ?>" alt="<?= $this->func->potong($res->judul, 40, "...") ?>">
										</figure>
										<div class="card-body">
											<a href="<?= site_url('blog/' . $res->url) ?>" class="btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">arrow_forward</i></a>
											<h5 class="small"><?= $this->func->potong(strip_tags($res->konten), 90, "...") ?></h5>

											<p class="text-mute small">
												<div class="newPrice"><?= $this->func->potong($res->judul, 20, "...") ?></div>

											</p>
										</div>
									</div>
								</div>

						<?php
							}
						} else {
							echo "<div class='col-12 text-center m-tb-40'><button class='btn btn-lg btn-default shadow btn-rounded'>belum ada posting blog</button></div>";
						}
						?>



					</div>
					<!-- Add Pagination -->
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="d-none d-sm-block swiper-button-next"></div>
					<div class="d-none d-sm-block swiper-button-prev"></div>

				</div>
			</div>

		</div>


		<?php include 'footer_alamat.php'; ?>

	</div>
	<?php $notif_booster = $this->func->getSetting("notif_booster");
	if ($notif_booster == 1) { ?>
		<div id="toaster" class="toaster row col-md-4" style="display:none;">
			<div class="col-3 img p-lr-6"><img style="width: 50%;" id="toast-foto" src="<?= base_url("admin/uploads/520200116140232.jpg") ?>" /></div>
			<div class="col-9 p-lr-6">
				<b id="toast-user">USER</b> telah membeli<br />
				<b id="toast-produk">Nama Produknya</b>
			</div>
		</div>
	<?php } ?>

	<?php include 'footermobiletukunen.php'; ?>

	<!-- notification -->
	<!-- <div class="notification bg-white shadow border-primary">
		<div class="row">
			<div class="col-auto align-self-center pr-0">
				<i class="material-icons text-primary md-36">fullscreen</i>
			</div>
			<div class="col">
				<h6>TUKUNEN TOKO ONLINE</h6>
				<p class="mb-0 text-secondary">Toko tuKUNEN ini hanya contoh, minat hub WA: 081225230447</p>
			</div>
			<div class="col-auto align-self-center pl-0">
				<button class="btn btn-link closenotification"><i class="material-icons text-secondary text-mute md-18 ">close</i></button>
			</div>
		</div>
	</div> -->
	<!-- notification ends -->


	<div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content ">
				<div class="modal-header theme-header border-0">
					<h6 class="">Setting Warna</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body pt-0">
					<div class="text-center theme-color">
						<button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
						<button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
						<button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
						<button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
						<button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
						<button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-12 text-center w-100">
						<div class="row justify-content-center">
							<div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
							<div class="col-auto text-center align-self-center px-0">
								<div class="custom-control custom-switch float-right">
									<input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
									<label class="custom-control-label" for="theme-dark"></label>
								</div>
							</div>
							<div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- color chooser menu ends -->
	<!-- jquery, popper and bootstrap js -->
	<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- cookie js -->
	<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

	<!-- swiper js -->
	<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

	<!-- template custom js -->
	<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/js/main.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>


	<script>
		const years = new Date().getFullYear();
		$("#footer-cr-years").text(years);
		$(window).on('load', function() {
			/* swiper slider carousel */
			var swiper = new Swiper('.small-slide', {
				slidesPerView: 'auto',
				spaceBetween: 0,
			});

			var swiper = new Swiper('.news-slide', {
				slidesPerView: 5,
				spaceBetween: 0,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},

				breakpoints: {
					1024: {
						slidesPerView: 4,
						spaceBetween: 0,
					},
					768: {
						slidesPerView: 3,
						spaceBetween: 0,
					},
					640: {
						slidesPerView: 2,
						spaceBetween: 0,
					},
					320: {
						slidesPerView: 2,
						spaceBetween: 0,
					}
				}
			});
			var swiper = new Swiper('.swiper-banner', {
				slidesPerView: 3,
				spaceBetween: 10,
				freeMode: true,
				loop: true,
				autoplay: {
					delay: 2500,
					disableOnInteraction: false,
				},
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					1024: {
						slidesPerView: 3,
						spaceBetween: 10,
						freeMode: true,
						loop: true,
					},
					768: {
						slidesPerView: 1,
						spaceBetween: 0,
						freeMode: true,
						loop: true,
					},
					640: {
						slidesPerView: 1,
						spaceBetween: 0,
						freeMode: true,
						loop: true,
					},
					320: {
						slidesPerView: 1,
						spaceBetween: 0,
						loop: true,
					}
				}
			});

			/* notification view and hide */
			setTimeout(function() {
				$('.notification').addClass('active');
				setTimeout(function() {
					$('.notification').removeClass('active');
				}, 35000);
			}, 500);
			$('.closenotification').on('click', function() {
				$(this).closest('.notification').removeClass('active')
			});
		});
		$.fn.stars = function() {
			return $(this).each(function() {

				var rating = $(this).data("rating");

				var numStars = $(this).data("numStars");

				var fullStar = new Array(Math.floor(rating + 1)).join('<i class="material-icons text-warning md-18 vm">star</i>');

				var halfStar = ((rating % 1) !== 0) ? '<i class="material-icons text-warning md-18 vm">star_half</i>' : '';

				var noStar = new Array(Math.floor(numStars + 1 - rating)).join('<i class="material-icons text-secondary md-18 vm">star_border</i>');

				$(this).html(fullStar + halfStar + noStar);

			});
		}

		$('.stars').stars();
	</script><!-- color chooser menu start -->
	<script type="text/javascript">
		$(function() {
			setTimeout(() => {
				toaster();
			}, 3000);
		});

		<?php if ($notif_booster == 1) { ?>

			function toaster() {
				$.post("<?= site_url("assync/booster") ?>", {
					"id": 0
				}, function(msg) {
					var data = eval("(" + msg + ")");
					if (data.success == true) {
						$("#toast-foto").attr("src", data.foto);
						$("#toast-user").html(data.user);
						$("#toast-produk").html(data.produk);

						$("#toaster").show("slow");
						setTimeout(() => {
							$("#toaster").hide("slow");
							setTimeout(() => {
								toaster();
							}, 3000);
						}, 5000);
					} else {
						setTimeout(() => {
							toaster();
						}, 5000);
					}
				});
			}
		<?php } ?>
	</script>