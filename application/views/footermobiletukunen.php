<div class="footer d-block d-sm-none">
    <div class="no-gutters">
        <div class="col-auto mx-auto">
            <div class="row no-gutters justify-content-center">
                <div class="col-auto">
                    <a href="<?= base_url() ?>" class="btn btn-link-default ">
                        <i class="material-icons">home</i>
                    </a>
                </div>
                <div class="col-auto">
                    <p data-toggle="modal" data-target="#modalMoreCategory" class="btn btn-link-default">
                        <i class="material-icons">view_module</i>
                    </p>
                </div>
                <div class="col-auto">

                    <a href="<?= site_url('home/keranjang') ?>" class="btn btn-default shadow centerbutton">
                        <i class="material-icons">shopping_cart</i>
                        <span class="cart_counter"><?= $this->func->getKeranjang() ?></span>
                    </a>
                </div>
                <div class="col-auto">
                    <a href="<?= base_url('shop') ?>" class="btn btn-link-default">
                        <i class="material-icons">local_mall</i>
                    </a>
                </div>
                <div class="col-auto">
                    <a href="<?= site_url('manage') ?>" class="btn btn-link-default active">
                        <i class="material-icons">account_circle</i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalMoreCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalCenterTitle">Semua Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="main-category">
                    <?php
                    $this->db->where("parent", 0);
                    $db = $this->db->get("kategori");
                    foreach ($db->result() as $r) {
                    ?>

                        <a href="<?= site_url("kategori/" . $r->url) ?>">
                            <div class="item">
                                <img src="<?= base_url("admin/kategori/" . $r->icon) ?>" alt="<?= $r->nama ?>">
                                <p><?= $r->nama ?></p>
                            </div>
                        </a>

                    <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cari Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= site_url("shop?cari=" . $cari) ?>" method="get" class="search-header">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" value="<?= $cari ?>" name="cari" placeholder="Cari Produk">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit"><i class="material-icons md-18">search</i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header theme-header border-0">
                <h6 class="">Setting Warna</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="text-center theme-color">
                    <button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
                    <button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
                    <button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
                    <button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
                    <button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
                    <button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-12 text-center w-100">
                    <div class="row justify-content-center">
                        <div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
                        <div class="col-auto text-center align-self-center px-0">
                            <div class="custom-control custom-switch float-right">
                                <input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
                                <label class="custom-control-label" for="theme-dark"></label>
                            </div>
                        </div>
                        <div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- color chooser menu ends -->