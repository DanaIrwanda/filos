<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--<form id="form">
				<ul class="sign-left">
					<li>
						<p id="error" style="color:#cc0000;display:none;"><small>alamat email tidak ditemukan, mohon cek kembali.</small></p>
						<p id="sukses" style="color:#00cc00;display:none;"><small>berhasil menyetel ulang password, silahkan cek email anda untuk langkah selanjutnya.</small></p>
					</li>
					<li>
						<div class="inner-addon left-addon">
							<span class="fa fa-envelope"></span>
							<input type="text" name="email" class="form border border-grey" placeholder="Alamat Email" required />
						</div>
					</li>
					<li>
						<button type="submit" class="btn btn-green" id="submit">Setel ulang password</button>
					</li>
				</ul>
			</form>-->
<div class="row no-gutters vh-100 proh bg-template">
	<div class="col align-self-center px-3 text-center">
		<form id="form" class="form-signin shadow">
			Masukkan alamat email Anda untuk mengatur ulang password.
			<p id="error" style="color:#cc0000;display:none;"><small>alamat email tidak ditemukan, mohon cek kembali.</small></p>
			<p id="sukses" style="color:#00cc00;display:none;"><small>berhasil menyetel ulang password, silahkan cek email anda untuk langkah selanjutnya.</small></p>
			<div class="form-group float-label">
				<input class="form-control" type="email" name="email">
				<label for="email" class="form-control-label">Alamat Email</label>
			</div>
			<div id="prosesmail" style="display:none;">
				<h5 class="cl1"><i class="fa fa-circle-o-notch fa-spin"></i> Memproses...</h5>
			</div>
			<button id="submitmail" type="submit" class="btn btn-sm btn-default btn-rounded shadow">
				reset password
			</button>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$("#form").on("submit", function(e) {
			e.preventDefault();

			$(".form").prop("readonly", true);
			$("#submitmail").hide();
			$("#prosesmail").show();
			$.post("<?php echo site_url("home/signin/pwreset"); ?>", $(this).serialize(), function(msg) {
				var data = eval('(' + msg + ')');
				$("#submitmail").show();
				$("#prosesmail").hide();
				if (data.success == true) {
					$(".form").val("");
					$(".form").prop("readonly", false);
					swal("Berhasil!", "Berhasil mengirimkan password baru, silahkan cek inbox email Anda.", "success").then((value) => {
						window.location.href = "<?php echo site_url("home/signin"); ?>";
					});
				} else {
					swal("Gagal!", "Alamat email tidak ditemukan, pastikan alamat email sudah benar.", "error");
				}
			});
		});
	});
</script>