<?php $set = $this->func->getSetting("semua"); ?>
<!-- breadcrumb -->
<div class="sidebar">
	<?php include 'menutukunen.php'; ?>
</div>
<div class="wrapper">
	<?php include 'menuheadtukunen.php'; ?>
	<div class="container">

		<form class="p-t-0 p-b-85">
			<div class="card">
				<div class="mt-3 text-center">
					<h4>Order ID <?php echo $data->trxid; ?></h4>
					<hr>
				</div>

				<div class="col-12">
					<div class="row">
						<div class="col-10 mobilefix">
							<h4 class="mtext-105">Terima Kasih <?php echo $this->func->getProfil($data->usrid, "nama", "usrid"); ?></h4>
						</div>
					</div>
				</div>
				<div class="col-12">
					<h4 class="mtext-109 cl2 p-b-20">
						Top Up Saldo
					</h4>

					<?php
					if ($data->total > 0) {
						$bayartotal = $data->total;
					?>
						<div class="col-md-12">
							<!--<h5 class="text-black">Metode Pembayaran: <span class="cl1" style="font-size: 16px;">Virtual Account, E-Wallet, Mini Market, Dll</span> </h5>-->
							<h5 class="text-black">Mohon lakukan pembayaran sejumlah <span style="color: #c0392b; font-size: 20px;"><b>Rp <?php echo $this->func->formUang($bayartotal); ?></b></span></h5>
						</div>
						<?php if ($set->payment_ipaymu != 1) { ?>
							<div class="row">
								<div class="col-md-12">
									<h5 class="text-black">Silahkan transfer pembayaran ke rekening berikut:</h5>
								</div>
								<div class="col-md-12">
									<p></p>
									<?php
									foreach ($bank->result() as $bn) {
										echo '
														<h5 class="cl2 m-t-10 m-b-10 p-t-10 p-l-10 p-b-10" style="border-left: 8px solid #C0A230;padding-left: 10px; background: #f1ecdf;">
															<b class="text-danger">Bank ' . $bn->nama . ': </b><b class="text-success">' . $bn->norek . '</b><br/>
															<span style="font-size: 90%">a/n ' . $bn->atasnama . '<br/>
															KCP ' . $bn->kcp . '</span>
														</h5>
													';
									}
									?>
									<p class="mb-5 mt-20">
										<b>PENTING: </b>
									</p>
									<ul style="margin-left: 15px;">
										<li style="list-style-type: disc;">Mohon lakukan pembayaran dalam <b>1x24 jam</b></li>
										<li style="list-style-type: disc;">Sistem akan otomatis mendeteksi apabila pembayaran sudah masuk</li>
										<li style="list-style-type: disc;">Apabila sudah transfer dan status pembayaran belum berubah, mohon konfirmasi pembayaran manual di bawah</li>
										<li style="list-style-type: disc;">Pesanan akan dibatalkan secara otomatis jika Anda tidak melakukan pembayaran.</li>
									</ul>
								</div>
							</div>
						<?php } ?>
						<hr class="m-t-30" />
						<div class="mb-3 text-center">
							<?php if ($set->payment_ipaymu == 1) { ?>
								<a href="<?php echo site_url("assync/topupipaymu/" . $data->id); ?>" class="btn btn-success btn-rounded shadow btn-md mb-2 bayarotomatis"><i class="fa fa-chevron-circle-right"></i> &nbsp;<b>BAYAR SEKARANG</b></a>
							<?php } ?>
							<a href="<?php echo site_url("manage"); ?>" class="btn btn-danger btn-rounded shadow btn-md text-center mb-2 bayarotomatis"><i class="fa fa-times"></i> &nbsp;<b>BAYAR NANTI SAJA</b></a>
						</div>
					<?php
					} else {
					?>
						<div class="p-b-13">
							<div class="row p-t-20">
								<div class="col-md-12">
									<h5 class="text-black">Metode Pembayaran: <span class="cl1" style="font-size: 16px;">Saldo <?= $this->func->getSetting("nama") ?></span> </h5>
								</div>
							</div>
							<div class="row p-t-5">
								<div class="col-md-12">
									<p>Terima kasih, saldo <b class='cl1'><?= $this->func->getSetting("nama") ?></b> sudah terpotong sebesar
										<span style="color: #c0392b; font-size: 20px;"><b>Rp <?php echo $this->func->formUang($data->saldo); ?></b></span>
										untuk pembayaran pesanan Anda.<br />
										<!--Kami sudah menginformasikan kepada merchant untuk memproses pesanan Anda.-->
									</p>
								</div>
							</div>
						</div>
						<hr class="m-t-30" />
						<a href="<?php echo site_url("manage/pesanan"); ?>" class="cl1 text-center w-full dis-block"><b>STATUS PESANAN</b> <i class="fa fa-chevron-circle-right"></i></a>
					<?php } ?>

				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
	$(".js-select2").each(function() {
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});
		/* chosen select*/
	});
	$(".chosen").chosen();
</script>


<script type="text/javascript">
	function bayarManual() {
		$(".metode-item").removeClass("active");
		$(".metode-item.manual").addClass("active");
		$(".bayarmanual").show();
		$(".bayarotomatis").hide();
	}

	function bayarOtomatis() {
		$(".metode-item").removeClass("active");
		$(".metode-item.otomatis").addClass("active");
		$(".bayarmanual").hide();
		$(".bayarotomatis").show();
	}
</script>