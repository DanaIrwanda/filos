<div class="sidebar">
    <?php include 'menutukunen.php'; ?>
</div>
<div class="wrapper">
    <?php include 'menuheadtukunen.php'; ?>
    <div class="container">
        <h4 class="mtext-105 cl2 m-b-20">Terima Kasih</h4>
        <div class="notif-ipaymu">
            Pembayaran akan otomatis terverifikasi oleh sistem kami maksimal 10 menit setelah Anda melakukan transfer.<br />
            <div class="m-t-20 text-center">
                <a class="btn btn-success btn-rounded shadow btn-md mt-5" href="<?= site_url("manage/pesanan") ?>">PESANANKU &nbsp;<i class="fa fa-chevron-right"></i> </a>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
    $(".js-select2").each(function() {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
        /* chosen select*/
    });
    $(".chosen").chosen();
</script>