<div class="header">
    <div class="row no-gutters">
        <div class="col-auto">
            <button class="btn  btn-link text-dark menu-btn"><i class="material-icons">sort</i></span></button>
        </div>

        <?php if ($this->func->cekLogin() == true) { ?>

            <div class="col text-center"><img src="<?= base_url("admin/assets/img/" . $set->logo) ?>" alt="" class="header-logo"></div>
            <div class="col-auto">
                <p data-toggle="modal" data-target="#modalSearch" class="btn btn-link text-dark"><i class="material-icons">search</i></p>
            </div>
            <div class="col-auto d-none d-sm-block">
                <a href="javascript:void(0)" onclick="$('#modalpilihpesan').modal()" class="btn btn-link text-dark"><i class="fa fa-comments"></i></a>
                <?php if ($notif > 0) { ?><p class="cart_counter_top"><?= $notif ?></p><?php } ?>
            </div>

            <div class="col-auto d-none d-sm-block">
                <a href="<?= base_url() ?>" class="btn  btn-link text-dark"><i class="material-icons">home</i></a>
            </div>
            <div class="col-auto d-none d-sm-block">
                <p data-toggle="modal" data-target="#modalMoreCategory" class="btn btn-link text-dark"><i class="material-icons">view_module</i></p>
            </div>
            <div class="col-auto d-none d-sm-block">
                <a href="<?= base_url('shop') ?>" class="btn btn-link text-dark"><i class="material-icons">local_mall</i></a>
            </div>

            <div class="col-auto d-none d-sm-block">
                <a href="<?= site_url('home/keranjang') ?>" class="btn  btn-link text-dark"><i class="material-icons">shopping_cart</i>
                    <p class="cart_counter_top"><?= $this->func->getKeranjang() ?></p>
                </a>
            </div>
            <div class="col-auto d-none d-sm-block">
                <a href="<?= site_url('manage') ?>" class="btn  btn-link text-dark"><i class="material-icons">account_circle</i></a>
            </div>
        <?php } else { ?>
            <div class="col text-center"><img src="<?= base_url("admin/assets/img/" . $set->logo) ?>" alt="" class="header-logo"></div>
            <div class="col-auto">
                <p data-toggle="modal" data-target="#modalSearch" class="btn btn-link text-dark"><i class="material-icons">search</i></p>
            </div>
            <div class="col-auto d-none d-sm-block">
                <a href="javascript:void(0)" onclick="$('#modalpilihpesan').modal()" class="btn btn-link text-dark"><i class="fa fa-comments"></i></a>
                <?php if ($notif > 0) { ?><p class="cart_counter_top"><?= $notif ?></p><?php } ?>
            </div>

            <div class="col-auto d-none d-sm-block">
                <a href="<?= base_url() ?>" class="btn  btn-link text-dark"><i class="material-icons">home</i></a>
            </div>
            <div class="col-auto d-none d-sm-block">
                <p data-toggle="modal" data-target="#modalMoreCategory" class="btn btn-link text-dark"><i class="material-icons">view_module</i></p>
            </div>
            <div class="col-auto d-none d-sm-block">
                <a href="<?= base_url('shop') ?>" class="btn btn-link text-dark"><i class="material-icons">local_mall</i></a>
            </div>

            <div class="col-auto d-none d-sm-block">
                <a href="<?= site_url('home/keranjang') ?>" class="btn  btn-link text-dark"><i class="material-icons">shopping_cart</i>
                    <p class="cart_counter_top"><?= $this->func->getKeranjang() ?></p>
                </a>
            </div>
            <div class="col-auto d-none d-sm-block">
                <a href="<?= site_url('manage') ?>" class="btn  btn-link text-dark"><i class="material-icons">login</i></a>
            </div>

        <?php } ?>

    </div>
</div>