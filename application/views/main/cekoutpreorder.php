<div class="sidebar">
	<?php include 'menutukunen.php'; ?>
</div>
<div class="wrapper">
	<?php include 'menuheadtukunen.php'; ?>

	<div class="container">
		<div class="card">
			<div class="mt-3 text-center">
				<h4>Order ID <?php echo $data->invoice; ?></h4>
				<hr>
			</div>
			<div class="col-12">
				<h4 class="mtext-105">Terima Kasih <?php echo $this->func->getProfil($_SESSION["usrid"], "nama", "usrid"); ?></h4>
			</div>
			<div class="col-12">
				<h4 class="mtext-109 cl2 p-b-20">
					Pembayaran
				</h4>

				<?php
				if ($data->transfer > 0) {
					$tombol = "KONFIRMASI PEMBAYARAN";
				?>
					<div class="p-b-13">
						<div class="row p-t-20">
							<div class="col-md-12">
								<h5 class="text-black">Metode Pembayaran: <span class="color1" style="font-size: 16px;">Bank Transfer</span> </h5>
							</div>
						</div>
						<div class="row p-t-5">
							<div class="col-md-12">
								<p>Mohon lakukan pembayaran DP sejumlah <span style="color: #c0392b; font-size: 20px;"><b>Rp <?php echo $this->func->formUang($data->transfer + $data->kodebayar); ?></b></span>
									ke rekening di bawah ini: </p>
								<?php
								foreach ($bank->result() as $bn) {
									echo '
														<h5 class="cl2 m-t-10 m-b-20 p-tb-20 p-lr-14" style="border-left: 8px solid #C0A230;padding-left: 10px; background: #f1ecdf;">
															<b class="text-danger">Bank ' . $bn->nama . ': </b><b class="text-success">' . $bn->norek . '</b><br/>
															<span style="font-size: 90%">a/n ' . $bn->atasnama . '<br/>
															KCP ' . $bn->kcp . '</span>
														</h5>
													';
								}
								?>
								<p class="m-b-5 m-t-20">
									<b>PENTING: </b>
								</p>
								<ul style="margin-left: 15px;">
									<li style="list-style-type: disc;">Mohon lakukan pembayaran dalam <b>1x24 jam</b></li>
									<li style="list-style-type: disc;">Sistem akan otomatis mendeteksi apabila pembayaran sudah masuk</li>
									<li style="list-style-type: disc;">Apabila sudah transfer dan status pembayaran belum berubah, mohon konfirmasi pembayaran manual di bawah</li>
									<li style="list-style-type: disc;">Pesanan akan dibatalkan secara otomatis jika Anda tidak melakukan pembayaran.</li>
								</ul>

							</div>
						</div>
					</div>
				<?php
				} else {
					$tombol = "STATUS PESANAN";
				?>
					<div class="p-b-13">
						<div class="row p-t-20">
							<div class="col-md-12">
								<h5 class="text-black">Metode Pembayaran: <span class="cl1" style="font-size: 16px;">Saldo <?= $this->func->getSetting("nama") ?></span> </h5>
							</div>
						</div>
						<div class="row p-t-5">
							<div class="col-md-12">
								<p>Terima kasih, saldo <b class='cl1'> <?= $this->func->getSetting("nama") ?></b> sudah terpotong sebesar
									<span style="color: #c0392b; font-size: 20px;"><b>Rp <?php echo $this->func->formUang($data->saldo); ?></b></span>
									untuk pembayaran pesanan Anda.<br />
									<!--Kami sudah menginformasikan kepada merchant untuk memproses pesanan Anda.-->
								</p>
							</div>
						</div>
					</div>
				<?php } ?>
				<hr class="m-t-30" />
				<div class="text-center mt-2 mb-4">
					<a href="javascript:void(0)" onclick="konfirmasi(<?= $data->id ?>)" class="btn btn-warning btn-rounded shadow btn-md "><b><?php echo $tombol; ?></b> <i class="fa fa-chevron-circle-right"></i></a>
				</div>
			</div>
		</div>

		<div class="card mt-2">
			<div class="col-12">
				<div class="mt-3 text-center">
					<h4> Produk Pesanan </h4>
					<hr>
				</div>

				<?php
				$produk = $this->func->getProduk($data->idproduk, "semua");
				$variasee = $this->func->getVariasi($data->variasi, "semua");
				$variasi = ($data->variasi != 0) ? $this->func->getWarna($variasee->warna, "nama") . " size " . $this->func->getSize($variasee->size, "nama") : "";
				$variasi = ($data->variasi != 0) ? "<br/><small class='text-warning'>variasi: " . $variasi . "</small>" : "";
				?>
				<ul class="list-items mb-1">
					<li>
						<div class="col-12">
							<div class="row">
								<div class="card shadow-sm border-0">
									<div class="card-body">
										<figure class="product-image"><img src="<?php echo $this->func->getFoto($data->idproduk, "utama"); ?>" alt="produk" class=""></figure>
									</div>
								</div>
								<div class="col">
									<p class="text-dark mb-1 h6 d-block"><?php if ($produk != null) {
																				echo $produk->nama;
																			} else {
																				echo "Produk telah dihapus";
																			} ?></p>
									<?= $variasi ?>
									</p>
									<h5 class="text-success font-weight-normal mb-0">Rp <?php echo $this->func->formUang($data->harga); ?> <span style="font-size:11px">x<?php echo $data->jumlah; ?></span></h5>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
	$(".js-select2").each(function() {
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});
		/* chosen select*/
	});
	$(".chosen").chosen();
</script>


<script type="text/javascript">
	function konfirmasi(bayar) {
		$('.js-modal2').modal('show');
		$("#bayar").val(bayar);
	}
</script>
<!-- Modal1 -->
<div class="modal fade wrap-modal2 js-modal2">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content text-center">
			<div class="modal-header">
				<h4 class="modal-title"><b>Konfirmasi</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<p>Upload Bukti Transfer <span style="font-size: 15px">(.jpg, .png, .pdf)</span></p>
			<form id="upload" method="POST" enctype="multipart/form-data" action="<?php echo site_url("manage/konfirmasipreorder"); ?>">
				<input name="idbayar" type="hidden" id="bayar" value="0" />
				<div class="col-md-12">
					<div class="form-control">
						<div class="upload-options">
							<label>
								<input type="file" name="bukti" class="w-full pointer image-upload bor8 p-t-15 p-b-15 p-l-25 p-r-30 p-lr-0-lg" accept="image/*" />
							</label>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-default btn-rounded mt-3 mb-3">
					Upload
				</button>
			</form>
		</div>
	</div>