<?php include 'loader.php'; ?>
<div class="sidebar">
	<?php include 'menutukunen.php'; ?>
</div>
<div class="wrapper">
	<?php include 'menuheadtukunen.php'; ?>
	<div class="container">
		<div class="card text-center mt-3 mb-3">
			<h2 class="mt-2"><b>BLOG TERBARU</b></h2>
		</div>
		<div class="row">
			<?php
			$page = (isset($_GET["page"]) and $_GET["page"] != "") ? $_GET["page"] : 1;
			$orderby = (isset($_GET["orderby"]) and $_GET["orderby"] != "") ? $_GET["orderby"] : "id DESC";
			$perpage = 12;

			$dbs = $this->db->get("blog");

			$this->db->limit($perpage, ($page - 1) * $perpage);
			$this->db->order_by($orderby);
			$db = $this->db->get("blog");

			if ($db->num_rows() > 0) {
				foreach ($db->result() as $res) {
			?>

					<div class="col-12 col-md-6">
						<div class="swiper-container news-slide mb-0">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<div class="card shadow-sm border-0 bg-dark text-white">
										<figure class="background">
											<img src="<?= base_url("admin/uploads/" . $res->img) ?>" alt="<?= $this->func->potong($res->judul, 40, "...") ?>">
										</figure>
										<div class="card-body">
											<a href="<?= site_url('blog/' . $res->url) ?>" class="btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">arrow_forward</i></a>
											<h5 class="small"><?= $this->func->potong(strip_tags($res->konten), 90, "...") ?></h5>

											<p class="text-mute small">
												<div class="newPrice"><?= $this->func->potong($res->judul, 20, "...") ?></div>

											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php
				}
			} else {
				echo "
						<div class='text-danger text-center'>
							BELUM ADA POSTINGAN
						</div>
					";
			}
			?>
		</div>
		<!-- Pagination -->
		<div class="pagination justify-content-center">
			<?= $this->func->createPagination($dbs->num_rows(), $page, $perpage) ?>
		</div>
	</div>
</div>

<?php include 'footermobiletukunen.php'; ?>
</div>
<!-- jquery, popper and bootstrap js -->
<!-- jquery, popper and bootstrap js -->
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
<!-- chosen multiselect js -->
<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


<!-- page level script -->
<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

<script type="text/javascript">
	$(".js-select2").each(function() {
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});
		/* chosen select*/
	});
	$(".chosen").chosen();
</script>



<script type="text/javascript">
	function refreshTabel(page) {
		window.location.href = "<?= site_url("blog") ?>?page=" + page;
	}
</script>