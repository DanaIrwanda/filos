<?php
if ($data->num_rows() == 0) {
  redirect("home");
  exit;
}
?>

<body>
  <?php include 'loader.php'; ?>
  <div class="sidebar">
    <?php include 'menutukunen.php'; ?>
  </div>
  <div class="wrapper">
    <?php include 'menuheadtukunen.php'; ?>
    <div class="container">
      <!-- Shoping Cart -->
      <form class="bg0 p-t-75 p-b-85" id="cekout">
        <div class="row">
          <div class="col-md-12">
            <h4 class="mtext-109 cl2 p-b-30">
              Alamat Pengiriman
            </h4>

            <?php
            $this->db->where("usrid", $_SESSION["usrid"]);
            $this->db->order_by("status", "DESC");
            $row = $this->db->get("alamat");
            ?>
            <div class="p-b-13">
              <input type="hidden" id="tujuan" value="" name="tujuan" />
              <div class="m-b-12 alamatform">
                <b>Alamat Pengiriman</b>
              </div>
              <div class="form-group float-label active alamatform">
                <select class="form-control chosen" name="alamat" id="idalamat">
                  <option value="">= Pilih Alamat Tujuan =</option>
                  <?php
                  foreach ($row->result() as $al) {
                    //RAJAONGKIR
                    $kec = $this->func->getKec($al->idkec, "semua");
                    $idkab = $kec->idkab;
                    $keckab = $kec->nama . ", " . $this->func->getKab($idkab, "nama");
                    echo '<option value="' . $al->id . '" data-tujuan="' . $al->idkec . '">' . strtoupper(strtolower($al->judul . ' - ' . $al->nama)) . ' (' . $keckab . ')</option>';
                  }
                  ?>
                  <option value="0">+ Tambah Alamat Baru</option>
                </select>
                <div class="dropDownSelect2"></div>
              </div>
              <div class="mb-3">
                <?php
                foreach ($row->result() as $als) {
                  $kec = $this->func->getKec($al->idkec, "semua");
                  $idkab = $kec->idkab;
                  $kec = $kec->nama;
                  $kab = $this->func->getKab($idkab, "nama");
                  echo "
                     <div class='col-12 px-0'>  
                      <div class='alamat' id='alamat_" . $als->id . "' data-tujuan='" . $al->idkec . "' style='display:none;'>
                        <b>Nama Penerima:</b><br/>" . strtoupper(strtolower($als->nama)) . "<br/>
                        <b>No HP:</b><br/>" . $als->nohp . "<br/>
                        <b>Alamat Lengkap:</b><br/>" . strtoupper(strtolower($als->alamat . "<br/>" . $kec . ", " . $kab)) . "<br/>KODEPOS " . $als->kodepos . "
                      </div>
                      </div>
                    ";
                }
                ?>
              </div>
            </div>

            <div class="mb-3 tambahalamat" style="display:none;">
              <b>Tambah Alamat Pengiriman</b>
            </div>
            <div class="tambahalamat" style="display:none;">
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group float-label">
                    <input class="form-control" type="text" name="judul">
                    <label class="form-control-label" for="judul">Alamat sebagai</label>
                    <small class="form-text text-danger pl-1"></small>
                    <small class="text-muted">Contoh: Rumah, Kantor dsb</small>

                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="form-group float-label">
                    <input class="form-control" type="text" name="nama">
                    <label for="nama" class="form-control-label">Nama Penerima</label>
                    <small class="form-text text-danger pl-1"></small>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group float-label">
                    <input class="form-control" type="text" name="nohp">
                    <label class="form-control-label" for="nohp">No HP penerima</label>
                    <small class="form-text text-danger pl-1"></small>
                    <small class="text-muted">Contoh: 081234567</small>

                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="form-group float-label">
                    <input type="text" name="alamatbaru" class="form-control" required autocomplete="off">
                    <label for="nama_penerima" class="form-control-label">Alamat Lengkap</label>

                    <small class="form-text text-danger pl-1"></small>

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6 mb-1">
                  <select class="js-select2" name="negara" readonly>
                    <option value="ID">Indonesia</option>
                  </select>
                  <div class="dropDownSelect2"></div>
                </div>
                <div class="col-12 col-md-6 mb-1">
                  <select class="form-control js-select2" id="prov">
                    <option value="">Provinsi</option>
                    <?php
                    $prov = $this->db->get("prov");
                    foreach ($prov->result() as $pv) {
                      echo '<option value="' . $pv->id . '">' . $pv->nama . '</option>';
                    }
                    ?>
                  </select>
                  <div class="dropDownSelect2"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6 mb-1">
                  <select class="form-control js-select2" id="kab">
                    <option value="">Kabupaten</option>
                  </select>
                  <div class="dropDownSelect2"></div>
                </div>
                <div class="col-12 col-md-6 mb-1">
                  <select class="form-control js-select2" id="kec" name="idkec">
                    <option value="">Kecamatan</option>
                  </select>
                  <div class="dropDownSelect2"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group float-label">
                    <input type="number" name="kodepos" class="form-control" required autocomplete="off">
                    <label for="kodepos" class="form-control-label">Kode Pos</label>
                    <small class="form-text text-danger pl-1"></small>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <b>Kurir Pengiriman</b>
            </div>
            <div class="row">
              <div class="col-md-4 mb-2">
                <div class="rs1-select2 rs2-select2 bor8 bg0 w-full">
                  <select class="js-select2 kurir" name="kurir">
                    <option value="">Pilih Ekspedisi</option>
                    <?php
                    $kur = $this->db->get("kurir");
                    foreach ($kur->result() as $kr) {
                      $kurir = explode("|", $this->func->getSetting("kurir"));
                      if (in_array($kr->id, $kurir)) {
                        echo '<option value="' . $kr->rajaongkir . '">' . $kr->nama . '</option>';
                      }
                    }
                    ?>
                  </select>
                  <div class="dropDownSelect2"></div>
                </div>
              </div>
              <div class="col-md-4 mb-2">
                <div class="rs1-select2 rs2-select2 bor8 bg0 w-full" id="paketform">
                  <select class="js-select2 paket" name="paket">
                    <option value="">Pilih Paket Pengiriman</option>
                  </select>
                  <div class="dropDownSelect2"></div>
                </div>
              </div>
              <div class="error text-danger col-md-12 m-t-10" id="error-kurir" style="display:none;">
                <span><i class="fa fa-exclamation-circle"></i> pilihan ekspedisi atau paket pengiriman tidak tersedia, silahkan pilih yg lain.</span>
                <p />
              </div>
            </div>
              <p class="note1"><b><i>*kurir toko khusus untuk area Kota Blitar.</i></b></p>
          </div>
          <div class="col-md-6 mt-3 mb-3">
            <b>Dropship</b>
            <div class="mt-3">
              <button type="button" id="nodrop" class="btn btn-rounded btn-success"><i class="fa fa-check-circle-o"></i> Tidak Dropship</button>
              <button type="button" id="yesdrop" class="btn btn-rounded btn-outline-success"><i class="fa fa-check-circle-o" style="display:none"></i> Dropship</button>
            </div>
            <div class="form-group mb-0 float-label" id="dropform" style="display:none;">
              <input type="text" name="dropship" class="form-control " placeholder="nama pengirim" />
              <input type="text" name="dropshipnomer" class="form-control " placeholder="no telepon" />
              <input type="text" name="dropshipalamat" class="form-control " placeholder="alamat pengirim" />
            </div>
          </div>

          <div class="col-12 px-0 card border-0 shadow-sm mb-3 mt-3">
            <div class="card-body">
              <div class="row">
                <div class="col-auto">
                  <span class="btn btn-success p-3 rounded-circle">
                    <i class="material-icons">local_activity</i>
                  </span>
                </div>
                <div class="col">

                  <div class="form-group mb-0 float-label active">
                    <input type="hidden" name="diskon" id="diskon" value='0' />
                    <input type="text" class="form-control" id="kodevoucher" name="kodevoucher">
                    <label class="form-control-label">Tambahkan Kupon</label>
                    <div class="m-t-20">
                      <div class="vouchergagal badge badge-danger" style="display:none;">Maaf, Voucher sudah tidak berlaku</div>
                      <div class="vouchersukses badge badge-success" style="display:none;">Selamat, Voucher berhasil dipakai dan nikmati potongannya</div>
                    </div>
                  </div>

                </div>
                <div class="col-auto align-self-center">
                  <button type="button" onclick="cekVoucher()" class="btn btn-default button-rounded-36 shadow"><i class="material-icons">arrow_forward</i></button>
                </div>
              </div>
            </div>
          </div>
          <?php
          $this->db->where("mulai <=", date("Y-m-d"));
          $this->db->where("selesai >=", date("Y-m-d"));
          $voc = $this->db->get("voucher");
          foreach ($voc->result() as $v) {
            $pot = $this->func->formUang($v->potongan);
            $potongan = $v->tipe == 2 ? "Rp " . $pot : $pot . "%";
            $jenis = $v->jenis == 1 ? "Harga" : "Ongkir";
            echo '
               
                <div class="col-12 px-0">
            <div class="coupon bg-white rounded mb-3 d-flex justify-content-between">
              <div class="kiri p-3">
                <div class="icon-container ">
                  <div class="icon-container_box">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAHtUlEQVR4nO2UQY4kMQzD5v+f3v1B10EQqCQkULe2I8tW//39/f27/PsirU/7t+drQ++37i8tgD6g9gHS87Wh91v3lxZAH1D7AOn52tD7rftLC6APqH2A9Hxt6P3W/aUF0AfUPkB6vjb0fuv+0gLoA2ofID1fG3q/dX9pAfQBtQ+Qnq8Nvd+6v7QA+oDaB0jP14beb93f9QV8sX7gbV5/P8WAlPvT/r3+fooBKfen/Xv9/RQDUu5P+/f6+ykGpNyf9u/191MMSLk/7d/r76cYkHJ/2r/X308xIOX+tH+vv59SD0h6QO3329D+rfdv66vrv37AMrR/6/3b+gxIOmAZ2r/1/m19BiQdsAzt33r/tj4Dkg5YhvZvvX9bnwFJByxD+7fev63PgKQDlqH9W+/f1mdA0gHL0P6t92/rMyDpgOO0/Wlz/f1cP+A4BmT8fq4fcBwDMn4/1w84jgEZv5/rBxzHgIzfz/UDjmNAxu/n+gHHMSDj93P9gOMYkPH7uX7AEPWx9fj9XD9giPrYevx+rh8wRH1sPX4/1w8Yoj62Hr+f6wcMUR9bj9/P9QOGqI+tx+/n+gFD1MfW4/dz/YAh6mPr8ftpL7ANrZ8+MJrr9V8/4Pj7tP6U6/VfP+D4+7T+lOv1Xz/g+Pu0/pTr9V8/4Pj7tP6U6/VfP+D4+7T+lOv1Xz/g+Pu0/pTr9V8/4Pj7tP6U6/V//eD0LzbI+qj+9A8XUB/wA+u79ad/uID6gB9Y360//cMF1Af8wPpu/ekfLqA+4AfWd+tP/3AB9QE/sL5bf/qHC6gP+IH13frTP1xAfcAPrO/Wn/49D30gp+uTy1k/wHV9cjnrB7iuTy5n/QDX9cnlrB/guj65nPUDXNcnl7N+gOv65HLWD3Bd3/XQC6YPqK1/fX66nuZTP73A9QUZkG49jQEJMSDdehoDEmJAuvU0BiTEgHTraQxIiAHp1tMYkBAD0q2nMSAhBqRbTxMHhBZIL3A9gG397Xr6qxuQcvuBpP1p/QYkbRBy+4Gk/Wn9BiRtEHL7gaT9af0GJG0QcvuBpP1p/QYkbRBy+4Gk/Wn9BiRtEHL7gaT9af0GJG0QcvuBpP1p/c8HhB6wrY+up+c//X1af9xgfUF0PT3/6e/T+uMG6wui6+n5T3+f1h83WF8QXU/Pf/r7tP64wfqC6Hp6/tPfp/XHDdYXRNfT85/+Pq0/brC+ILqenv/092n9cYP1BdH19Pynv0/rrzdYX2Bb//p8bdb9NyCw/vX52qz7b0Bg/evztVn334DA+tfna7PuvwGB9a/P12bdfwMC61+fr826/wYE1r8+X5t1/w0IrH99vjbr/uP+0QeybqD9WXD99IIMyNn92+D66QUZkLP7t8H10wsyIGf3b4PrpxdkQM7u3wbXTy/IgJzdvw2un16QATm7fxtcP70gA3J2/zaf+k8fMKUdEPpA/YMI+xsQA2JAQIHrGBADggpcx4AYEFTgOgbEgKAC1zEgBgQVuI4BMSCowHUMiAGJGrQP5Pavzfr+2v3r+3vdgLrBZdb31+5f39/rBtQNLrO+v3b/+v5eN6BucJn1/bX71/f3ugF1g8us76/dv76/1w2oG1xmfX/t/vX9vW5A3eAy6/tr96/v73UD6gaXWd9fu399f+0B2wa1+9P+pND66f3F+l43gH6/Da2f3l+s73UD6Pfb0Prp/cX6XjeAfr8NrZ/eX6zvdQPo99vQ+un9xfpeN4B+vw2tn95frO91A+j329D66f3F+l43gH6/Da2f3l+sjzYgZX0Bt7/f/tr64wfa9SnzBl/+vgEp16fMG3z5+wakXJ8yb/Dl7xuQcn3KvMGXv29AyvUp8wZf/r4BKdenzBt8+fsGpFyfMm/w5e9fH5DXOf0PIoXWb0DGMSAGRH5gQAyI/MCAGBD5gQExIPIDA2JA5AcGxIDIDwyIAak+sP6l3B4gej7an8/36QM2ICz0fLQ/BiTk9AP4gp6P9seAhJx+AF/Q89H+GJCQ0w/gC3o+2h8DEnL6AXxBz0f7Y0BCTj+AL+j5aH8MSMjpB/AFPR/tTxyQdU4PSPsPgP6DSfXh/WmDUtYNNiCZPrw/bVDKusEGJNOH96cNSlk32IBk+vD+tEEp6wYbkEwf3p82KGXdYAOS6cP70walrBtsQDJ9eH/aoJR1gw1Ipg/vTy+g/X5bfwqtf/3DWV/Quv4UWv/6h7O+oHX9KbT+9Q9nfUHr+lNo/esfzvqC1vWn0PrXP5z1Ba3rT6H1r3846wta159C61//cNYXtK4/hda//uGsL2hdv/q2Dzz2j14gvSD1GRADUtSvPgNSFUAvSH0GxIAU9avPgFQF0AtSnwExIEX96jMgVQH0gtRnQAxIQHu+tj8pz/trQNj5DMj2Z0Dg+QzI9mdA4PkMyPZnQOD5DMj2Z0Dg+QzI9mdA4PkMyPZnQOD5DMj2hy8ghQ4Y7d/pAWv7b0A+vvX6FANiQH5CHzjtnwExID+hD5z2z4AYkJ/QB077Z0AMyE/oA6f9MyAG5Cf0gdP+GRAD8hP6wGn/DAgckNO/1ODX69P+Ke33DUhq0OP1BuTyLzbo8XoDcvkXG/R4vQG5/IsNerzegFz+xQY9Xm9ALv9igx6vNyCXf7FBj9c/HZD/F0W1k4x1XWcAAAAASUVORK5CYII=" width="85" alt="totoprayogo.com" class="" />
                  </div>
                </div>
              </div>
              <div class="tengah py-3 d-flex w-100 justify-content-start">
                <div>
                  <span class="badge badge-success">' . $v->nama . '</span>
                  <h3 class="lead">Kode: <strong>' . $v->kode . '</strong> </h3>
                  <h6 class="text-muted mb-0">Potong: ' . $jenis . '</h6>
                  <p class="text-muted mb-0">Min Beli: Rp. ' . $this->func->formUang($v->potonganmin) . '</p>
                </div>
              </div>
              <div class="kanan">
                <div class="info text-center mt-5 m-3 d-flex align-items-center">
                  <div class="w-100">
                    <div class="block">
                      <span class="time font-weight-light">
                        <span>' . $potongan . '</span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
                ';
          }
          ?>
          <?php
          $totalharga = 0;
          $totalbayar = 0;
          $totalberat = 0;
          ?>
          <div class="col-md-12 m-r-auto m-l-auto">

            <?php
            foreach ($data->result() as $res) {
              $produk = $this->func->getProduk($res->idproduk, "semua");
              $totalbayar += $res->harga * $res->jumlah;
              $totalberat += $produk->berat * $res->jumlah;
              $totalproduks = $res->harga * $res->jumlah;
              $totalharga += $res->harga * $res->jumlah;
              $variasee = $this->func->getVariasi($res->variasi, "semua");
              $variasi = ($res->variasi != 0) ? $this->func->getWarna($variasee->warna, "nama") . " " . $produk->subvariasi . " " . $this->func->getSize($variasee->size, "nama") : "";
              $variasi = ($res->variasi != 0) ? "<br/><small class='text-primary'>" . $produk->variasi . ": " . $variasi . "</small>" : "";
            ?>
              <div class="row">
                <div class="col-12 px-0">
                  <ul class="list-group list-group-flush mb-1">

                    <li class="list-group-item">
                      <div class="row">
                        <div class="col-2 align-self-center">
                          <figure class="product-image h-auto"><img src="<?php echo $this->func->getFoto($produk->id, "utama"); ?>" alt="Redmi Note 9 6/128 putih class=" vm"></figure>
                        </div>
                        <div class="col px-0">
                          <p class="text-dark mb-1 h6 d-block"><?php echo $produk->nama . $variasi; ?></p>
                          <p class="text-secondary small text-mute mb-0">Harga: Rp <?php echo $this->func->formUang($res->harga); ?></p>
                          <p class="text-secondary small text-mute mb-0">Jumlah: <?php echo $res->jumlah; ?></p>
                          <h5 class="text-success font-weight-normal mb-0">Rp <?php echo $this->func->formUang($res->harga * $res->jumlah); ?></h5>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <input type="hidden" class="idproduks" name="idproduk[]" value="<?php echo $res->id; ?>" />
              <input type="hidden" id="totalproduks_<?php echo $res->id; ?>" value="<?php echo $totalproduks; ?>" />
              <input type="hidden" id="namaproduks_<?php echo $res->id; ?>" value="<?php echo $produk->nama; ?>" />
              <input type="hidden" id="kategoriproduks_<?php echo $res->id; ?>" value="<?php echo $this->func->getKategori($produk->idcat, "nama"); ?>" />

            <?php
            }
            ?>
            <input type="hidden" id="totalharga" value="<?php echo $totalharga; ?>" />
            <input type="hidden" name="berat" class="berat" id="berat" value="<?php echo $totalberat; ?>" />
            <input type="hidden" name="ongkir" class="ongkir" id="ongkir" value="0" />
            <!-- RAJAONGKIR -->
            <input type="hidden" name="dari" id="dari" value="<?php echo $this->func->getSetting("kota"); ?>" />
            <input type="hidden" id="subtotal" value="<?php echo $totalbayar; ?>" />
            <input type="hidden" id="total" name="total" value="<?php echo $totalbayar; ?>" />
            <!-- Subtotal -->
          </div>
        </div>
        <div class="full card border-0 shadow-sm mt-3">
          <div class="card-body">
            <div class="row ">
              <div class="col-4">
                <p class="text-secondary mb-1 small">Sub Total</p>
                <h6 class="mb-0">Rp <span id="subtotalbayar"><?php echo $this->func->formUang($totalbayar); ?></h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-secondary mb-1 small">Ongkir</p>
                <h6 class="mb-0">
                  <p>Rp <span id="ongkirshow">0</span></p>
                </h6>
              </div>
              <div class="col-4 text-right">
                <p class="text-secondary mb-1 small">Discount</p>
                <h6 class="mb-0">- Rp <span id="diskonshow">0</span></h6>
              </div>
            </div>
          </div>
        </div>
        <div class="full card mb-4 border-0 shadow-sm border-top-dashed">
          <div class="card-body text-center">
            <p class="text-secondary my-1">Total Bayar</p>
            <h3 class="mb-0">
              <p>Rp <span id="totalbayar"><?php echo $this->func->formUang($totalbayar); ?></p>

            </h3>
          </div>
        </div>
        <div class="error cl1" id="error-bayar">
          <i>Belum dapat menyelesaikan pesanan, silahkan lengkapi alamat dan pilih kurir terlebih dahulu.</i>
          <p />
        </div>
        <div class="cl1" id="proses" style="display:none;">
          <h5><i class="fa fa-circle-o-notch fa-spin"></i> <i>Memproses pesanan, mohon tunggu sebentar</i></h5>
          <p />
        </div>
        <div class="pembayaran" style="display:none;">
          <b>Metode Pembayaran</b>
          <div class="row">
            <div class="col-4 col-md-2">
              <select class="js-select2" name="metode" id="metode">
                <option value="1">Transfer</option>
                <?php if ($saldo > 0) { ?>
                  <option value="2">Saldo (Rp <?php echo $this->func->formUang($saldo); ?>)</option>
                <?php } ?>
              </select>
              <div class="dropDownSelect2"></div>
            </div>
            <div class="col-6 col-md-6">
              <?php if ($saldo > 0) { ?>
                <div class="col-md-6">
                  Potong Saldo:<br />
                  <h5 id="saldo">Rp 0</h5>
                </div>
              <?php } ?>
              <div class="col-md-6">
                Transfer: <h5 id="transfer">Rp <?php echo $this->func->formUang($totalbayar); ?></h5>
              </div>
            </div>
            <input type="hidden" id="saldoval" value="<?php echo $saldo ?>" />
            <input type="hidden" name="saldo" id="saldopotong" value="0" />
          </div>
        </div>
        <div class="pembayaran" style="display:none;">
        </div>
        <div class="text-center mt-3">
          <a href="javascript:void(0);" onclick="checkoutNow();" class="btn btn-lg btn-default shadow btn-rounded"><span>lanjut bayar</span><i class="material-icons">arrow_forward</i></a>
        </div>
      </form>

    </div>
  </div>

  <?php include 'footermobiletukunen.php'; ?>

  <!-- jquery, popper and bootstrap js -->
  <script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
  <script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
  <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
  <!-- cookie js -->
  <script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

  <!-- swiper js -->
  <script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

  <!-- template custom js -->
  <script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
  <script src="<?= base_url('assets/js/main.js') ?>"></script>
  <script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
  <!-- chosen multiselect js -->
  <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


  <!-- page level script -->
  <script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

  <script type="text/javascript">
    $(".js-select2").each(function() {
      $(this).select2({
        minimumResultsForSearch: 20,
        dropdownParent: $(this).next('.dropDownSelect2')
      });
      /* chosen select*/
    });
    $(".chosen").chosen();
  </script>

</body>

</html>



<script type="text/javascript">
  $(function() {
    $("#cekout").on("submit", function(e) {
      e.preventDefault();
    });

    $("#nodrop").click(function() {
      $("#yesdrop").removeClass("btn-success");
      $("#yesdrop").addClass("btn-outline-success");
      $(this).removeClass("btn-outline-success");
      $(this).addClass("btn-success");
      $(".fa", this).show()
      $("#yesdrop .fa").hide();
      $("#dropform").hide();
      $("#dropform input").val("");
      $("#dropform input").prop("required", false);
    });
    $("#yesdrop").click(function() {
      $("#nodrop").removeClass("btn-success");
      $("#nodrop").addClass("btn-outline-success");
      $(this).removeClass("btn-outline-success");
      $(this).addClass("btn-success");
      $("#dropform").show();
      $(".fa", this).show()
      $("#nodrop .fa").hide();
      $("#dropform input").prop("required", true);
    });

    $(".ongkir").change(function() {
      var ongkir = $(this).val();
      //if(ongkir > 0){
      var harga = $("#totalharga").val();
      var totalharga = Number(harga) + Number(ongkir);

      //$("#ongkirtoko").html(formUang(ongkir));
      //$("#totaltoko").html(formUang(totalharga));
      //}
    });

    $("#idalamat").change(function() {
      var idalamat = $(this).val();
      var tujuan = $("#alamat_" + idalamat).data('tujuan');
      $("#tujuan").val(tujuan);

      $(".alamat").hide();
      if ($(this).val() == "") {
        resetOngkir();
        $(".tambahalamat").hide();
        $(".tambahalamat input,.tambahalamat textarea").prop("required", false);
      } else if ($(this).val() == 0) {
        resetOngkir();
        $(".tambahalamat").show();
        $(".tambahalamat input,.tambahalamat textarea").prop("required", true);
        if ($("#kab").val() != "") {
          $("#tujuan").val($("#kab").val());
          hitungOngkir();
        } else {
          resetOngkir();
        }
      } else if ($(this).val() > 0) {
        $("#alamat_" + idalamat).show();
        $(".tambahalamat").hide();
        $(".tambahalamat input,.tambahalamat textarea").prop("required", false);

        hitungOngkir();
      }
    });

    $("#kodevoucher").change(function() {
      $("#diskon").val(0);
      $("#diskonshow").html(0);
      //$("#totalbayar").html(formUang($("#total").val()));
      hitungOngkir();
      $(".vouchergagal").hide();
      $(".vouchersukses").hide();
    });
  });

  //CEK VOUCHER
  function cekVoucher() {
    if ($("#kodevoucher").val() != "") {
      $.post("<?= site_url("assync/cekvoucher") ?>", {
        "kode": $("#kodevoucher").val(),
        "harga": $("#totalharga").val(),
        "ongkir": $("#ongkir").val()
      }, function(msg) {
        var data = eval("(" + msg + ")");
        if (data.success == true) {
          var total = parseFloat($("#total").val()) - data.diskon;
          $("#diskon").val(data.diskon);
          $("#diskonshow").html(formUang(data.diskon));
          $("#totalbayar").html(formUang(total));
          $("#transfer").html(formUang(total));
          $(".vouchergagal").hide();
          $(".vouchersukses").show();
        } else {
          $("#diskon").val(0);
          $("#diskonshow").html(0);
          $(".vouchergagal").show();
          $(".vouchersukses").hide();
        }
      });
    } else {
      swal("Masukkan Kode Voucher!", "masukkan kode voucher terlebih dahulu lalu klik tombol cek voucher", "warning");
    }
  }

  //HITUNG ONGKIR
  function hitungOngkir() {
    if (($("#idalamat").val() != "" && $("#idalamat").val() != "0") || $("#tujuan").val() > 0) {
      var tujuan = $("#tujuan").val();

      var kurir = $(".kurir").val();
      var service = $(".paket").val();
      var berat = $("#berat").val();
      var dari = $("#dari").val();
      if (kurir != "" && service != "") {
        $.post("<?php echo site_url("assync/cekongkir"); ?>", {
          "berat": berat,
          "dari": dari,
          "tujuan": tujuan,
          "kurir": kurir,
          "service": service
        }, function(msg) {
          var data = eval("(" + msg + ")");
          if (data.success == true) {
            $("#ongkir").val(data.harga).trigger("change");
            if (data.harga == 0 && $(".kurir").val() != "cod") {
              errorKurir();
            }
            calculateOngkir();
          } else {
            $("#ongkir").val(0).trigger("change");
            calculateOngkir();
            errorKurir();
          }
        });
      }
    } else {
      swal("Penting!", "mohon cek kembali dan pastikan alamat sudah benar", "error");
    }
  }

  function calculateOngkir() {
    var sum = 0;
    var sumi = true;
    $(".ongkir").each(function() {
      sum += parseFloat($(this).val());
      if ($(this).val() > 0 && sumi == true) {
        sumi = true;
      } else {
        if ($(".kurir").val() == "cod") {
          sumi == true;
        } else {
          sumi = false;
        }
      }
    });
    var totalbayar = sum + parseFloat($("#subtotal").val()) - parseFloat($("#diskon").val());

    if (sumi == false) {
      $(".pembayaran").hide();
      $("#error-bayar").show();
    } else {
      $(".pembayaran").show();
      $("#error-bayar").hide();
    }

    $("#total").val(totalbayar);
    $("#totalbayar").html(formUang(totalbayar));
    $("#ongkirshow").html(formUang(sum));
    //$('html, body').animate({ scrollTop: $("#totalbayar").offset().top - 400 });

    //RESET PEMBAYARAN
    $("#transfer").html("Rp " + formUang(totalbayar));
    $("#saldo").html("Rp 0");
    $("#saldopotong").val(0);
    $("#metode option").prop("selected", false);
    $("#metode option[value=1]").prop("selected", true).trigger("change");
  }

  function resetOngkir() {
    var total = parseFloat($("#subtotal").val()) - parseFloat($("#diskon").val());
    $("#total").val($("#subtotal").val());
    $("#totalbayar").html(formUang(total));
    $("#saldo").html("Rp 0");
    $(".ongkir").val(0);
    $("#ongkirshow").html(0);
    //$("#tujuan").val(0);

    //RESET PEMBAYARAN
    $(".pembayaran").hide();
    $("#error-bayar").show();
    $("#transfer").html("Rp " + formUang($("#subtotal").val()));
    $("#saldopotong").val(0);
    $("#saldo").html("Rp 0");
    $("#metode option").prop("selected", false);
    $("#metode option[value=1]").prop("selected", true).trigger("change");
  }

  function errorKurir() {
    $("#error-kurir").show();
    //$('html, body').animate({ scrollTop: $("#error-kurir-"+idtoko).offset().top - 260 });
  }

  //METODE Bayar
  $("#metode").change(function() {
    var saldo = parseFloat($("#saldoval").val());
    var total = parseFloat($("#total").val());

    if ($(this).val() == 2) {
      if (saldo >= total) {
        $("#saldopotong").val(total);
        $("#saldo").html("Rp " + formUang(total));
        $("#transfer").html("Rp 0");
      } else {
        var selisih = total - saldo;
        $("#saldopotong").val(saldo);
        $("#saldo").html("Rp " + formUang(saldo));
        $("#transfer").html("Rp " + formUang(selisih));
      }
    } else {
      $("#saldopotong").val(0);
      $("#saldo").html("Rp 0");
      $("#transfer").html("Rp " + formUang(total));
    }
  });

  //KURIR PENGIRIMAN
  $(".kurir").change(function() {
    $("#error-kurir").hide();
    if ($(this).val() != "") {
      var data = $("#service" + $(this).val()).html();
      $(".paket").html(data)
      $(".ongkir").val("0");
      $("#ongkirshow").html("0");
      $(".alamatform").show();

      hitungOngkir();
    } else {
      resetOngkir();
      $(".alamatform").hide();
    }
  });
  $(".paket").change(function() {
    $("#error-kurir").hide();
    hitungOngkir();
  });

  //LOAD KABUPATEN KOTA & KECAMATAN
  $("#prov").change(function() {
    $("#kab").html("<option value=''>Loading...</option>");
    $("#kec").html("<option value=''>Kecamatan</option>");
    resetOngkir();
    $.post("<?php echo site_url("assync/getkab"); ?>", {
      "id": $(this).val()
    }, function(msg) {
      var data = eval("(" + msg + ")");
      $("#kab").html(data.html);
    });
  });
  $("#kab").change(function() {
    resetOngkir();
    $("#kec").html("<option value=''>Loading...</option>");
    $.post("<?php echo site_url("assync/getkec"); ?>", {
      "id": $(this).val()
    }, function(msg) {
      var data = eval("(" + msg + ")");
      $("#kec").html(data.html);
    });
  });
  $("#kec").change(function() {
    var data = $(this).find(":selected").val();
    $("#tujuan").val(data);
    hitungOngkir();
  });

  function checkoutNow() {
    $(".pembayaran").hide();
    $("#proses").show();
    var total = parseFloat($("#total").val());

    $(".idproduks").each(function() {
      var id = $(this).val();
      var nama = $("#namaproduks_" + id).val();
      var kategori = $("#kategoriproduks_" + id).val();
      var total = $("#totalproduks_" + id).val();
      fbq('track', 'Purchase', {
        content_ids: id,
        content_type: kategori,
        content_name: nama,
        currency: "IDR",
        value: total
      });
    });

    $.post("<?php echo site_url("assync/bayarpesanan"); ?>", $("#cekout").serialize(), function(msg) {
      var data = eval("(" + msg + ")");
      if (data.success == true) {
        window.location.href = data.url;
      } else {
        $(".pembayaran").show();
        $("#proses").hide();
      }
    });
  }
</script>

<!-- ADDITIONAL DATA FOR RAJAONGKIR -->
<select id="servicecod" style="display:none;">
  <option value="cod" selected>COD</option>
</select>
<select id="servicetoko" style="display:none;">
  <option value="toko" selected>Kurir Toko</option>
</select>
<?php
$kr = $this->db->get("kurir");
foreach ($kr->result() as $kur) {
?>
  <select id="service<?php echo $kur->rajaongkir; ?>" style="display:none;">
    <option value="">Pilih Paket</option>
    <?php
    $this->db->where("idkurir", $kur->id);
    $pak = $this->db->get("paket");
    $no = 1;
    foreach ($pak->result() as $pk) {
      $select = ($no == 1) ? "selected" : "";
      echo '<option value="' . $pk->rajaongkir . '" ' . $select . '>' . $pk->nama . '</option>';
      $no++;
    }
    ?>
  </select>
<?php
}
?>