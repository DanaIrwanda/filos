<?php include 'loader.php'; ?>
<div class="sidebar">
	<?php include 'menutukunen.php'; ?>
</div>
<div class="wrapper">
	<?php include 'menuheadtukunen.php'; ?>

	<?php
	$this->db->where("url", $url);
	$db = $this->db->get("blog");

	foreach ($db->result() as $res) {
	?>
		<!-- breadcrumb -->
		<div class="container mb-3">
			<a href="<?php echo site_url(); ?>">
				Home
				<i class="fa fa-angle-right" aria-hidden="true"></i>
			</a>
			<a href="<?php echo site_url("blog"); ?>">
				Blog
				<i class="fa fa-angle-right" aria-hidden="true"></i>
			</a>

			<span>
				<?= $this->func->potong($res->judul, 60, "...") ?>
			</span>
		</div>


		<div class="container">

			<div class="col-12">
				<div class="swiper-container news-slide mb-0">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<div class="card shadow-sm border-0 bg-dark text-white">
								<figure class="background">
									<img src="<?= base_url("admin/uploads/" . $res->img) ?>" alt="<?= $res->judul ?>"></figure>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-12">
				<div class="full">
					<h3><b><?= $res->judul ?></b></h3>
				</div>
				<div class="small">
					Diposting oleh <b>admin</b> pada <?= $this->func->ubahTgl("d M Y", $res->tgl) ?>
				</div>
				<div class="col-12">
					<?= $res->konten ?>
				</div>
			</div>
		<?php
	}
		?>
		</div>
		<?php include 'footermobiletukunen.php'; ?>


		<!-- jquery, popper and bootstrap js -->
		<!-- jquery, popper and bootstrap js -->
		<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
		<!-- cookie js -->
		<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

		<!-- swiper js -->
		<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

		<!-- template custom js -->
		<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
		<script src="<?= base_url('assets/js/main.js') ?>"></script>
		<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>
		<!-- chosen multiselect js -->
		<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>


		<!-- page level script -->
		<script type="text/javascript" src="<?= base_url('assets/vendor/select2/select2.min.js') ?>"></script>

		<script type="text/javascript">
			$(".js-select2").each(function() {
				$(this).select2({
					minimumResultsForSearch: 20,
					dropdownParent: $(this).next('.dropDownSelect2')
				});
				/* chosen select*/
			});
			$(".chosen").chosen();
		</script>