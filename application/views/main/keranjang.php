<body>
	<?php include 'loader.php'; ?>
	<div class="sidebar">
		<?php include 'menutukunen.php'; ?>
	</div>
	<div class="wrapper">
		<?php include 'menuheadtukunen.php'; ?>
		<div class="container">
			<?php
			$keranjang = (isset($_SESSION["usrid"]) and $_SESSION["usrid"] > 0) ? $this->func->getKeranjang() : 0;
			$hapusProduk = "";
			if ($keranjang > 0) {
			?>

				<div class="row">
					<div class="col-12 px-0">
						<ul class="list-group list-group-flush mb-4">

							<?php
							$this->db->where("usrid", $_SESSION['usrid']);
							$this->db->where("idtransaksi", 0);
							$ca = $this->db->get("transaksiproduk");
							$totalbayar = 0;
							foreach ($ca->result() as $car) {
								$produk = $this->func->getProduk($car->idproduk, "semua");
								if ($produk == null) {
									$hapusProduk .= "hapusProduk(" . $car->id . ")";
								}
								$totalbayar += $car->harga * $car->jumlah;
								$variasi = $this->func->getVariasi($car->variasi, "semua");
							?>

								<li class="list-group-item">
									<div class="row">
										<div class="col-auto align-self-center">
											<a href="javascript:hapus(<?= $car->id ?>)"><button class="btn btn-sm btn-link p-0 float-right"><i class="material-icons">remove_circle</i></button></i></a>
										</div>
										<div class="col-2 pl-0 align-self-center">
											<figure class="product-image h-auto"><img src="<?php echo $this->func->getFoto($produk->id, "utama"); ?>" alt="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L] class=" vm"></figure>
										</div>
										<div class="col px-0">
											<a href="<?= base_url('produk/' . $produk->url) ?>" class="text-dark mb-1 h6 d-block"><?php echo $produk->nama; ?></a>
											<p>
												<?php
												if ($car->variasi > 0) {
													echo "<span style='font-size:80%;color:#aaa;'><b>" . $produk->variasi . ": </b> " . $this->func->getWarna($variasi->warna, 'nama') . " " . $produk->subvariasi . " " . $this->func->getSize($variasi->size, 'nama') . "</span>";
												}
												if ($car->keterangan != "") {
													echo "<br/><span style='font-size:80%;color:#c0392b;'><b>Note: </b> <i>" . $car->keterangan . "</i></span>";
												}
												?>

											</p>
											<p class="text-secondary small text-mute mb-0">Harga: Rp <?php echo $this->func->formUang($car->harga); ?></p>
											<h5 class="text-success font-weight-normal mb-0">Total: Rp <span id="totalhargauang_<?php echo $car->id; ?>"><?php echo $this->func->formUang($car->harga * $car->jumlah); ?></h5>

											<br />
										</div>
										<div class="col-auto align-self-center">
											<div class="input-group mb-3">
												<div class="btn-num-product-down input-group-prepend ">
													<button type="button" class="btn btn-light-grey px-1"><i class="material-icons">remove</i></button>
												</div>

												<input class="w-45 num-product produk-jumlah" type="text" min="<?php echo $produk->minorder; ?>" id="jumlah_<?php echo $car->id; ?>" name="jumlah[]" value="<?php echo $car->jumlah; ?>" data-proid="<?php echo $car->id; ?>" />

												<div class="btn-num-product-up input-group-prepend">
													<button type="button" class="btn btn-light-grey px-1"><i class="material-icons">add</i></button>
												</div>
											</div>
										</div>

									</div>
								</li>

								<div class="container">
									<div class="row">
										<div class="col-6">
										</div>
									</div>
								</div>

								<td class="column-4">

								</td>
								<input type="hidden" id="harga_<?php echo $car->id; ?>" value="<?php echo $car->harga; ?>" />
								<input type="hidden" class="totalhargaproduk" id="totalharga_<?php echo $car->id; ?>" value="<?php echo $car->harga * $car->jumlah; ?>" />
								<input type="hidden" name="id[]" value="<?php echo $car->id; ?>" />
							<?php
							}
							?>


						</ul>
					</div>
				</div>
				<div class="full card mb-4 border-0 shadow-sm border-top-dashed">
					<div class="card-body text-center">
						<p class="text-secondary my-1">Total Bayar</p>
						<h3 class="mb-0">Rp <span id="totalbayar"><?php echo $this->func->formUang($totalbayar); ?></span></h3>
						<br>
					</div>
					<div class="text-center mb-3">
						<a href="<?php echo site_url("shop"); ?>" class="btn btn-lg btn-default shadow btn-rounded"><i class="material-icons">arrow_back</i><span>belanja</span></a>
						<a href="<?php echo site_url("home/pembayaran"); ?>" class="btn btn-lg btn-default shadow btn-rounded"><span>bayar</span><i class="material-icons">arrow_forward</i></a>

					</div>

				</div>


		</div>
	</div>


<?php
			} else {
?>
	<div class="full card mb-4 border-0 shadow-sm border-top-dashed">
		<div class="card-body text-center">
			<p class="text-secondary my-1">Ups Keranjang Masih Kosong.</p>
			<h3 class="mb-0">Rp <span id="totalbayar"><?php echo $this->func->formUang($totalbayar); ?></span></h3>
			<br>
		</div>
		<div class="text-center mb-3">
			<a href="<?php echo site_url("shop"); ?>" class="btn btn-lg btn-default shadow btn-rounded"><i class="material-icons">arrow_back</i><span>belanja</span></a>

		</div>

	</div>

<?php
			}
?>

<?php include 'footermobiletukunen.php'; ?>

<!-- jquery, popper and bootstrap js -->
<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- cookie js -->
<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

<!-- swiper js -->
<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

<!-- template custom js -->
<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>




<!-- page level script -->

</body>

</html>

<script>
	$('.btn-num-product-down').on('click', function(e) {
		e.preventDefault();
		var numProduct = Number($(this).next().val());
		if (numProduct > 1) $(this).next().val(numProduct - 1).trigger("change");
	});

	$('.btn-num-product-up').on('click', function(e) {
		e.preventDefault();
		var numProduct = Number($(this).prev().val());
		$(this).prev().val(numProduct + 1).trigger("change");
	});

	<?= $hapusProduk ?>

	$(".produk-jumlah").on('change', function() {
		var jumlah = $(this).val();
		var prodid = $(this).attr("data-proid");
		var harga = Number($("#harga_" + prodid).val());
		var hargatotal = Number(jumlah) * harga;

		if (jumlah > 0) {

			if (jumlah < Number($(this).attr("min"))) {
				$(this).val($(this).attr("min")).trigger("change");
				return;
			}

			$.post("<?php echo site_url("assync/updatekeranjang"); ?>", {
				"update": prodid,
				"jumlah": jumlah
			}, function(msg) {
				var data = eval("(" + msg + ")");
				if (data.success == false) {
					swal("Gagal", data.msg, "error")
						.then((willDelete) => {
							location.reload();
						});
				}
			});

			$("#totalhargauang_" + prodid).html(formUang(hargatotal));
			$("#totalharga_" + prodid).val(hargatotal);
			var sum = 0;
			$(".totalhargaproduk").each(function() {
				sum += parseFloat($(this).val());
			});
			$("#totalbayar").html(formUang(sum));

		} else {
			swal({
					title: "Anda yakin?",
					text: "menghapus produk dari keranjang belanja",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						//$("#produk_"+prodid).hide();
						$.post("<?php echo site_url("assync/hapuskeranjang"); ?>", {
							"hapus": prodid
						}, function(msg) {
							var data = eval("(" + msg + ")");
							if (data.success == true) {
								location.reload();
							} else {
								confirm("Gagal menghapus pesanan, silahkan ulangi beberapa saat lagi");
							}
						});
					} else {
						$(this).val($(this).attr("min"));
					}
				});
		}
	});

	function hapus(id) {
		$("#jumlah_" + id).val(0).trigger("change");
	}

	function hapusProduk(id) {
		$.post("<?php echo site_url("assync/hapuskeranjang"); ?>", {
			"hapus": id
		}, function(msg) {
			var data = eval("(" + msg + ")");
			if (data.success == true) {
				location.reload();
			} else {
				confirm("Gagal menghapus pesanan, silahkan ulangi beberapa saat lagi");
			}
		});
	}
</script>