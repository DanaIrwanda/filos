<div class="text-center">
	<button class="btn btn-sm btn-default btn-rounded mt-3">produk terkait</button>
</div>
<div class="row mt-3">
	<!-- Swiper -->

	<?php
	$this->db->where("idcat", $kategori->id);
	$this->db->where("id!=", $data->id);
	$this->db->limit(6);
	$this->db->order_by("id", "RANDOM");
	$dbst = $this->db->get("produk");
	foreach ($dbst->result() as $re) {
		$levelt = isset($_SESSION["lvl"]) ? $_SESSION["lvl"] : 0;
		if ($levelt == 5) {
			$resultt = $re->hargadistri;
		} elseif ($levelt == 4) {
			$resultt = $re->hargaagensp;
		} elseif ($levelt == 3) {
			$resultt = $re->hargaagen;
		} elseif ($levelt == 2) {
			$resultt = $re->hargareseller;
		} else {
			$resultt = $re->harga;
		}
		$ulasan = $this->func->getReviewProduk($re->id);

		$this->db->where("idproduk", $re->id);
		$dbvt = $this->db->get("produkvariasi");
		$totalstokt = ($dbvt->num_rows() > 0) ? 0 : $re->stok;
		$hargst = 0;
		$hargat = array();
		foreach ($dbvt->result() as $rv) {
			$totalstokt += $rv->stok;
			if ($levelt == 5) {
				$hargat[] = $rv->hargadistri;
			} elseif ($levelt == 4) {
				$hargat[] = $rv->hargaagensp;
			} elseif ($levelt == 3) {
				$hargat[] = $rv->hargaagen;
			} elseif ($levelt == 2) {
				$hargat[] = $rv->hargareseller;
			} else {
				$hargat[] = $rv->harga;
			}
			$hargst += $rv->harga;
		}
	?>
		<div class="swiper-slide col-6 col-md-4 col-lg-3 col-xl-2">
			<div class="card shadow-sm border-0 mb-4">
				<div class="card-body">
					<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" class="add_wishlist btn btn-sm btn-link p-0"><i class="material-icons md-18">favorite_outline</i></button>-->
					<span class="badge badge-warning bdg-1"><?= $ulasan['nilai'] ?></span>
					<i class="stars" data-rating="<?= $ulasan['nilai'] ?>" data-num-stars="5"></i>

					<figure class="product-image"><a href="<?php echo site_url('produk/' . $re->url); ?>"><img src="<?php echo $this->func->getFoto($re->id, 'utama'); ?>" alt="<?php echo $re->nama; ?>"></a></figure>
					<a href="<?php echo site_url('produk/' . $re->url); ?>" class="text-dark mb-1 mt-2 h6 tulisan-card"><?php echo $re->nama; ?></a>
					<h5 class="text-success font-weight-normal mb-0">

						<div class="oldPrice">	
							<?php echo "Rp. " . $this->func->formUang($re->hargaagen); ?>
						</div>	

						<div class="newPrice">
							<?php
							if ($hargst > 0) {
								echo "Rp. " . $this->func->formUang(min($hargat));
							} else {
								echo "Rp. " . $this->func->formUang($resultt);
							}
							?>

						</div>
					</h5>
					
					<?php if ($hargst > 0): ?>
										
										<div class="product_marks" style="background: red;">
										<?php 
											$amount 	= $this->func->formUang($re->hargaagen) - $this->func->formUang($re->harga);
											$discount 	= $amount/$this->func->formUang($re->hargaagen)*100;

											echo "<b style='color:white;'>" . " " . intval($discount). "%" . "</b>";       
										?>
									</div>

										
									<?php else: ?>
									
									<div class="product_marks" style="background: blue;">
											<?php echo "<b style='color:white;'>" . "New" . "</b>"; ?>
										</div>

									<?php endif ?>
					<!--<button class="btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>
                                        -->
					<p class="text-secondary small text-mute mb-0"><?= $ulasan['ulasan'] ?> Ulasan</p>
					<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" data-ket="" class="add_cart btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>-->
				</div>
			</div>
		</div>

	<?php } ?>


</div>