<body oncontextmenu="return false">
	<?php include 'loader.php'; ?>
	<div class="wrapper">
		<?php include 'menuheadtukunen_produk.php'; ?>
		<div class="container">
			<?php
			$link = $this->func->arrEnc(array("category" => $data->idcat), "encode");
			$set = $this->func->getSetting("semua");
			$kategori = $this->func->getKategori($data->idcat, "semua");
			$kategorinama = is_object($kategori) ? $kategori->nama : "";
			$kategoriurl = is_object($kategori) ? $kategori->url : "";
			$textorder = $data->preorder == 0 ? "Tambah Ke Keranjang" : "Pre Order";

			$level = isset($_SESSION["lvl"]) ? $_SESSION["lvl"] : 0;
			if ($level == 5) {
				$result = $data->hargadistri;
			} elseif ($level == 4) {
				$result = $data->hargaagensp;
			} elseif ($level == 3) {
				$result = $data->hargaagen;
			} elseif ($level == 2) {
				$result = $data->hargareseller;
			} else {
				$result = $data->harga;
			}

			$this->db->where("idproduk", $data->id);
			$dbv = $this->db->get("produkvariasi");
			$totalstok = 0;
			$hargs = 0;
			foreach ($dbv->result() as $rv) {
				$totalstok += $rv->stok;
				if ($level == 5) {
					$harga[] = $rv->hargadistri;
				} elseif ($level == 4) {
					$harga[] = $rv->hargaagensp;
				} elseif ($level == 3) {
					$harga[] = $rv->hargaagen;
				} elseif ($level == 2) {
					$harga[] = $rv->hargareseller;
				} else {
					$harga[] = $rv->harga;
				}
				$hargs += $rv->harga;
			}
			?>
			<!-- Swiper -->
			<div data-pagination='{"el": ".swiper-pagination"}' data-zoom='{"enabled": true}' data-navigation='{"nextEl": ".swiper-button-next", "prevEl": ".swiper-button-prev"}' class="swiper-container product-details full bg-white mb-2">
				<div class="swiper-wrapper">

					<?php
					$this->db->where("idproduk", $data->id);
					$this->db->order_by("jenis", "DESC");
					$db = $this->db->get("upload");
					foreach ($db->result() as $res) {
					?>
						<div class="swiper-slide">

							<div class="swiper-zoom-container"><img src="<?= base_url("admin/uploads/" . $res->nama); ?>" alt="<?= $data->nama; ?>"></div>
						</div>

					<?php } ?>

				</div>
				<!-- Add Pagination -->
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
				<div class="swiper-pagination"></div>
			</div>

			<div class="bg-white full py-3 pl-3 pr-3 mb-2">


				<!--<a href="http://localhost/tukunenV2.0.2/profile"><button class="btn btn-sm btn-link p-0"><i class="material-icons md-18">favorite_outline</i></button></a>-->

				<a href="javascript:void(0)" class="btn btn-sm btn-default btn-rounded ml-2" data-toggle="modal" data-target="#share"><i class="material-icons mb-18 mr-2">share</i>Bagikan</a>




				<p class="text-secondary my-3 small">
					<?php
					$ulasan = $this->func->getBintang($data->id);
					$star = $ulasan["star"];
					$laku = $this->func->getLaku($data->id);

					?>
					<i class="stars" data-rating="<?= $ulasan["star"]; ?>" data-num-stars="5"></i>
					<!--<span class="text-dark vm ml-2">Rating 2.8</span>--> <span class="vm"><?= $ulasan["jml"]; ?> Ulasan</span>
				</p>


				<a href="<?= site_url('produk/' . $data->url); ?>" title="<?= $data->nama; ?>" id="js-name-detail" class="text-dark mb-1 mt-2 h6 d-block"><?= $data->nama; ?></a>
				<p class="text-secondary small mb-2">
					<?= $kategorinama; ?>
					| stok: <?php if ($dbv->num_rows() == 0) {
								$totalstok = $data->stok;
							}
							?>
					<?= $totalstok; ?>
					| Terjual: <?= $laku["terjual"]; ?>

				</p>
			</div>
			<div class="bg-white full py-3 pl-3 pr-3 mb-2">
				<h5>Info Produk</h5>
				<div class="text-secondary small mb-2">
					<table>
						<tr>
							<td class="tabel-info-produk">Produk</td>

							<td><?php if ($data->preorder != 0) { ?>
									PREORDER
								<?php } else { ?>
									Tersedia
								<?php } ?></td>
						</tr>
						<tr>
							<td>Kategori</td>
							<td><a href="<?= base_url("kategori/" . $kategoriurl); ?>"> <?= $kategorinama; ?>
								</a></td>
						</tr>

						<tr>
							<td>Berat</td>
							<td><?= $data->berat; ?> gram</td>
						</tr>
						<tr>
							<td>Stok</td>
							<?php if ($dbv->num_rows() == 0) {
								$totalstok = $data->stok;
							}
							?>
							<td><?= $totalstok; ?> produk</td>
						</tr>

						<tr>
							<td></td>
							<?php
							if ($dbv->num_rows() > 0) {
								foreach ($dbv->result() as $s) {
									$this->db->where("id", $s->warna);
									$dbf = $this->db->get("variasiwarna");
									foreach ($dbf->result() as $p)
										$this->db->where("id", $s->size);
									$dbsize = $this->db->get("variasisize");
									foreach ($dbsize->result() as $q)

							?>
									<td><?= $p->nama ?> : <?= $q->nama ?> (<?= $s->stok ?>)</td>

								<?php
								}
							} else {
								?>

							<?php
							}
							?>
						</tr>

					</table>

				</div>
			</div>
			<div class="bg-white full py-3 pl-3 pr-3 mb-2">
				<h5>Diskripsi</h5>

				<p class="text-secondary">
					<p><?= $data->deskripsi; ?></p>
				</p>
                
                
                <div class="oldPrice">	
					<?php echo "Rp. " . $this->func->formUang($data->hargaagen); ?>
				</div>	
                
				<div class="row mb-4">
					<div class="col">
						<h3 class="text-success font-weight-normal mb-0">
							<?php
							if ($hargs > 0) {
								echo "Rp " . $this->func->formUang(min($harga));
							}
							?>
						</h3>
						<p class="text-secondary small mb-2"> produk dilihat <?= $data->dilihat; ?> kali</p>
					</div>
					<?php if ($this->func->cekLogin() == true) { ?>
						<?php if ($data->preorder == 0) { ?>
							<!--<div class="p-t-10 p-b-10 p-l-10 p-r-20 m-b-16 m-t-16" style="border-radius:6px;background-color:#dcdde1;color:#c0392b;position:relative;align-items:middle;">
							<span onclick="$(this).parent().hide();" class="pointer" style="position:absolute;right:10px;"><i class="fa fa-times"></i></span>
							Sebelum membeli pastikan terlebih dahulu ketersediaan stok.
						</div>-->
						<?php } ?>
						<?php
						if ($dbv->num_rows() == 0) {
							$totalstok = $data->stok;
						}
						if ($data->preorder > 0) {
							$this->db->where("idproduk", $data->id);
							$t = $this->db->get("preorder");
							$totalorder = 0;
							foreach ($t->result() as $r) {
								$totalorder += $r->jumlah;
							}
							$totalstok = $totalstok - $totalorder;
						}

						if ($totalstok > 0) {
						?>
						<?php } else { ?>
							<?php if ($data->preorder == 0) { ?>
								<button class="btn btn-lg btn-danger shadow btn-rounded mr-3"> Stok habis </button>

							<?php } else { ?>
								<button class="btn btn-lg btn-danger shadow btn-rounded mr-3"> Kuota penuh </button>



							<?php } ?>
						<?php } ?>
						<div class="d-none d-sm-block mr-3">
							<button data-toggle="modal" data-target="#cart" type="button" class="btn btn-lg btn-default shadow btn-rounded"><i class="material-icons">shopping_cart</i>+ keranjang</button>
						</div>
						<div class="d-none d-sm-block mr-3">
							<a href="https://wa.me/<?= $this->func->getRandomWasap() ?>/?text=Halo,%20saya%20ingin%20membeli%20produk%20*<?= $data->nama ?>*%20apakah%20masih%20tersedia?" target="_blank"> <button class="btn btn-lg btn-success shadow btn-rounded"><i class="fa fa-whatsapp"></i> chat WA</button></a>
						</div>

					<?php } else { ?>

						<div class="d-none d-sm-block mr-3">
							<a href="<?= site_url("home/signin"); ?>"> <button class="btn btn-lg btn-default shadow btn-rounded"><i class="material-icons">shopping_cart</i>+ keranjang</button></a>
						</div>
						<div class="d-none d-sm-block mr-3">
							<a href="https://wa.me/<?= $this->func->getRandomWasap() ?>/?text=Halo,%20saya%20ingin%20membeli%20produk%20*<?= $data->nama ?>*%20apakah%20masih%20tersedia?" target="_blank"> <button class="btn btn-lg btn-success shadow btn-rounded"><i class="fa fa-whatsapp"></i> chat WA</button></a>
						</div>

					<?php } ?>




				</div>
				<!--<p class="text-secondary small mb-2"> produk dilihat 252 kali</p>-->
			</div>


			<nav>
				<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
					<a class="nav-item nav-link text-left active" id="nav-delivery-tab" data-toggle="tab" href="#nav-delivery" role="tab" aria-controls="nav-delivery" aria-selected="true">
						<div class="row">
							<div class="col-auto align-self-center pr-1">
								<span class="btn btn-success button-rounded-26">
									<i class="material-icons md-18 text-mute">comment</i>
								</span>
							</div>
							<div class="col pl-2">
								<p class="text-secondary mb-0 small text-truncate">Ulasan</p>
								<h6 class="text-dark my-0"><?= $ulasan["jml"]; ?></h6>
							</div>
						</div>
					</a>
					<a class="nav-item nav-link text-left" id="nav-booking-tab" data-toggle="tab" href="#nav-booking" role="tab" aria-controls="nav-booking" aria-selected="false">
						<div class="row">
							<div class="col-auto align-self-center pr-1">
								<span class="btn btn-warning button-rounded-26">
									<i class="material-icons md-18 text-mute">star</i>
								</span>
							</div>
							<div class="col pl-2">
								<p class="text-secondary mb-0 small text-truncate">Rating</p>
								<i class="stars" data-rating="<?= $ulasan["star"]; ?>" data-num-stars="5"></i>
							</div>
						</div>
					</a>
				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-delivery" role="tabpanel" aria-labelledby="nav-delivery-tab">
					<ul class="list-items">
						<?php
						$this->db->where("idproduk", $data->id);
						$this->db->limit(8);
						$this->db->order_by("nilai,id DESC");
						$re = $this->db->get("review");
						if ($re->num_rows() > 0) {
							foreach ($re->result() as $rev) {
								$staron = "<i class='fa fa-star text-warning'></i>";
								$staroff = "<i class='fa fa-star text-secondary'></i>";
								$star = "";
								for ($i = 1; $i <= 5; $i++) {
									$star .= ($i <= $rev->nilai) ? $staron : $staroff;
								}
								echo "
								<li>
								<div class='row'>
									<div class='col-6 mb-2'>
									
										<p>
											<i class='stars' data-rating=" . $rev->nilai . " data-num-stars='5'></i>
											<br>
										</p>
									</div>
									<div class='col-6 mb-3 text-right'>
										<p class='text-muted small'>" . $this->func->ubahTgl("d/m/Y", $rev->tgl) . "</p>
									</div>
									<!--<div class='col-auto align-self-start'>
										<figure class='avatar avatar-50'>
											<img src='http://localhost/tukunenV2.0.2/assets/images/profile/1597900243455.jpg' alt='ulasan Black Shark 2 Pro 8GB/128GB Include Rookie Kit - Iceberg Grey'>
										</figure>
									</div>-->
									<div class='col'>
										<h6>" . $this->func->getProfil($rev->usrid, "nama", "usrid") . "</h6>
										<p class='text-secondary small'>" . $rev->keterangan . "</p>
									</div>
								</div>
							</li>
	
							";
							}
						} else {
							echo "
							
							<div class='row'>
									<!--<div class='col-auto align-self-start'>
										<figure class='avatar avatar-50'>
											<img src='http://localhost/tukunenV2.0.2/assets/images/profile/1597900243455.jpg' alt='ulasan Black Shark 2 Pro 8GB/128GB Include Rookie Kit - Iceberg Grey'>
										</figure>
									</div>-->
									<div class='col ml-3'>
										<h6>Belum ada Ulasan</h6>
									</div>
							</div>
							";
						}
						?>
					</ul>
				</div>
				<div class="tab-pane fade" id="nav-booking" role="tabpanel" aria-labelledby="nav-booking-tab">
					<ul class="list-items">
						<li>
							<div class="row">
								<div class="col-5">
									<h4>Rata rata Rating</h4>

									<h2 class="bold padding-bottom-7"><?= $ulasan["star"]; ?><small>/ 5</small></h2>
									<span class="stars" data-rating="<?= $ulasan["star"]; ?>" data-num-stars="5"></span>
								</div>

								<?php
								foreach ($review->result_array() as $c) {
									$jumlah += $c['nilai'];
									$banyak += count($c['nilai']);
									$rata2 = $jumlah / $banyak;
									$rata = round($rata2, 1);
									if ($c['nilai'] == 1) {
										$bintang1 += count($c['nilai']);
									} else if ($c['nilai'] == 2) {
										$bintang2 += count($c['nilai']);
									} else if ($c['nilai'] == 3) {
										$bintang3 += count($c['nilai']);
									} else if ($c['nilai'] == 4) {
										$bintang4 += count($c['nilai']);
									} else if ($c['nilai'] == 5) {
										$bintang5 += count($c['nilai']);
									} else {
									}
								}
								?>
								<div class="col-6">
									<h4>Rating</h4>
									<div class="pull-left">
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<?php
										$star1 = round($bintang1 / $banyak * 100);
										$star2 = round($bintang2 / $banyak * 100);
										$star3 = round($bintang3 / $banyak * 100);
										$star4 = round($bintang4 / $banyak * 100);
										$star5 = round($bintang5 / $banyak * 100);
										?>
										<div class="pull-left progress-rating">
											<div class="progress progress-rating-2">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: <?= $star5; ?>%">
												</div>
											</div>
										</div>
										<div class="pull-right">
										</div>
									</div>
									<div class="pull-left">
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<div class="pull-left progress-rating">
											<div class="progress progress-rating-2">
												<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: <?= $star4; ?>%">
												</div>
											</div>
										</div>
										<div class="pull-right">
										</div>
									</div>
									<div class="pull-left">
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<div class="pull-left progress-rating">
											<div class="progress progress-rating-2">
												<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: <?= $star3; ?>%">
												</div>
											</div>
										</div>
										<div class="pull-right">
										</div>
									</div>
									<div class="pull-left">
										<span class="material-icons text-warning md-18 vm">star</span>
										<span class="material-icons text-warning md-18 vm">star</span>
										<div class="pull-left progress-rating">
											<div class="progress progress-rating-2">
												<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: <?= $star2; ?>%">
												</div>
											</div>
										</div>
										<div class="pull-right">
										</div>
									</div>
									<div class="pull-left">
										<span class="material-icons text-warning md-18 vm">star</span>
										<div class="pull-left progress-rating">
											<div class="progress progress-rating-2">
												<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: <?= $star1; ?>%">
												</div>
											</div>
										</div>
										<div class="pull-right">
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<?php include 'produks_terkait.php'; ?>
			</div>
		</div>
		<div class="footer-detail d-block d-sm-none">
			<div class="no-gutters">
				<div class="col-auto text-center">
					<div class="row no-gutters justify-content-center">
						<div class="col-2">
							<a href="https://wa.me/<?= $this->func->getRandomWasap() ?>/?text=Halo,%20saya%20ingin%20membeli%20produk%20*<?= $data->nama ?>*%20apakah%20masih%20tersedia?" title="tanya <?= $data->nama ?>" target="_blank"><button type="button" class="mb-2 tombol-footer-detail-chat btn-sm btn-outline-primary rounded"> <i class="fa fa-2x fa-whatsapp" aria-hidden="true"></i></button>
							</a>

						</div>

						<?php if ($this->func->cekLogin() == true) { ?>

							<div class="col-10">
								<button data-toggle="modal" data-target="#cart" class="mb-2 btn tombol-footer-detail btn-sm btn-default"><i class="material-icons">shopping_cart</i>+ keranjang</button>
							</div>
						<?php } else { ?>
							<div class="col-10">
								<a href="<?= site_url("home/signin"); ?>"><button class="mb-2 btn tombol-footer-detail btn-sm btn-default"><i class="material-icons">shopping_cart</i>+ keranjang</button></a>
							</div>

						<?php } ?>


					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalMoreCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h5 class="modal-title" id="exampleModalCenterTitle">Semua Kategori</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="main-category">
						<?php
						$this->db->where("parent", 0);
						$db = $this->db->get("kategori");
						foreach ($db->result() as $r) {
						?>

							<a href="<?= site_url("kategori/" . $r->url) ?>">
								<div class="item">
									<img src="<?= base_url("admin/kategori/" . $r->icon) ?>" alt="<?= $r->nama ?>">
									<p><?= $r->nama ?></p>
								</div>
							</a>

						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h5 class="modal-title" id="exampleModalCenterTitle">Cari Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="<?= site_url("shop?cari=" . $cari) ?>" method="get" class="search-header">
						<div class="input-group mb-3">
							<input class="form-control" type="text" value="<?= $cari ?>" name="cari" placeholder="Cari Produk">
							<div class="input-group-append">
								<button class="btn btn-outline-secondary" type="submit"><i class="material-icons md-18">search</i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content ">
				<div class="modal-header theme-header border-0">
					<h6 class="">Setting Warna</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body pt-0">
					<div class="text-center theme-color">
						<button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
						<button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
						<button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
						<button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
						<button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
						<button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-12 text-center w-100">
						<div class="row justify-content-center">
							<div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
							<div class="col-auto text-center align-self-center px-0">
								<div class="custom-control custom-switch float-right">
									<input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
									<label class="custom-control-label" for="theme-dark"></label>
								</div>
							</div>
							<div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- color chooser menu ends -->
	<!-- Modal -->

	<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-end" role="document">
			<div class="modal-content text-center">
				<input type="text" class="url_public_id" id="myInput" value="<?= site_url('produk/' . $data->url); ?>" readonly></input>
				<div class="modal-body">
					<h6 class="subtitle mt-0">Bagikan</h6>
					<span id="myTooltip"></span>

					<div class="row">
						<div class="col-12">
							<div class="text-center">
								<a href="https://www.facebook.com/sharer.php?u=<?= site_url('produk/' . $data->url); ?>" target="_blank" title="Facebook" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-facebook-f"></i></a>
								<a href="https://twitter.com/share?url=<?= site_url('produk/' . $data->url); ?>" target="_blank" title="Twitter" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-twitter"></i></a>
								<a href="whatsapp://send?text=<?= site_url('produk/' . $data->url); ?>" target="_blank" title="Whatsapp" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fab fa-whatsapp"></i></a>
								<button type="button" onclick="copyToClipboard()" title="copy link" class="btn btn-default button-rounded-36 shadow-sm"><i class=" fa fa-copy"></i></button>
								<style>
									.url_public_id {
										border: #fff;
										color: #fff;
										margin-top: 18px;
										margin-left: 31px;
										position: absolute;
										z-index: -1;
									}
								</style>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<h5 class="subtitle mt-0 text-center" id="exampleModalCenterTitle">Tambah Ke Troli</h5>
					<div class="modal-body">

						<div class="row">
							<div class="col-2 pl-0">
								<figure class="product-image h-auto"><img src="<?= base_url("admin/uploads/" . $res->nama); ?>" alt="" class="vm"></figure>
							</div>
							<div class="col px-0">
								<span class="text-dark mb-1 h6 d-block"><?= $data->nama ?></span>
								
								<div class="oldPrice">	
                					<?php echo "Rp. " . $this->func->formUang($data->hargaagen); ?>
                				</div>	

								<h5>
									<span id="hargacetak" class="text-success font-weight-normal mb-0">
										<?php
										if ($hargs > 0) {
											echo "Rp " . $this->func->formUang(min($harga));
										}
										?>
									</span>
								</h5>
							</div>
							<div class="text-center">
								<div class="btn btn-sm btn-warning shadow btn-rounded mt-3 mb-3" id="stokrefresh">STOK: <?= $totalstok; ?> </div>
							</div>

						</div>

						<form id="keranjang">
							<input type="hidden" name="idproduk" value="<?= $data->id; ?>" />
							<input type="hidden" id="variasi" name="variasi" value="0" />
							<input type="hidden" id="harga" name="harga" value="<?= $result ?>" />
							<?php
							if ($dbv->num_rows() > 0) {
								$warnaid = array();
								$sizeid = array();
								foreach ($dbv->result() as $var) {
									$this->db->where("variasi", $var->id);
									$dbf = $this->db->get("preorder");
									$totalpre = 0;
									foreach ($dbf->result() as $rf) {
										$totalpre += $rf->jumlah;
									}

									//$warna[] = $this->func->getWarna($var->warna,"nama");
									$warnaid[] = $var->warna;
									$variasi[$var->warna][] = $var->id;
									$sizeid[$var->warna][] = $var->size;
									$har[$var->warna][] = $var->harga;
									$harreseller[$var->warna][] = $var->hargareseller;
									$haragen[$var->warna][] = $var->hargaagen;
									$haragensp[$var->warna][] = $var->hargaagensp;
									$hardistri[$var->warna][] = $var->hargadistri;
									if (isset($stoks[$var->warna])) {
										$stoks[$var->warna] += ($data->preorder == 0) ? $var->stok : $var->stok - $totalpre;
									} else {
										$stoks[$var->warna] = ($data->preorder == 0) ? $var->stok : $var->stok - $totalpre;
									}
									$stok[$var->warna][] = ($data->preorder == 0) ? $var->stok : $var->stok - $totalpre;
									//$size[$var->warna][] = $this->func->getSize($var->size,"nama");
								}
								$warnaid = array_unique($warnaid);
								$warnaid = array_values($warnaid);
								//$sizeid = array_unique($sizeid);
								//$sizeid = array_values($sizeid);
							?>

								<div class="container">
									<div class="row">
										<div class="col-12">
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<label class="input-group-text" for="gender3"> <?= $data->variasi ?> </label>
												</div>
												<select class="custom-select" id="warna" required>
													<option value=""> Pilih <?= $data->variasi ?> </option>
													<?php
													for ($i = 0; $i < count($warnaid); $i++) {
														if ($stoks[$warnaid[$i]] > 0) {
															echo "<option value='" . $warnaid[$i] . "'>" . $this->func->getWarna($warnaid[$i], "nama") . "</option>";
														}
													}
													?>
												</select>
												<div class="input-group-append">
													<label class="input-group-text" for="gender3"></label>
												</div>

											</div>
										</div>
									</div>
								</div>
								<div class="container">
									<div class="row">
										<div class="col-12">
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<label class="input-group-text"> <?= $data->subvariasi ?></label>
												</div>
												<select class="custom-select" id="size" required>
													<option value=""> Pilih <?= $data->variasi ?> dulu </option>
												</select>
												<div class="input-group-append">
													<label class="input-group-text"></label>
												</div>

											</div>
										</div>
									</div>
								</div>


							<?php
							}
							?>
							<div class="container">
								<div class="row">
									<div class="col-4 text-center">
										<span class="text-dark mb-1 h6 d-block">Jumlah: </span>
									</div>
									<div class="col-6">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button type="button" class="btn btn-light-grey px-1 btn-number" data-type="minus" data-field="jumlah"><i class="material-icons">remove</i></button>
											</div>
											<input class="w-45 num-product" type="text" min="<?= $data->minorder; ?>" name="jumlah" value="<?= $data->minorder; ?>" id="jumlahorder" max="<?= $totalstok; ?>" required>

											<div class="input-group-append">
												<button type="button" class="btn btn-light-grey px-1 btn-number" data-type="plus" data-field="jumlah"><i class="material-icons">add</i></button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12">
								<div class="form-group mb-0 float-label">
									<input type="text" class="form-control" name="keterangan" value="">
									<label class="form-control-label">Keterangan</label>
								</div>
							</div>


					</div>

					<div class="text-center">
						<button type="submit" id="submit" class="btn btn-lg btn-default shadow btn-rounded mt-3"><i class="material-icons mb-18 mr-2">shopping_cart</i><?= $textorder ?></button>

						<!--<a href="https://wa.me/<?= $this->func->getRandomWasap() ?>/?text=Halo,%20saya%20ingin%20membeli%20produk%20*<?= $data->nama ?>*%20apakah%20masih%20tersedia?" class="flex-c-m size1 btn-success bo-rad-23 hov1 s-text1 trans-0-4 m-t-10">
											<i class="fa fa-whatsapp"></i> &nbsp;Beli via Whatsapp
										</a>-->
						<span id="proses" class="cl1" style="display:none;"><b><i class="fa fa-spin fa-spinner"></i> Memproses pesanan</b></span>
						<span id="gagal" class="cl1 m-t-20" style="display:none;"><i class="fa fa-exclamation-triangle"></i> Gagal memproses pesanan.</span>
					</div>
					</form>



				</div>

			</div>
		</div>
	</div>



	<!-- jquery, popper and bootstrap js -->
	<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- cookie js -->
	<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

	<!-- swiper js -->
	<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

	<!-- nouislider js -->
	<script src="<?= base_url('assets/js/nouislider.min.js') ?>"></script>

	<!-- chosen multiselect js -->
	<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>

	<!-- template custom js -->
	<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/js/main.js') ?>"></script>
	<!-- page level script -->
	<script>
		/* chosen select*/
		$(".chosen").chosen();

		function plusProduct(price, stock) {
			let inputJml;
			inputJml = parseInt($("input.valueJml").val());
			inputJml = inputJml + 1;
			if (inputJml <= stock) {
				$("input.valueJml").val(inputJml);
				const newPrice = inputJml * price;
				const rpFormat = number_format(newPrice);
				$("#detailTotalPrice").text(rpFormat.split(",").join("."));
			}
		}

		function minusProduct(price) {
			let inputJml;
			inputJml = parseInt($("input.valueJml").val());
			inputJml = inputJml - 1;
			if (inputJml >= 1) {
				$("input.valueJml").val(inputJml);
				const newPrice = inputJml * price;
				const rpFormat = number_format(newPrice);
				$("#detailTotalPrice").text(rpFormat.split(",").join("."));
			}
		}

		$(function() {
			$("#keranjang").on("submit", function(e) {
				e.preventDefault();
				$("#submit").hide();
				$("#proses").show();
				<?php if ($data->preorder == 0) { ?>
					$.post("<?= site_url("assync/prosesbeli"); ?>", $(this).serialize(), function(msg) {
						var data = eval("(" + msg + ")");
						$("#proses").hide();
						$("#submit").show();
						if (data.success == true) {
							fbq('track', 'AddToCart', {
								content_ids: "<?= $data->id ?>",
								content_type: "<?= $kategorinama ?>",
								content_name: "<?= $data->nama ?>",
								currency: "IDR",
								value: data.total
							});
							var nameProduct = $('#js-name-detail').html();
							swal(nameProduct, "berhasil ditambahkan ke keranjang", "success").then((value) => {
								window.location.href = "<?= site_url("home/keranjang"); ?>";
							});
						} else {
							swal("Gagal", "tidak dapat memproses pesanan \n " + data.msg, "error");
						}
					});
				<?php } else { ?>
					$.post("<?= site_url("assync/prosespreorder"); ?>", $(this).serialize(), function(msg) {
						var data = eval("(" + msg + ")");
						$("#proses").hide();
						$("#submit").show();
						if (data.success == true) {
							fbq('track', 'AddToCart', {
								content_ids: "<?= $data->id ?>",
								content_type: "<?= $kategori->nama ?>",
								content_name: "<?= $data->nama ?>",
								currency: "IDR",
								value: data.total
							});
							var nameProduct = $('#js-name-detail').html();
							swal(nameProduct, "berhasil mengikuti preorder", "success").then((value) => {
								window.location.href = "<?= site_url("home/invoicepreorder"); ?>?inv=" + data.inv;
							});
						} else {
							swal("Gagal", "tidak dapat memproses pesanan", "error");
						}
					});
				<?php } ?>
			});

			$("#jumlahorder").change(function() {
				if (parseInt($(this).val()) < parseInt($(this).attr("min"))) {
					$(this).val($(this).attr("min")).trigger("change");
				}

				if (parseInt($(this).val()) > parseInt($(this).attr("max"))) {
					$(this).val($(this).attr("max")).trigger("change");
				}
			});

			$("#tambahdiskusi").on("submit", function(e) {
				e.preventDefault();
				$("textarea", this).prop("readonly", true);
				$.post("<?= site_url("assync/tambahdiskusi"); ?>", $(this).serialize(), function(msg) {
					var data = eval("(" + msg + ")");
					if (data.success == true) {
						swal("Berhasil!", "pertanyaan Anda sudah disimpan", "success").then((value) => {
							location.reload();
						});
					} else {

					}
				});
			});

			$("#warna").on("change", function() {
				if ($(this).val() != "") {
					$("#size").html($("#warna_" + $(this).val()).html());
				} else {
					$("#size").html("<option value=\"\"> Pilih <?= $data->variasi ?> dulu </option>");
				}
				$("#stokrefresh").html("");
			});
			$("#size").on("change", function() {
				$("#variasi").val($(this).find(":selected").data('variasi'));
				$("#jumlahorder").attr("max", $(this).find(":selected").data('stok'));
				$("#stokmaks").html($(this).find(":selected").data('stok'));
				$("#harga").val($(this).find(":selected").data('harga'));
				$("#hargacetak").html("Rp " + formUang($(this).find(":selected").data('harga')));
				$("#stokrefresh").html("stok: " + $(this).find(":selected").data('stok'));
			});
		});

		$.fn.stars = function() {
			return $(this).each(function() {

				var rating = $(this).data("rating");

				var numStars = $(this).data("numStars");

				var fullStar = new Array(Math.floor(rating + 1)).join('<i class="material-icons text-warning md-18 vm">star</i>');

				var halfStar = ((rating % 1) !== 0) ? '<i class="material-icons text-warning md-18 vm">star_half</i>' : '';

				var noStar = new Array(Math.floor(numStars + 1 - rating)).join('<i class="material-icons text-secondary md-18 vm">star_border</i>');

				$(this).html(fullStar + halfStar + noStar);

			});
		}

		$('.stars').stars();
	</script>

	<div style="display:none;">
		<?php
		for ($i = 0; $i < count($warnaid); $i++) {
			echo "
				<div id='warna_" . $warnaid[$i] . "'>
					<option value=''> Pilih " . $data->subvariasi . " </option>
			";
			for ($a = 0; $a < count($sizeid[$warnaid[$i]]); $a++) {
				if ($stok[$warnaid[$i]][$a] > 0) {
					if ($level == 5) {
						$result = $hardistri[$warnaid[$i]][$a];
					} elseif ($level == 4) {
						$result = $haragensp[$warnaid[$i]][$a];
					} elseif ($level == 3) {
						$result = $haragen[$warnaid[$i]][$a];
					} elseif ($level == 2) {
						$result = $harreseller[$warnaid[$i]][$a];
					} else {
						$result = $har[$warnaid[$i]][$a];
					}
					echo "<option value='" . $sizeid[$warnaid[$i]][$a] . "' data-stok='" . $stok[$warnaid[$i]][$a] . "' data-harga='" . $result . "' data-variasi='" . $variasi[$warnaid[$i]][$a] . "'>" . $this->func->getSize($sizeid[$warnaid[$i]][$a], "nama") . "</option>";
				}
			}
			echo "
				</div>
			";
		}
		?>
	</div>
	<script>
		function copyToClipboard() {
			var copyText = document.getElementById("myInput");
			copyText.select();
			copyText.setSelectionRange(0, 99999); /*For mobile devices*/
			document.execCommand("copy");
			var tooltip = document.getElementById("myTooltip");
			tooltip.innerHTML = "link berhasil di Copy ";

		}

		$(window).on('load', function() {
			var swiper = new Swiper('.swiper-container', {
				zoom: true,
				pagination: {
					el: '.swiper-pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
			/* counter count down script */
			var countDownDate = new Date("2020-10-10 21:12:00").getTime();

			// Update the count down every 1 second
			var x = setInterval(function() {

				// Get today's date and time
				var now = new Date().getTime();

				// Find the distance between now and the count down date
				var distance = countDownDate - now;

				// Time calculations for days, hours, minutes and seconds
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				// Display the result in the element with id="demo"
				document.getElementById("days").innerHTML = days;
				document.getElementById("hrs").innerHTML = hours
				document.getElementById("min").innerHTML = minutes
				document.getElementById("sec").innerHTML = seconds

				// If the count down is finished, write some text
				if (distance < 0) {
					clearInterval(x);
					document.getElementById("timer").innerHTML = "EXPIRED";
				}
			}, 1000);

		});

		function copyText() {
			var copyText = document.getElementById("myInput");
			copyText.select();
			copyText.setSelectionRange(0, 99999)
			document.execCommand("copy");
			alert("Copied the text: " + copyText.value);
		}



		$('.btn-number').click(function(e) {
			e.preventDefault();

			fieldName = $(this).attr('data-field');
			type = $(this).attr('data-type');
			var input = $("input[name='" + fieldName + "']");
			var currentVal = parseInt(input.val());
			if (!isNaN(currentVal)) {
				if (type == 'minus') {

					if (currentVal > input.attr('min')) {
						input.val(currentVal - 1).change();
					}
					if (parseInt(input.val()) == input.attr('min')) {
						$(this).attr('disabled', true);
					}

				} else if (type == 'plus') {

					if (currentVal < input.attr('max')) {
						input.val(currentVal + 1).change();
					}
					if (parseInt(input.val()) == input.attr('max')) {
						$(this).attr('disabled', true);
					}

				}
			} else {
				input.val(0);
			}
		});
		$('.input-number').focusin(function() {
			$(this).data('oldValue', $(this).val());
		});
		$('.input-number').change(function() {

			minValue = parseInt($(this).attr('min'));
			maxValue = parseInt($(this).attr('max'));
			valueCurrent = parseInt($(this).val());

			name = $(this).attr('name');
			if (valueCurrent >= minValue) {
				$(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
			} else {
				alert('Maaf, kurang dari minimum');
				$(this).val($(this).data('oldValue'));
			}
			if (valueCurrent <= maxValue) {
				$(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
			} else {
				alert('Maaf, lebih dari maksimum');
				$(this).val($(this).data('oldValue'));
			}


		});
		$(".input-number").keydown(function(e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
				// Allow: Ctrl+A
				(e.keyCode == 65 && e.ctrlKey === true) ||
				// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
	</script><!-- color chooser menu start -->
	<div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content ">
				<div class="modal-header theme-header border-0">
					<h6 class="">Setting Warna</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body pt-0">
					<div class="text-center theme-color">
						<button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
						<button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
						<button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
						<button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
						<button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
						<button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-12 text-center w-100">
						<div class="row justify-content-center">
							<div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
							<div class="col-auto text-center align-self-center px-0">
								<div class="custom-control custom-switch float-right">
									<input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
									<label class="custom-control-label" for="theme-dark"></label>
								</div>
							</div>
							<div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- color chooser menu ends -->

</body>

</html>