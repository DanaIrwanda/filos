<?php
$page = (isset($_GET["page"]) and $_GET["page"] != "") ? $_GET["page"] : 1;
$orderby = (isset($_GET["orderby"]) and $_GET["orderby"] != "") ? $_GET["orderby"] : "id DESC";
$cari = (isset($_GET["cari"]) and $_GET["cari"] != "") ? $_GET["cari"] : "";
$perpage = 31;
?>

<body>
	<?php include 'loader.php'; ?>
	<div class="sidebar">
		<?php include 'menutukunen.php'; ?>
	</div>
	<div class="wrapper">
		<?php include 'menuheadtukunen.php'; ?>

		<div class="container">
			<form action="" method="get" class="search-header">
				<div class="input-group mb-3">
					<input class="form-control" type="text" value="<?= $cari ?>" name="cari" placeholder="Cari Produk">
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="submit"><i class="material-icons md-18">search</i></button>
					</div>
				</div>
			</form>

			<div class="row">


				<?php
				$idcat = $this->func->getKategori($url, "id", "url");

				$this->db->select("SUM(stok),idproduk");
				$this->db->group_by("idproduk");
				$dbvar = $this->db->get("produkvariasi");
				$notin = array();
				foreach ($dbvar->result() as $not) {
					$notin[] = $not->idproduk;
				}

				$where = "(harga LIKE '%$cari%' OR hargareseller LIKE '%$cari%' OR nama LIKE '%$cari%' OR deskripsi LIKE '%$cari%') AND status = 1 AND idcat = " . $idcat;
				$this->db->where($where);
				if (count($notin) > 0) {
					$this->db->where_not_in($notin);
				}
				$dbs = $this->db->get("produk");

				$this->db->where($where);
				if (count($notin) > 0) {
					$this->db->where_not_in($notin);
				}
				$this->db->limit($perpage, ($page - 1) * $perpage);
				$this->db->order_by($orderby);
				$db = $this->db->get("produk");
				$totalproduk = 0;

				foreach ($db->result() as $r) {
					$level = isset($_SESSION["lvl"]) ? $_SESSION["lvl"] : 0;
					if ($level == 5) {
						$result = $r->hargadistri;
					} elseif ($level == 4) {
						$result = $r->hargaagensp;
					} elseif ($level == 3) {
						$result = $r->hargaagen;
					} elseif ($level == 2) {
						$result = $r->hargareseller;
					} else {
						$result = $r->harga;
					}
					$ulasan = $this->func->getReviewProduk($r->id);

					$this->db->where("idproduk", $r->id);
					$dbv = $this->db->get("produkvariasi");
					$totalstok = ($dbv->num_rows() > 0) ? 0 : $r->stok;
					$hargs = 0;
					$harga = array();
					foreach ($dbv->result() as $rv) {
						$totalstok += $rv->stok;
						if ($level == 5) {
							$harga[] = $rv->hargadistri;
						} elseif ($level == 4) {
							$harga[] = $rv->hargaagensp;
						} elseif ($level == 3) {
							$harga[] = $rv->hargaagen;
						} elseif ($level == 2) {
							$harga[] = $rv->hargareseller;
						} else {
							$harga[] = $rv->harga;
						}
						$hargs += $rv->harga;
					}

					if ($totalstok > 0) {
						$totalproduk += 1;
				?>

						<div class="col-6 col-md-4 col-lg-3 col-xl-2">
							<div class="card shadow-sm border-0 mb-4">
								<div class="card-body">
									<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" class="add_wishlist btn btn-sm btn-link p-0"><i class="material-icons md-18">favorite_outline</i></button>-->

									<span class="badge badge-warning pl-1 pr-1"><?= $ulasan['nilai'] ?></span>
									<i class="stars" data-rating="<?= $ulasan['nilai'] ?>" data-num-stars="5"></i>

									<figure class="product-image"><a href="<?php echo site_url('produk/' . $r->url); ?>"><img src="<?= $this->func->getFoto($r->id, "utama") ?>" alt="<?= $r->nama ?>" class=""></a></figure>
									<a href="<?php echo site_url('produk/' . $r->url); ?>" class="text-dark mb-1 mt-2 h6 tulisan-card"><?= $r->nama ?></a>
									<h5 class="text-success font-weight-normal mb-0">

										
									<div class="oldPrice">	
										<?php echo "Rp. " . $this->func->formUang($r->hargaagen); ?>
									</div>	

										<div class="newPrice">
											<?php
											if ($hargs > 0) {
												echo "Rp. " . $this->func->formUang(min($harga));
											} else {
												echo "Rp. " . $this->func->formUang($result);
											}
											?>
										</div>
									</h5>

									<?php if ($hargs > 0): ?>
										
										<div class="product_marks" style="background: red;">
										<?php 
											$amount 	= $this->func->formUang($r->hargaagen) - $this->func->formUang($r->harga);
											$discount 	= $amount/$this->func->formUang($r->hargaagen)*100;

											echo "<b style='color:white;'>" . " " . intval($discount). "%" . "</b>";       
										?>
									</div>

										
									<?php else: ?>
									
									<div class="product_marks" style="background: blue;">
											<?php echo "<b style='color:white;'>" . "New" . "</b>"; ?>
										</div>

									<?php endif ?>
									<!--<button class="btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>
                                        -->
									<p class="text-secondary small text-mute mb-0"><?= $ulasan['ulasan'] ?> Ulasan</p>

									<?php if ($totalstok > 0) : ?>
										<span class="badge badge-info pl-1 pr-1">Tersedia</span>
									<?php else : ?>
										<span class="badge badge-dark pl-1 pr-1">Habis</span>
									<?php endif ?>
									<!--<button data-id_product="10" data-product_name="Asus Zenfone MAX M1 ZB555KL Smartphone [32GB/ 3GB/ L]" data-price="1260000" data-img="1586960622849.jpg" data-slug="asus-zenfone-max-m1-zb555kl-smartphone-32gb-3gb-l" data-weight="430" data-qty="1" data-ket="" class="add_cart btn btn-default button-rounded-36 shadow-sm float-bottom-right"><i class="material-icons md-18">shopping_cart</i></button>-->
								</div>
							</div>
						</div>
				<?php
					}
				}

				if ($totalproduk == 0) {
					echo "<div class='col-12 text-center m-tb-40'><button class='btn btn-lg btn-default shadow btn-rounded'>Produk Kosong</button></div>";
				}
				?>

			</div>
		</div>
		<div class="pagination justify-content-center">
			<?php
			if ($totalproduk > 0) {
				echo $this->func->createPagination($dbs->num_rows(), $page, $perpage);
			}
			?>
		</div>
		<?php include 'footer_alamat.php'; ?>

	</div>

	<?php include 'footermobiletukunen.php'; ?>

	<div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content ">
				<div class="modal-header theme-header border-0">
					<h6 class="">Setting Warna</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body pt-0">
					<div class="text-center theme-color">
						<button class="m-1 btn pink-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="pink-theme"></button>
						<button class="m-1 btn brown-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="brown-theme"></button>
						<button class="m-1 btn blue-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="blue-theme"></button>
						<button class="m-1 btn purple-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="purple-theme"></button>
						<button class="m-1 btn green-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="green-theme"></button>
						<button class="m-1 btn grey-theme-bg text-white btn-rounded-54 shadow-sm" data-theme="grey-theme"></button>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-12 text-center w-100">
						<div class="row justify-content-center">
							<div class="col-auto text-right align-self-center"><i class="material-icons text-warning md-36 vm">wb_sunny</i></div>
							<div class="col-auto text-center align-self-center px-0">
								<div class="custom-control custom-switch float-right">
									<input type="checkbox" name="themelayout" class="custom-control-input" id="theme-dark">
									<label class="custom-control-label" for="theme-dark"></label>
								</div>
							</div>
							<div class="col-auto text-left align-self-center"><i class="material-icons text-dark md-36 vm">brightness_2</i></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- color chooser menu ends -->
	<!-- jquery, popper and bootstrap js -->
	<script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- cookie js -->
	<script src="<?= base_url('assets/js/jquery.cookie.js') ?>"></script>

	<!-- swiper js -->
	<script src="<?= base_url('assets/js/swiper.min.js') ?>"></script>

	<!-- template custom js -->
	<script src="https://kit.fontawesome.com/2baad1d54e.js" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/js/main.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.countdown.min.js') ?>"></script>

	<script type="text/javascript">
		function refreshTabel(page) {
			window.location.href = "<?= site_url('kategori/' . $url) ?>?page=" + page;
		}
	</script>
	<script>
		$.fn.stars = function() {
			return $(this).each(function() {

				var rating = $(this).data("rating");

				var numStars = $(this).data("numStars");

				var fullStar = new Array(Math.floor(rating + 1)).join('<i class="material-icons text-warning md-18 vm">star</i>');

				var halfStar = ((rating % 1) !== 0) ? '<i class="material-icons text-warning md-18 vm">star_half</i>' : '';

				var noStar = new Array(Math.floor(numStars + 1 - rating)).join('<i class="material-icons text-secondary md-18 vm">star_border</i>');

				$(this).html(fullStar + halfStar + noStar);

			});
		}

		$('.stars').stars();
	</script><!-- color chooser menu start -->