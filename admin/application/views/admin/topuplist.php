<div class="table-responsive">
	<table class="table table-condensed table-hover">
		<tr>
			<th scope="col">No</th>
			<th scope="col">Nama</th>
			<th scope="col">User Id</th>
			<th scope="col">No HP</th>
			<th scope="col">Total Saldo</th>
			<th scope="col">Aksi</th>
		</tr>
		<?php
		$page = (isset($_GET["page"]) and $_GET["page"] != "") ? $_GET["page"] : 1;
		$cari = (isset($_POST["cari"]) and $_POST["cari"] != "") ? $_POST["cari"] : "";
		$orderby = (isset($data["orderby"]) and $data["orderby"] != "") ? $data["orderby"] : "id";
		$perpage = 10;

		$where = "(username LIKE '%$cari%' OR nama LIKE '%$cari%' OR tgl LIKE '%$cari%')";
		if ($_GET["load"] == "distri") {
			$where .= " AND level = 5";
			$fungsi = "loadDistri";
		} elseif ($_GET["load"] == "reseller") {
			$where .= " AND level = 2";
			$fungsi = "loadReseller";
		} elseif ($_GET["load"] == "agen") {
			$where .= " AND level = 3";
			$fungsi = "loadAgen";
		} elseif ($_GET["load"] == "agensp") {
			$where .= " AND level = 4";
			$fungsi = "loadAgenSP";
		} else {
			$where .= " AND level = 1";
			$fungsi = "loadUser";
		}
		$this->db->select("id");
		$this->db->where($where);
		$rows = $this->db->get("userdata");
		$rows = $rows->num_rows();

		$this->db->where($where);
		$this->db->order_by("id", "DESC");
		$this->db->limit($perpage, ($page - 1) * $perpage);
		$db = $this->db->get("userdata");

		if ($rows > 0) {
			$no = 1;
			$total = 0;
			foreach ($db->result() as $r) {
				$this->db->where("usrid", $r->id);
				$dbs = $this->db->get("saldo");
				foreach ($dbs->result() as $res) {
					$total = $res->saldo;
				}
		?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $r->nama ?></td>
					<td><?= $r->id ?></td>
					<td><?= $r->nohp ?></td>
					<td>Rp. <?= $this->func->formUang($total) ?></td>
					<td>

						<a href='javascript:void(0)' onclick='editSaldo(<?= $r->id ?>, <?= $total ?>)' class='btn btn-primary btn-xs'><i class="fas fa-plus"></i> tambah</a>
						<!--<button type="button" onclick="addSaldo(<?= $r->id ?>)" class="btn btn-xs btn-success"><i class="fas fa-plus"></i> update</button>-->
					</td>
				</tr>
		<?php
				$no++;
			}
		} else {
			echo "<tr><td colspan=5 class='text-center text-danger'>Belum ada " . ucwords($_GET["load"]) . "</td></tr>";
		}
		?>
	</table>

	<?= $this->func->createPagination($rows, $page, $perpage, $fungsi); ?>
</div>