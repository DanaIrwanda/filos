<div class="content-wrapper">

	<section class="content">
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">BLog</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>


		<div class="m-b-60">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-6">
							<h4 class="page-title">Postingan Blog</h4>
						</div>
						<div class="col-6">
							<a class="float-right btn-sm btn btn-primary" href="<?= site_url("tukunen/editblog") ?>"><i class="fas fa-plus-circle"></i> Postingan Baru</a>
						</div>
					</div>
				</div>

				<div class="card-body" id="load">
					<i class="fas fa-spin fa-spinner"></i> Loading data...
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(function() {
		loadBlog(1);

		$(".tabs-item").on('click', function() {
			$(".tabs-item.active").removeClass("active");
			$(this).addClass("active");
		});

		$("#rekeningform").on("submit", function(e) {
			e.preventDefault();
			swal.fire({
				text: "pastikan lagi data yang anda masukkan sudah sesuai",
				title: "Validasi data",
				type: "warning",
				showCancelButton: true,
				cancelButtonText: "Cek Lagi"
			}).then((vals) => {
				if (vals.value) {
					$.post("<?= site_url("api/updateblog") ?>", $("#rekeningform").serialize(), function(msg) {
						var data = eval("(" + msg + ")");
						if (data.success == true) {
							loadBlog(1);
							$("#modal").modal("hide");
							swal.fire("Berhasil", "data halaman sudah disimpan", "success");
						} else {
							swal.fire("Gagal!", "gagal menyimpan data, coba ulangi beberapa saat lagi", "error");
						}
					});
				}
			});
		});
	});

	function loadBlog(page) {
		$("#load").html('<i class="fas fa-spin fa-spinner"></i> Loading data...');
		$.post("<?= site_url("tukunen/blog?load=hal&page=") ?>" + page, {
			"cari": $("#cari").val()
		}, function(msg) {
			$("#load").html(msg);
		});
	}

	function hapus(pro) {
		swal.fire({
			title: "Anda yakin menghapus?",
			text: "postingan yang sudah dihapus tidak dapat dikembalikan",
			type: "error",
			showCancelButton: true,
			cancelButtonColor: '#d33',
			cancelButtonText: "Batal",
			confirmButtonText: "Tetap Hapus"
		}).then((result) => {
			if (result.value) {
				$.post("<?php echo site_url('api/hapusblog'); ?>", {
					"id": pro
				}, function(msg) {
					var data = eval("(" + msg + ")");
					if (data.success == true) {
						swal.fire("Berhasil!", "Berhasil menghapus data", "success").then((data) => {
							location.reload();
						});
					} else {
						swal.fire("Gagal!", "Gagal menghapus data, terjadi kesalahan sistem", "error");
					}
				});
			}
		});
	}
</script>