<!DOCTYPE html>
<html>

<head>
	<?php $set = $this->func->globalset("semua"); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title><?= $set->nama ?> Dashboard Management</title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="<?= base_url("assets/img/" . $this->func->globalset("favicon")) ?>" />

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/summernote/summernote-bs4.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/sweetalert2.min.css"); ?>" />

	<!-- IMPORTANT JS -->
	<script src="<?= base_url() ?>assets/js/core/jquery-3.2.1.min.js"></script>
	<?php if (isset($tiny) and $tiny == true) { ?>
		<script src="<?= base_url() ?>assets/plugandin/tinymce/tinymce.js"></script>
		<!--<script src="https://cdn.tiny.cloud/1/pa3llg12ezvnxollin25u4ddg7d95nj77s2o7hvjhh1tkgir/tinymce/5/tinymce.min.js"></script>-->
	<?php } ?>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
				</li>
			</ul>

			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<!-- Notifications Dropdown Menu -->
				<li class="nav-item dropdown">
					<a class="nav-link" data-toggle="dropdown" href="#">
						<i class="far fa-user"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
						<span class="dropdown-item dropdown-header">Menu Admin</span>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="javascript:$('#modalgantipass').modal();">
							<i class="fas fa-envelope mr-2"></i> Ganti Password
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="javascript:void(0)" onclick="logout()">
							<i class="fas fa-users mr-2"></i> logout
						</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
						<i class="fas fa-th-large"></i>
					</a>
				</li>
			</ul>
		</nav>
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Brand Logo -->
			<a href="#" class="brand-link">
				<img src="<?= base_url() ?>assets/img/user.png" alt="user-img" width="36" class="brand-image img-circle elevation-3">
				<span class="brand-text font-weight-light"><?= $this->func->getUser($_SESSION["usrid"], "nama") ?></span>
			</a>

			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
						<li class="nav-item">
							<a href="<?= site_url("tukunen") ?>" class="nav-link">
								<i class="nav-icon far fa-calendar-alt"></i>
								<p>
									Dashboard
								</p>
							</a>
						</li>
						<li class="nav-item has-treeview menu-open">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-copy"></i>
								<p>
									Data Produk
									<i class="fas fa-angle-left right"></i>
									<span class="badge badge-info right">3</span>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= site_url("tukunen/produk") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Produk</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/kategori") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Kategori</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/variasi") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Variasi</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item has-treeview menu-open">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-copy"></i>
								<p>
									Pesanan
									<i class="fas fa-angle-left right"></i>
									<span class="badge badge-info right">3</span>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= site_url("tukunen/pesanan") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Order

											<?php
											$order = $this->func->getJmlPesanan();
											if ($order > 0) {
											?>
												<span class="right badge badge-danger"><?= $order ?></span>
											<?php } ?>

										</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/preorder") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Preorder</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/pesan") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Pesan masuk
											<?php
											$order = $this->func->getJmlPesan();
											if ($order > 0) {
											?>
												<span class="right badge badge-danger"><?= $order ?></span>
											<?php } ?>
										</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item has-treeview">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-copy"></i>
								<p>
									Atur Promo
									<i class="fas fa-angle-left right"></i>
									<span class="badge badge-info right">1</span>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= site_url("tukunen/voucher") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Voucher</p>
									</a>
								</li>
							</ul>
						</li>

						<li class="nav-item has-treeview">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-copy"></i>
								<p>
									User
									<i class="fas fa-angle-left right"></i>
									<span class="badge badge-info right">2</span>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<?php if (isset($_SESSION["level"]) and $_SESSION['level'] == 2) { ?>

									<li class="nav-item">
										<a href="<?= site_url("tukunen/usermanager") ?>" class="nav-link">
											<i class="far fa-circle nav-icon"></i>
											<p>User</p>
										</a>
									</li>
								<?php } ?>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/saldomanager") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Saldo User</p>
									</a>
								</li>
								<?php if (isset($_SESSION["level"]) and $_SESSION['level'] == 2) { ?>

									<li class="nav-item">
										<a href="<?= site_url("tukunen/agen") ?>" class="nav-link">
											<i class="far fa-circle nav-icon"></i>
											<p>Agen & reseler</p>
										</a>
									</li>
								<?php } ?>

							</ul>
						</li>


						<li class="nav-item has-treeview">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-copy"></i>
								<p>
									Laporan <i class="fas fa-angle-left right"></i>
									<span class="badge badge-info right">2</span>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="<?= site_url("tukunen/laporantransaksi") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Penjualan</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/laporanuser") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Transaksi User</p>
									</a>
								</li>

							</ul>
						</li>

						<li class="nav-item has-treeview">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-copy"></i>
								<p>
									Pengaturan
									<i class="fas fa-angle-left right"></i>
									<span class="badge badge-info right">5</span>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<?php if (isset($_SESSION["level"]) and $_SESSION['level'] == 2) { ?>

									<li class="nav-item">
										<a href="<?= site_url("tukunen/pengaturan") ?>" class="nav-link">
											<i class="far fa-circle nav-icon"></i>
											<p>Umum</p>
										</a>
									</li>
								<?php } ?>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/slider") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Banner</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/booster") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Booster</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/blog") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Blog</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?= site_url("tukunen/halaman") ?>" class="nav-link">
										<i class="far fa-circle nav-icon"></i>
										<p>Page</p>
									</a>
								</li>
							</ul>
						</li>

					</ul>
				</nav>
				<br />
				<br />
				<br />
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>