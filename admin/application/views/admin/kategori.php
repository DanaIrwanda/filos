<div class="content-wrapper">

	<section class="content">
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Daftar Kategori</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>

		<div class="card">
			<div class="card-header">
				<div class="row">
					<div class="col-md-12 text-center">
						<a class="float-right btn btn-primary" href="<?= site_url("tukunen/kategoriform") ?>"><i class="fas fa-plus-circle"></i> Tambah Kategori</a>
					</div>
				</div>
			</div>

			<div class="card-body table-responsive">
				<table class="table mt-3">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Nama</th>
						<th scope="col">id Kategori</th>
						<th scope="col">Aksi</th>
					</tr>
					<?php
					$page = (isset($_GET["page"]) and $_GET["page"] != "") ? $_GET["page"] : 1;
					$orderby = (isset($data["orderby"]) and $data["orderby"] != "") ? $data["orderby"] : "id";
					$perpage = 10;

					$rows = $this->db->get("kategori");
					$rows = $rows->num_rows();

					$this->db->from('kategori');
					$this->db->order_by($orderby, "desc");
					$this->db->limit($perpage, ($page - 1) * $perpage);
					$pro = $this->db->get();

					if ($rows > 0) {
						$no = 1;
						foreach ($pro->result() as $r) {
					?>

							<tr>
								<td class="text-center"><img style="max-height:50px;" src="<?= base_url("kategori/" . $r->icon) ?>" /></td>
								<td>
									<?= $r->nama ?>
								</td>
								<td>
									<?= $r->id ?>
								</td>

								<td>
									<a href="<?php echo site_url("tukunen/kategoriform/" . $r->id); ?>" class="btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>
									<a href="javascript:hapusPromo(<?php echo $r->id; ?>)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
								</td>
							</tr>
					<?php
							$no++;
						}
					} else {
						echo "<tr><td colspan=3>Belum ada kategori</td></tr>";
					}
					?>
				</table>

				<?= $this->func->createPagination($rows, $page, $perpage); ?>

			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(function() {

	});

	function hapusPromo(pro) {
		swal.fire({
			title: "Anda yakin menghapus?",
			text: "kategori yang sudah dihapus tidak dapat dikembalikan",
			type: "error",
			showCancelButton: true,
			cancelButtonColor: '#d33',
			cancelButtonText: "Batal",
			confirmButtonText: "Tetap Hapus"
		}).then((result) => {
			if (result.value) {
				$.post("<?php echo site_url('tukunen/hapuskategori'); ?>", {
					"pro": pro
				}, function(msg) {
					var data = eval("(" + msg + ")");
					if (data.success == true) {
						swal.fire("Berhasil!", "Berhasil menghapus data", "success").then((data) => {
							location.reload();
						});
					} else {
						swal.fire("Gagal!", "Gagal menghapus data, terjadi kesalahan sistem", "error");
					}
				});
			}
		});
	}

	function refreshTabel(page) {
		window.location.href = "<?php echo site_url('tukunen/kategori/?page='); ?>" + page;
	}
</script>