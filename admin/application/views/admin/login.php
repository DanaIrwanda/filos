<!DOCTYPE html>
<html>

<head>
	<?php $set = $this->func->globalset("semua"); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= $set->nama ?> admin | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/summernote/summernote-bs4.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/sweetalert2.min.css"); ?>" />
</head>

<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo text-center">
			<img style="width:200px;" src="<?= base_url("assets/img/" . $this->func->globalset("logo")) ?>" class="logo" />
		</div>
		<!-- /.login-logo -->
		<div class="card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Silahkan login</p>

				<form id="login">
					<div class="form-group">
						<input placeholder="Username" type="text" class="form-control" id="username" name="username" required />
					</div>
					<div class="form-group">
						<input placeholder="password" type="password" class="form-control" id="pass" name="pass" required />
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success"><i class="la la-check"></i> Masuk</button>
					</div>
				</form>

			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="../../plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>

</body>

</html>

<div class="login-footer">
	Copyrights &copy; <?php echo date("Y"); ?> | <?= $this->func->globalset("nama") ?>
</div>
<!-- SCRIPT LOAD -->
<script type="text/javascript" src="<?php echo base_url("assets/js/core/jquery-3.2.1.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/core/bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/core/popper.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/sweetalert2.min.js"); ?>"></script>
<script type="text/javascript">
	$(function() {
		$("#login").on("submit", function(e) {
			e.preventDefault();
			var btn = $(".btn-success").html();
			$(".btn-success").html("<i class='la la-spin la-spinner'></i> Tunggu Sebentar...");
			$.post("<?php echo site_url("tukunen/auth"); ?>", $(this).serialize(), function(msg) {
				var dt = eval("(" + msg + ")");
				$(".btn-success").html(btn);
				if (dt.success == true) {
					swal.fire("Berhasil!", "selamat datang kembali " + dt.name, "success").then(function() {
						window.location.href = "<?= site_url("tukunen"); ?>";
					});
				} else {
					swal.fire("Gagal!", "gagal masuk, cek kembali username & password anda", "warning");
				}
			});
		});
	});
</script>

</html>