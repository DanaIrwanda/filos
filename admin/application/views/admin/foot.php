<footer class="main-footer">
	<strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
	All rights reserved.
	<div class="float-right d-none d-sm-inline-block">
		<b>Version</b> 3.0.3
	</div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<style>
	.pagination {
		display: block;
		width: 100%;
		padding: 20px 10px;
		box-sizing: border-box;
		text-align: center;
	}

	.pagination .item {
		border: 1px solid #727975;
		color: #727975;
		padding: 5px 10px;
		border-radius: 50%;
		font-weight: bold;
		text-decoration: none;
	}

	.pagination .item.active,
	.pagination .item:hover {
		background: #727975;
		color: #fff;
	}

	/* UPLOAD FOTO */
	.uploadfotodivider {
		display: block;
		overflow: hidden;
	}

	.uploadfoto {
		float: left;
		margin-right: 10px;
	}

	.form-uploadfoto {
		border: 1px dashed #ddd;
		border-radius: 15px;
		width: 120px;
		height: 120px;
		position: relative;
		background: #fff;
	}

	.form-uploadfoto:hover {
		cursor: pointer;
		border-color: #aaa;
	}

	.form-uploadfoto input[type="file"] {
		display: none;
	}

	.form-uploadfoto img {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 40px;
		margin: -20px 0 0 -20px;
		transition: 0.3s ease-out;
	}

	.form-uploadfoto:hover>img {
		width: 50px;
		margin: -25px 0 0 -25px;
	}

	.uploadfoto-result {
		margin: 0;
		float: left;
	}

	.uploadfoto-item {
		border: 1px solid #ccc;
		width: 120px;
		height: 120px;
		border-radius: 15px;
		position: relative;
		display: inline-block;
		margin: 0 10px 0 0;
		overflow: hidden;
	}

	.uploadfoto-item img {
		vertical-align: middle;
		max-width: 120%;
		max-height: 120%;
		position: absolute;
		top: 45%;
		left: 50%;
		transform: translate(-50%, -50%);
	}

	.uploadfoto-item .jadiutama {
		padding: 3px;
		width: 50%;
		margin: 0;
		position: absolute;
		bottom: 0;
		left: 0;
		border: none;
		font-weight: normal;
		text-decoration: none;
		color: #fff;
		background: #27ae60;
	}

	.uploadfoto-item .jadiutama:hover {
		cursor: pointer;
		background: #31d878;
	}

	.uploadfoto-item .hapus {
		background: #e74c3c;
		color: #fff;
		padding: 3px;
		width: 50%;
		margin: 0;
		position: absolute;
		bottom: 0;
		right: 0;
		border: none;
		font-weight: normal;
		text-decoration: none;
	}

	.uploadfoto-item .hapus:hover {
		cursor: pointer;
		background: #ff5f4f;
	}

	.uploadfoto-item .button-full {
		width: 100%;
	}

	.uploadfoto-item .utama {
		padding: 3px;
		border: none;
		font-weight: normal;
		text-decoration: none;
		width: 100%;
		position: absolute;
		bottom: 0;
	}

	.uploadfoto-item button[disabled] {
		background: #fff;
		font-size: 80%;
	}

	.color-wrap {
		padding: 40px;
	}

	/* SNIPPET */
	.imgpreview {
		display: none;
		max-width: 100%;
		max-height: 50vh;
		box-shadow: 0px 0px 10px #7f8c8d;
	}

	.imgPreview {
		max-width: 100%;
		max-height: 50vh;
		box-shadow: 0px 0px 10px #7f8c8d;
		border-radius: 10px;
	}

	#imgInp {
		position: absolute;
		top: -100vh;
	}

	.imgInpPreview {
		padding: 30px;
		box-sizing: border-box;
		background: #f2f3f8;
		text-align: center;
	}

	.imgInpPreview .delete {
		display: none;
		margin-top: 10px;
	}

	.imgInpPreview .delete a {
		text-decoration: none;
		color: #c0392b;
		font-weight: bold;
	}

	.imgInpPreview .text {
		font-weight: bold;
		color: #95a5a6;
		padding: 20px 0;
		text-transform: uppercase;
	}

	/* TABS */
	.tabs {}

	.tabs .tabs-item {
		display: inline-block;
		padding: 8px 12px;
		color: #dee2e6;
		border-bottom: 3px solid transparent;
	}

	.tabs .tabs-item.active,
	.tabs .tabs-item:hover {
		border-bottom: 3px solid #6c757d;
		color: #6c757d;
	}

	/* PESAN KOTAK MASUK */
	.pesan {
		overflow-y: scroll;
		overflow-x: hidden;
		max-height: 70vh;
		padding-bottom: 36px;
	}

	.pesan .pesanwrap {
		position: relative;
		padding: 5px;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.pesan .isipesan {
		box-shadow: 0px 0px 4px 1px #eee;
		padding: 8px 16px;
		display: inline-block;
		width: auto;
		max-width: 60%;
		border-radius: 10px;
		text-align: center;
		font-size: 90%;
		position: relative;
	}

	.pesan .isipesan small {
		font-size: 70%;
	}

	/*.pesan .isipesan small{
	position: absolute;
	bottom: -16px;
	right: 0px;
	background: #dedede;
	color: #C0A230;
	padding: 2px 4px;
	border-radius: 4px;
}*/
	.pesan .center .isipesan {
		margin: auto;
		box-shadow: 0px 0px 2px 1px #eee;
		background: #eee;
		padding: 4px 12px;
		border-radius: 4px;
		text-align: center;
	}

	.pesan .right {
		justify-content: flex-end;
	}

	.pesan .right .isipesan {
		background: #c0a230;
		color: #fff;
		text-align: right;
	}

	.pesan .left {
		justify-content: flex-start;
	}

	.pesan .left .isipesan {
		text-align: left;
	}

	.formpesan {}

	.formpesan input {
		border-radius: 6px 0px 0px 6px;
	}

	.formpesan button {
		border-radius: 0px 6px 6px 0px;
	}

	.pesanmasuk {
		box-shadow: 0px 0px 6px #ccc;
		border-radius: 6px;
		padding: 10px 20px;
		cursor: pointer;
		margin-bottom: 10px;
	}

	.pesanmasuk .nama {
		font-weight: bold;
		font-size: 110%;
	}

	.pesanmasuk .isipesan {
		padding-left: 26px;
	}

	/* PENGATURAN LOGO */
	.logoset {
		width: 100%;
		margin-bottom: 24px;
	}

	.logoset .title {
		font-weight: bold;
		font-size: 120%;
		margin-bottom: 16px;
	}

	.logoset .favicon,
	.logoset .logo {
		border: 2px dashed #ccc;
		padding: 12px;
		text-align: center;
		box-sizing: border-box;
		width: 100%;
		max-width: 100%;
	}

	.logoset .logo img {
		margin-bottom: 12px;
		width: 80%;
	}

	.logoset .favicon img {
		margin-bottom: 12px;
		width: 30%;
	}

	/* LABEL PENGIRIMAN */
	.labelkirim {
		font-size: 140%;
	}

	.labelkirim .content,
	.labelkirim .header {
		margin-bottom: 20px;
	}

	.labelkirim .logo {
		max-width: 20%;
	}

	/* VOUCHER */
	.kodevoucher:focus,
	.kodevoucher {
		font-size: 140%;
		font-weight: bold;
		background: #eee;
	}

	/* BLINK */
	.blink {
		animation: blinker 1s linear infinite;
	}

	@keyframes blinker {
		50% {
			opacity: 0;
		}
	}

	.divider {
		height: 20px;
	}

	.divider10 {
		height: 10px;
	}

	.divider5 {
		height: 5px;
	}

	/* USABLE */
	.tox-statusbar__branding {
		display: none;
	}


	@media only screen and (max-width: 768px) {
		.wrap {
			width: 100%;
		}

		.saveproduk-imp {
			position: fixed;
			width: 100%;
			bottom: 0;
			left: 0;
			margin: 0;
			z-index: 1000;
		}
	}

	.progress-card {
		margin-bottom: 25px;
	}
</style>

<!-- jQuery -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/moment/moment.min.js"></script>
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url() ?>assets/AdminLTE3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/AdminLTE3/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url() ?>assets/AdminLTE3/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>assets/AdminLTE3/dist/js/demo.js"></script>
<script src="<?= base_url("assets/js/sweetalert2.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-datetimepicker.js"></script>



<script type="text/javascript">
	$(function() {
		$("#userpass").on("submit", function(e) {
			e.preventDefault();
			if ($("#usrpass").val() == $("#usrpass2").val()) {
				swal.fire({
					text: "pastikan lagi data yang anda masukkan sudah sesuai",
					title: "Validasi data",
					type: "warning",
					showCancelButton: true,
					cancelButtonText: "Cek Lagi"
				}).then((vals) => {
					if (vals.value) {
						$.post("<?= site_url("api/tambahuser") ?>", $("#userpass").serialize(), function(msg) {
							var data = eval("(" + msg + ")");
							if (data.success == true) {
								$("#modalgantipass").modal("hide");
								swal.fire("Berhasil", "data user sudah disimpan", "success");
							} else {
								swal.fire("Gagal!", "gagal menyimpan data, coba ulangi beberapa saat lagi", "error");
							}
						});
					}
				});
			} else {
				swal.fire("Cek Password", "password yang Anda masukkan tidak sesuai, pastikan isi formulirnya dengan benar", "error");
			}
		});
	});

	function logout() {
		swal.fire({
			text: "Anda yakin akan keluar?",
			title: "Logout",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "Batal"
		}).then((vals) => {
			window.location.href = "<?= site_url("tukunen/logout") ?>";
		});
	}
</script>

<div class="modal fade" id="modalgantipass" tabindex="-1" role="dialog" aria-labelledby="modalLagu" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h6 class="modal-title">Ganti Password</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if ($this->func->demo() == true) {
					echo "Fitur tidak tersedia di mode demo aplikasi";
				} else {
					echo '
							<form id="userpass">
							<div class="form-group">
								<label>Password Baru</label>
								<input type="password" id="usrpass" name="gantipass" class="form-control" required />
							</div>
							<div class="form-group">
								<label>Ulangi Password</label>
								<input type="password" id="usrpass2" class="form-control" required />
							</div>
							<div class="form-group m-tb-10">
								<button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Simpan</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal" ><i class="fas fa-times"></i> Batal</button>
							</div>
						</form>';
				}
				?>
			</div>
		</div>
	</div>
</div>

</body>

</html>