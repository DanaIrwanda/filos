<div class="content-wrapper">

	<section class="content">
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Topup User</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>

		<div class="m-b-60">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-4 row m-lr-0 flex-end">
							<div class="col-10 p-lr-0"><input type="text" class="form-control" onchange="cariData()" placeholder="cari user" id="cari" /></div>
							<div class="col-2 p-lr-0"><button class="btn btn-sm btn-secondary w-full" onclick="cariData()"><i class="fas fa-search"></i></button></div>
						</div>
					</div>
				</div>
				<div class="card-body" id="load">
					<i class="fas fa-spin fa-spinner"></i> Loading data...
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(function() {
		loadUser(1);

		$(".tabs-item").on('click', function() {
			$(".tabs-item.active").removeClass("active");
			$(this).addClass("active");
		});
	});

	function loadUser(page) {
		$("#load").html('<i class="fas fa-spin fa-spinner"></i> Loading data...');
		$.post("<?= site_url("tukunen/topupsaldo?load=normal&page=") ?>" + page, {
			"cari": $("#cari").val()
		}, function(msg) {
			$("#load").html(msg);
		});
	}

	function cariData() {
		loadUser(1);
	}

	function addSaldo(id) {
		swal.fire({
			text: "Tambah saldo",
			title: "Tambah saldo ke user",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "Batal"
		}).then((vals) => {
			if (vals.value) {
				$.post("<?= site_url("api/tambahsaldo") ?>", {
					"usrid": id,
					"jumlah": 10000
				}, function(msg) {
					var data = eval("(" + msg + ")");
					if (data.success == true) {
						loadUser(1)
						$("#modal").modal();
						swal.fire("Berhasil", "saldo telah ditambahkan", "success");
					} else {
						swal.fire("Gagal!", "gagal mengubah data user, coba ulangi beberapa saat lagi", "error");
					}
				});
			}
		});

	}

	function editSaldo(id, $total) {
		$(".modal-title").html("<i class='fas fa-tags'></i> Tambah Saldo");
		$("#usrid").val(id);
		$("#user").val(id);
		$("#total").val($total);
		$("#modal").modal();
	}
</script>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLagu" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h6 class="modal-title"><i class="fas fa-plus"></i> Tambah Data</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-4">
						<label> User id</label>
					</div>
					<div class="col-8">
						<input class="btn btn-success" id="user" readonly />
					</div>
				</div>

				<div class="row mt-3">
					<div class="col-4">
						<label> Saldo Awal</label>
					</div>
					<div class="col-4">
						<input class="btn btn-success" id="total" readonly />
					</div>
				</div>
			</div>
			<form id="simpan" action="<?= site_url("api/tambahsaldo") ?>" method="POST">
				<input type="hidden" id="usrid" name="usrid" value="0" />
				<div class="modal-body">
					<div class="form-group">
						<label>Jumlah</label>
						<input type="text" class="form-control" id="jumlah" name="jumlah" required />
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="submit" class="btn btn-success">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>